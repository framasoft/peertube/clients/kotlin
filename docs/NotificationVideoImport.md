
# NotificationVideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**video** | [**VideoInfo**](VideoInfo.md) |  |  [optional]
**torrentName** | **kotlin.String** |  |  [optional]
**magnetUri** | [**MagnetUri**](MagnetUri.md) |  |  [optional]
**targetUri** | [**java.net.URI**](java.net.URI.md) |  |  [optional]



