
# InlineObject10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **kotlin.String** | Reason why the user reports this video | 
**predefinedReasons** | [**inline**](#kotlin.collections.List&lt;PredefinedReasonsEnum&gt;) | Reason categories that help triage reports |  [optional]
**video** | [**AbusesVideo**](AbusesVideo.md) |  |  [optional]
**comment** | [**AbusesComment**](AbusesComment.md) |  |  [optional]
**account** | [**AbusesAccount**](AbusesAccount.md) |  |  [optional]


<a name="kotlin.collections.List<PredefinedReasonsEnum>"></a>
## Enum: predefinedReasons
Name | Value
---- | -----
predefinedReasons | violentOrAbusive, hatefulOrAbusive, spamOrMisleading, privacy, rights, serverRules, thumbnails, captions



