
# VideoCommentThreadTree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**VideoComment**](VideoComment.md) |  |  [optional]
**children** | [**kotlin.collections.List&lt;VideoCommentThreadTree&gt;**](VideoCommentThreadTree.md) |  |  [optional]



