# VideoRatesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMeVideosVideoIdRatingGet**](VideoRatesApi.md#usersMeVideosVideoIdRatingGet) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
[**videosIdRatePut**](VideoRatesApi.md#videosIdRatePut) | **PUT** /videos/{id}/rate | Like/dislike a video


<a name="usersMeVideosVideoIdRatingGet"></a>
# **usersMeVideosVideoIdRatingGet**
> GetMeVideoRating usersMeVideosVideoIdRatingGet(videoId)

Get rate of my user for a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoRatesApi()
val videoId : kotlin.Int = 56 // kotlin.Int | The video id
try {
    val result : GetMeVideoRating = apiInstance.usersMeVideosVideoIdRatingGet(videoId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoRatesApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoRatesApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **kotlin.Int**| The video id |

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdRatePut"></a>
# **videosIdRatePut**
> videosIdRatePut(id, inlineObject24)

Like/dislike a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoRatesApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val inlineObject24 : InlineObject24 =  // InlineObject24 | 
try {
    apiInstance.videosIdRatePut(id, inlineObject24)
} catch (e: ClientException) {
    println("4xx response calling VideoRatesApi#videosIdRatePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoRatesApi#videosIdRatePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **inlineObject24** | [**InlineObject24**](InlineObject24.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

