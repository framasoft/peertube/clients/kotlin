
# ServerConfigUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoQuota** | **kotlin.Int** |  |  [optional]
**videoQuotaDaily** | **kotlin.Int** |  |  [optional]



