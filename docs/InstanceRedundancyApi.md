# InstanceRedundancyApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serverRedundancyHostPut**](InstanceRedundancyApi.md#serverRedundancyHostPut) | **PUT** /server/redundancy/{host} | Update a server redundancy policy


<a name="serverRedundancyHostPut"></a>
# **serverRedundancyHostPut**
> serverRedundancyHostPut(host, inlineObject27)

Update a server redundancy policy

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceRedundancyApi()
val host : kotlin.String = host_example // kotlin.String | server domain to mirror
val inlineObject27 : InlineObject27 =  // InlineObject27 | 
try {
    apiInstance.serverRedundancyHostPut(host, inlineObject27)
} catch (e: ClientException) {
    println("4xx response calling InstanceRedundancyApi#serverRedundancyHostPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceRedundancyApi#serverRedundancyHostPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **kotlin.String**| server domain to mirror |
 **inlineObject27** | [**InlineObject27**](InlineObject27.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

