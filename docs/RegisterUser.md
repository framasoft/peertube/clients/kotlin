
# RegisterUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** | immutable name of the user, used to find or mention its actor | 
**password** | **kotlin.String** |  | 
**email** | **kotlin.String** | email of the user, used for login or service communications | 
**displayName** | **kotlin.String** | editable name of the user, displayed in its representations |  [optional]
**channel** | [**RegisterUserChannel**](RegisterUserChannel.md) |  |  [optional]



