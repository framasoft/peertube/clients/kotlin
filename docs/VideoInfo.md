
# VideoInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**uuid** | [**Uuid**](Uuid.md) |  |  [optional]
**name** | [**Name**](Name.md) |  |  [optional]



