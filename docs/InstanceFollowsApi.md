# InstanceFollowsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serverFollowersGet**](InstanceFollowsApi.md#serverFollowersGet) | **GET** /server/followers | List instances following the server
[**serverFollowersNameWithHostAcceptPost**](InstanceFollowsApi.md#serverFollowersNameWithHostAcceptPost) | **POST** /server/followers/{nameWithHost}/accept | Accept a pending follower to your server
[**serverFollowersNameWithHostDelete**](InstanceFollowsApi.md#serverFollowersNameWithHostDelete) | **DELETE** /server/followers/{nameWithHost} | Remove or reject a follower to your server
[**serverFollowersNameWithHostRejectPost**](InstanceFollowsApi.md#serverFollowersNameWithHostRejectPost) | **POST** /server/followers/{nameWithHost}/reject | Reject a pending follower to your server
[**serverFollowingGet**](InstanceFollowsApi.md#serverFollowingGet) | **GET** /server/following | List instances followed by the server
[**serverFollowingHostOrHandleDelete**](InstanceFollowsApi.md#serverFollowingHostOrHandleDelete) | **DELETE** /server/following/{hostOrHandle} | Unfollow an actor (PeerTube instance, channel or account)
[**serverFollowingPost**](InstanceFollowsApi.md#serverFollowingPost) | **POST** /server/following | Follow a list of actors (PeerTube instance, channel or account)


<a name="serverFollowersGet"></a>
# **serverFollowersGet**
> InlineResponse2001 serverFollowersGet(state, actorType, start, count, sort)

List instances following the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val state : kotlin.String = state_example // kotlin.String | 
val actorType : kotlin.String = actorType_example // kotlin.String | 
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : InlineResponse2001 = apiInstance.serverFollowersGet(state, actorType, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowersGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowersGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **kotlin.String**|  | [optional] [enum: pending, accepted]
 **actorType** | **kotlin.String**|  | [optional] [enum: Person, Application, Group, Service, Organization]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serverFollowersNameWithHostAcceptPost"></a>
# **serverFollowersNameWithHostAcceptPost**
> serverFollowersNameWithHostAcceptPost(nameWithHost)

Accept a pending follower to your server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val nameWithHost : kotlin.String =  // kotlin.String | The remote actor handle to remove from your followers
try {
    apiInstance.serverFollowersNameWithHostAcceptPost(nameWithHost)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowersNameWithHostAcceptPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowersNameWithHostAcceptPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nameWithHost** | [**kotlin.String**](.md)| The remote actor handle to remove from your followers |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverFollowersNameWithHostDelete"></a>
# **serverFollowersNameWithHostDelete**
> serverFollowersNameWithHostDelete(nameWithHost)

Remove or reject a follower to your server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val nameWithHost : kotlin.String =  // kotlin.String | The remote actor handle to remove from your followers
try {
    apiInstance.serverFollowersNameWithHostDelete(nameWithHost)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowersNameWithHostDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowersNameWithHostDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nameWithHost** | [**kotlin.String**](.md)| The remote actor handle to remove from your followers |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverFollowersNameWithHostRejectPost"></a>
# **serverFollowersNameWithHostRejectPost**
> serverFollowersNameWithHostRejectPost(nameWithHost)

Reject a pending follower to your server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val nameWithHost : kotlin.String =  // kotlin.String | The remote actor handle to remove from your followers
try {
    apiInstance.serverFollowersNameWithHostRejectPost(nameWithHost)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowersNameWithHostRejectPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowersNameWithHostRejectPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nameWithHost** | [**kotlin.String**](.md)| The remote actor handle to remove from your followers |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverFollowingGet"></a>
# **serverFollowingGet**
> InlineResponse2001 serverFollowingGet(state, actorType, start, count, sort)

List instances followed by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val state : kotlin.String = state_example // kotlin.String | 
val actorType : kotlin.String = actorType_example // kotlin.String | 
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : InlineResponse2001 = apiInstance.serverFollowingGet(state, actorType, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowingGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowingGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **kotlin.String**|  | [optional] [enum: pending, accepted]
 **actorType** | **kotlin.String**|  | [optional] [enum: Person, Application, Group, Service, Organization]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serverFollowingHostOrHandleDelete"></a>
# **serverFollowingHostOrHandleDelete**
> serverFollowingHostOrHandleDelete(hostOrHandle)

Unfollow an actor (PeerTube instance, channel or account)

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val hostOrHandle : kotlin.String = hostOrHandle_example // kotlin.String | The hostOrHandle to unfollow
try {
    apiInstance.serverFollowingHostOrHandleDelete(hostOrHandle)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowingHostOrHandleDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowingHostOrHandleDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostOrHandle** | **kotlin.String**| The hostOrHandle to unfollow |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverFollowingPost"></a>
# **serverFollowingPost**
> serverFollowingPost(inlineObject1)

Follow a list of actors (PeerTube instance, channel or account)

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = InstanceFollowsApi()
val inlineObject1 : InlineObject1 =  // InlineObject1 | 
try {
    apiInstance.serverFollowingPost(inlineObject1)
} catch (e: ClientException) {
    println("4xx response calling InstanceFollowsApi#serverFollowingPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling InstanceFollowsApi#serverFollowingPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject1** | [**InlineObject1**](InlineObject1.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

