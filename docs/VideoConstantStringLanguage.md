
# VideoConstantStringMinusLanguage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | language id of the video (see [/videos/languages](#operation/getLanguages)) |  [optional]
**label** | **kotlin.String** |  |  [optional]



