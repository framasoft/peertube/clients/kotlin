
# InlineObject21

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimestamp** | **kotlin.Int** | Start the video at this specific timestamp |  [optional]
**stopTimestamp** | **kotlin.Int** | Stop the video at this specific timestamp |  [optional]



