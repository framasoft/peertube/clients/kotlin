# SessionApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOAuthClient**](SessionApi.md#getOAuthClient) | **GET** /oauth-clients/local | Login prerequisite
[**getOAuthToken**](SessionApi.md#getOAuthToken) | **POST** /users/token | Login
[**revokeOAuthToken**](SessionApi.md#revokeOAuthToken) | **POST** /users/revoke-token | Logout


<a name="getOAuthClient"></a>
# **getOAuthClient**
> OAuthClient getOAuthClient()

Login prerequisite

You need to retrieve a client id and secret before [logging in](#operation/getOAuthToken).

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = SessionApi()
try {
    val result : OAuthClient = apiInstance.getOAuthClient()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#getOAuthClient")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#getOAuthClient")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OAuthClient**](OAuthClient.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getOAuthToken"></a>
# **getOAuthToken**
> InlineResponse2002 getOAuthToken(clientId, clientSecret, grantType, username, password, refreshToken)

Login

With your [client id and secret](#operation/getOAuthClient), you can retrieve an access and refresh tokens.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = SessionApi()
val clientId : kotlin.String = clientId_example // kotlin.String | 
val clientSecret : kotlin.String = clientSecret_example // kotlin.String | 
val grantType : kotlin.String = grantType_example // kotlin.String | 
val username : kotlin.String = username_example // kotlin.String | immutable name of the user, used to find or mention its actor
val password : kotlin.String = password_example // kotlin.String | 
val refreshToken : kotlin.String = refreshToken_example // kotlin.String | 
try {
    val result : InlineResponse2002 = apiInstance.getOAuthToken(clientId, clientSecret, grantType, username, password, refreshToken)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SessionApi#getOAuthToken")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#getOAuthToken")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **kotlin.String**|  | [optional]
 **clientSecret** | **kotlin.String**|  | [optional]
 **grantType** | **kotlin.String**|  | [optional] [default to password] [enum: password, refresh_token]
 **username** | **kotlin.String**| immutable name of the user, used to find or mention its actor | [optional]
 **password** | **kotlin.String**|  | [optional]
 **refreshToken** | **kotlin.String**|  | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="revokeOAuthToken"></a>
# **revokeOAuthToken**
> revokeOAuthToken()

Logout

Revokes your access token and its associated refresh token, destroying your current session.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = SessionApi()
try {
    apiInstance.revokeOAuthToken()
} catch (e: ClientException) {
    println("4xx response calling SessionApi#revokeOAuthToken")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SessionApi#revokeOAuthToken")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

