# VideoChannelsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsNameVideoChannelsGet**](VideoChannelsApi.md#accountsNameVideoChannelsGet) | **GET** /accounts/{name}/video-channels | List video channels of an account
[**addVideoChannel**](VideoChannelsApi.md#addVideoChannel) | **POST** /video-channels | Create a video channel
[**delVideoChannel**](VideoChannelsApi.md#delVideoChannel) | **DELETE** /video-channels/{channelHandle} | Delete a video channel
[**getVideoChannel**](VideoChannelsApi.md#getVideoChannel) | **GET** /video-channels/{channelHandle} | Get a video channel
[**getVideoChannelVideos**](VideoChannelsApi.md#getVideoChannelVideos) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
[**getVideoChannels**](VideoChannelsApi.md#getVideoChannels) | **GET** /video-channels | List video channels
[**putVideoChannel**](VideoChannelsApi.md#putVideoChannel) | **PUT** /video-channels/{channelHandle} | Update a video channel
[**videoChannelsChannelHandleAvatarDelete**](VideoChannelsApi.md#videoChannelsChannelHandleAvatarDelete) | **DELETE** /video-channels/{channelHandle}/avatar | Delete channel avatar
[**videoChannelsChannelHandleAvatarPickPost**](VideoChannelsApi.md#videoChannelsChannelHandleAvatarPickPost) | **POST** /video-channels/{channelHandle}/avatar/pick | Update channel avatar
[**videoChannelsChannelHandleBannerDelete**](VideoChannelsApi.md#videoChannelsChannelHandleBannerDelete) | **DELETE** /video-channels/{channelHandle}/banner | Delete channel banner
[**videoChannelsChannelHandleBannerPickPost**](VideoChannelsApi.md#videoChannelsChannelHandleBannerPickPost) | **POST** /video-channels/{channelHandle}/banner/pick | Update channel banner


<a name="accountsNameVideoChannelsGet"></a>
# **accountsNameVideoChannelsGet**
> VideoChannelList accountsNameVideoChannelsGet(name, withStats, start, count, sort)

List video channels of an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
val withStats : kotlin.Boolean = true // kotlin.Boolean | include view statistics for the last 30 days (only if authentified as the account user)
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoChannelList = apiInstance.accountsNameVideoChannelsGet(name, withStats, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |
 **withStats** | **kotlin.Boolean**| include view statistics for the last 30 days (only if authentified as the account user) | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="addVideoChannel"></a>
# **addVideoChannel**
> InlineResponse204 addVideoChannel(videoChannelCreate)

Create a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val videoChannelCreate : VideoChannelCreate =  // VideoChannelCreate | 
try {
    val result : InlineResponse204 = apiInstance.addVideoChannel(videoChannelCreate)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#addVideoChannel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#addVideoChannel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoChannelCreate** | [**VideoChannelCreate**](VideoChannelCreate.md)|  | [optional]

### Return type

[**InlineResponse204**](InlineResponse204.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="delVideoChannel"></a>
# **delVideoChannel**
> delVideoChannel(channelHandle)

Delete a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
try {
    apiInstance.delVideoChannel(channelHandle)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#delVideoChannel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#delVideoChannel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVideoChannel"></a>
# **getVideoChannel**
> VideoChannel getVideoChannel(channelHandle)

Get a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
try {
    val result : VideoChannel = apiInstance.getVideoChannel(channelHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#getVideoChannel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#getVideoChannel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideoChannelVideos"></a>
# **getVideoChannelVideos**
> VideoListResponse getVideoChannelVideos(channelHandle, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.getVideoChannelVideos(channelHandle, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#getVideoChannelVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#getVideoChannelVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideoChannels"></a>
# **getVideoChannels**
> VideoChannelList getVideoChannels(start, count, sort)

List video channels

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoChannelList = apiInstance.getVideoChannels(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#getVideoChannels")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#getVideoChannels")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putVideoChannel"></a>
# **putVideoChannel**
> putVideoChannel(channelHandle, videoChannelUpdate)

Update a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
val videoChannelUpdate : VideoChannelUpdate =  // VideoChannelUpdate | 
try {
    apiInstance.putVideoChannel(channelHandle, videoChannelUpdate)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#putVideoChannel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#putVideoChannel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |
 **videoChannelUpdate** | [**VideoChannelUpdate**](VideoChannelUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="videoChannelsChannelHandleAvatarDelete"></a>
# **videoChannelsChannelHandleAvatarDelete**
> videoChannelsChannelHandleAvatarDelete(channelHandle)

Delete channel avatar

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
try {
    apiInstance.videoChannelsChannelHandleAvatarDelete(channelHandle)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#videoChannelsChannelHandleAvatarDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#videoChannelsChannelHandleAvatarDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videoChannelsChannelHandleAvatarPickPost"></a>
# **videoChannelsChannelHandleAvatarPickPost**
> InlineResponse2004 videoChannelsChannelHandleAvatarPickPost(channelHandle, avatarfile)

Update channel avatar

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
val avatarfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload.
try {
    val result : InlineResponse2004 = apiInstance.videoChannelsChannelHandleAvatarPickPost(channelHandle, avatarfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#videoChannelsChannelHandleAvatarPickPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#videoChannelsChannelHandleAvatarPickPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |
 **avatarfile** | **java.io.File**| The file to upload. | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="videoChannelsChannelHandleBannerDelete"></a>
# **videoChannelsChannelHandleBannerDelete**
> videoChannelsChannelHandleBannerDelete(channelHandle)

Delete channel banner

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
try {
    apiInstance.videoChannelsChannelHandleBannerDelete(channelHandle)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#videoChannelsChannelHandleBannerDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#videoChannelsChannelHandleBannerDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videoChannelsChannelHandleBannerPickPost"></a>
# **videoChannelsChannelHandleBannerPickPost**
> InlineResponse20010 videoChannelsChannelHandleBannerPickPost(channelHandle, bannerfile)

Update channel banner

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelsApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
val bannerfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload.
try {
    val result : InlineResponse20010 = apiInstance.videoChannelsChannelHandleBannerPickPost(channelHandle, bannerfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelsApi#videoChannelsChannelHandleBannerPickPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelsApi#videoChannelsChannelHandleBannerPickPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |
 **bannerfile** | **java.io.File**| The file to upload. | [optional]

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

