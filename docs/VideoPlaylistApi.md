# VideoPlaylistApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videoPlaylistsGet**](VideoPlaylistApi.md#videoPlaylistsGet) | **GET** /video-playlists | Get list of video playlists


<a name="videoPlaylistsGet"></a>
# **videoPlaylistsGet**
> kotlin.Array&lt;VideoPlaylist&gt; videoPlaylistsGet(start, count, sort)

Get list of video playlists

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<VideoPlaylist> = apiInstance.videoPlaylistsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistApi#videoPlaylistsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistApi#videoPlaylistsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;VideoPlaylist&gt;**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

