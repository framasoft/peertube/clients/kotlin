
# AddUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** | immutable name of the user, used to find or mention its actor | 
**password** | **kotlin.String** |  | 
**email** | **kotlin.String** | The user email | 
**videoQuota** | **kotlin.Int** | The user video quota in bytes | 
**videoQuotaDaily** | **kotlin.Int** | The user daily video quota in bytes | 
**role** | [**UserRole**](UserRole.md) |  | 
**channelName** | **kotlin.String** | immutable name of the channel, used to interact with its actor |  [optional]
**adminFlags** | [**UserAdminFlags**](UserAdminFlags.md) |  |  [optional]



