
# ServerConfigSearchRemoteUri

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | **kotlin.Boolean** |  |  [optional]
**anonymous** | **kotlin.Boolean** |  |  [optional]



