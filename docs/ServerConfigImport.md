
# ServerConfigImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videos** | [**ServerConfigImportVideos**](ServerConfigImportVideos.md) |  |  [optional]



