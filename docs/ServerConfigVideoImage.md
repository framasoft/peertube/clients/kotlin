
# ServerConfigVideoImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extensions** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**propertySize** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  |  [optional]



