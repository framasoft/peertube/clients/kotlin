# VideoCommentApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosIdCommentThreadsGet**](VideoCommentApi.md#videosIdCommentThreadsGet) | **GET** /videos/{id}/comment-threads | Get the comment threads of a video by its id
[**videosIdCommentThreadsPost**](VideoCommentApi.md#videosIdCommentThreadsPost) | **POST** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
[**videosIdCommentThreadsThreadIdGet**](VideoCommentApi.md#videosIdCommentThreadsThreadIdGet) | **GET** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
[**videosIdCommentsCommentIdDelete**](VideoCommentApi.md#videosIdCommentsCommentIdDelete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
[**videosIdCommentsCommentIdPost**](VideoCommentApi.md#videosIdCommentsCommentIdPost) | **POST** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id


<a name="videosIdCommentThreadsGet"></a>
# **videosIdCommentThreadsGet**
> CommentThreadResponse videosIdCommentThreadsGet(id, start, count, sort)

Get the comment threads of a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort comments by criteria
try {
    val result : CommentThreadResponse = apiInstance.videosIdCommentThreadsGet(id, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentApi#videosIdCommentThreadsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentApi#videosIdCommentThreadsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort comments by criteria | [optional] [enum: -createdAt, -totalReplies]

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdCommentThreadsPost"></a>
# **videosIdCommentThreadsPost**
> CommentThreadPostResponse videosIdCommentThreadsPost(id)

Creates a comment thread, on a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    val result : CommentThreadPostResponse = apiInstance.videosIdCommentThreadsPost(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentApi#videosIdCommentThreadsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentApi#videosIdCommentThreadsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdCommentThreadsThreadIdGet"></a>
# **videosIdCommentThreadsThreadIdGet**
> VideoCommentThreadTree videosIdCommentThreadsThreadIdGet(id, threadId)

Get the comment thread by its id, of a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val threadId : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The thread id (root comment id)
try {
    val result : VideoCommentThreadTree = apiInstance.videosIdCommentThreadsThreadIdGet(id, threadId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentApi#videosIdCommentThreadsThreadIdGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentApi#videosIdCommentThreadsThreadIdGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **threadId** | **java.math.BigDecimal**| The thread id (root comment id) |

### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdCommentsCommentIdDelete"></a>
# **videosIdCommentsCommentIdDelete**
> videosIdCommentsCommentIdDelete(id, commentId)

Delete a comment in a comment thread by its id, of a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val commentId : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The comment id
try {
    apiInstance.videosIdCommentsCommentIdDelete(id, commentId)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentApi#videosIdCommentsCommentIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentApi#videosIdCommentsCommentIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **commentId** | **java.math.BigDecimal**| The comment id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdCommentsCommentIdPost"></a>
# **videosIdCommentsCommentIdPost**
> CommentThreadPostResponse videosIdCommentsCommentIdPost(id, commentId)

Creates a comment in a comment thread by its id, of a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val commentId : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The comment id
try {
    val result : CommentThreadPostResponse = apiInstance.videosIdCommentsCommentIdPost(id, commentId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentApi#videosIdCommentsCommentIdPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentApi#videosIdCommentsCommentIdPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **commentId** | **java.math.BigDecimal**| The comment id |

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

