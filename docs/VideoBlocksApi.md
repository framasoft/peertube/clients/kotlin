# VideoBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addVideoBlock**](VideoBlocksApi.md#addVideoBlock) | **POST** /videos/{id}/blacklist | Block a video
[**delVideoBlock**](VideoBlocksApi.md#delVideoBlock) | **DELETE** /videos/{id}/blacklist | Unblock a video by its id
[**getVideoBlocks**](VideoBlocksApi.md#getVideoBlocks) | **GET** /videos/blacklist | List video blocks


<a name="addVideoBlock"></a>
# **addVideoBlock**
> addVideoBlock(id)

Block a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlocksApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.addVideoBlock(id)
} catch (e: ClientException) {
    println("4xx response calling VideoBlocksApi#addVideoBlock")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlocksApi#addVideoBlock")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delVideoBlock"></a>
# **delVideoBlock**
> delVideoBlock(id)

Unblock a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlocksApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.delVideoBlock(id)
} catch (e: ClientException) {
    println("4xx response calling VideoBlocksApi#delVideoBlock")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlocksApi#delVideoBlock")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVideoBlocks"></a>
# **getVideoBlocks**
> InlineResponse2008 getVideoBlocks(type, search, start, count, sort)

List video blocks

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlocksApi()
val type : kotlin.Int = 56 // kotlin.Int | list only blocks that match this type: - `1`: manual block - `2`: automatic block that needs review 
val search : kotlin.String = search_example // kotlin.String | plain search that will match with video titles, and more
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort blocklists by criteria
try {
    val result : InlineResponse2008 = apiInstance.getVideoBlocks(type, search, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoBlocksApi#getVideoBlocks")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlocksApi#getVideoBlocks")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **kotlin.Int**| list only blocks that match this type: - &#x60;1&#x60;: manual block - &#x60;2&#x60;: automatic block that needs review  | [optional] [enum: 1, 2]
 **search** | **kotlin.String**| plain search that will match with video titles, and more | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort blocklists by criteria | [optional] [enum: -id, name, -duration, -views, -likes, -dislikes, -uuid, -createdAt]

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

