# RegisterApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**registerUser**](RegisterApi.md#registerUser) | **POST** /users/register | Register a user
[**resendEmailToVerifyUser**](RegisterApi.md#resendEmailToVerifyUser) | **POST** /users/ask-send-verify-email | Resend user verification link
[**verifyUser**](RegisterApi.md#verifyUser) | **POST** /users/{id}/verify-email | Verify a user


<a name="registerUser"></a>
# **registerUser**
> registerUser(registerUser)

Register a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = RegisterApi()
val registerUser : RegisterUser =  // RegisterUser | 
try {
    apiInstance.registerUser(registerUser)
} catch (e: ClientException) {
    println("4xx response calling RegisterApi#registerUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RegisterApi#registerUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerUser** | [**RegisterUser**](RegisterUser.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="resendEmailToVerifyUser"></a>
# **resendEmailToVerifyUser**
> resendEmailToVerifyUser()

Resend user verification link

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = RegisterApi()
try {
    apiInstance.resendEmailToVerifyUser()
} catch (e: ClientException) {
    println("4xx response calling RegisterApi#resendEmailToVerifyUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RegisterApi#resendEmailToVerifyUser")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="verifyUser"></a>
# **verifyUser**
> verifyUser(id, inlineObject2)

Verify a user

Following a user registration, the new user will receive an email asking to click a link containing a secret. 

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = RegisterApi()
val id : kotlin.Int = 56 // kotlin.Int | The user id
val inlineObject2 : InlineObject2 =  // InlineObject2 | 
try {
    apiInstance.verifyUser(id, inlineObject2)
} catch (e: ClientException) {
    println("4xx response calling RegisterApi#verifyUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling RegisterApi#verifyUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| The user id |
 **inlineObject2** | [**InlineObject2**](InlineObject2.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

