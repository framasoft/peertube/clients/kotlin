
# MRSSGroupContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **kotlin.String** |  |  [optional]
**fileSize** | **kotlin.Int** |  |  [optional]
**type** | **kotlin.String** |  |  [optional]
**framerate** | **kotlin.Int** |  |  [optional]
**duration** | **kotlin.Int** |  |  [optional]
**height** | **kotlin.Int** |  |  [optional]
**lang** | **kotlin.String** |  |  [optional]



