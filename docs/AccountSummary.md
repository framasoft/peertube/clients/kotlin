
# AccountSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**host** | **kotlin.String** |  |  [optional]
**avatar** | [**ActorImage**](ActorImage.md) |  |  [optional]



