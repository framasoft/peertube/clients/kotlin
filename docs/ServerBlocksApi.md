# ServerBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serverBlocklistServersGet**](ServerBlocksApi.md#serverBlocklistServersGet) | **GET** /server/blocklist/servers | List server blocks
[**serverBlocklistServersHostDelete**](ServerBlocksApi.md#serverBlocklistServersHostDelete) | **DELETE** /server/blocklist/servers/{host} | Unblock a server by its domain
[**serverBlocklistServersPost**](ServerBlocksApi.md#serverBlocklistServersPost) | **POST** /server/blocklist/servers | Block a server


<a name="serverBlocklistServersGet"></a>
# **serverBlocklistServersGet**
> serverBlocklistServersGet(start, count, sort)

List server blocks

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerBlocksApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    apiInstance.serverBlocklistServersGet(start, count, sort)
} catch (e: ClientException) {
    println("4xx response calling ServerBlocksApi#serverBlocklistServersGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerBlocksApi#serverBlocklistServersGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverBlocklistServersHostDelete"></a>
# **serverBlocklistServersHostDelete**
> serverBlocklistServersHostDelete(host)

Unblock a server by its domain

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerBlocksApi()
val host : kotlin.String = host_example // kotlin.String | server domain to unblock
try {
    apiInstance.serverBlocklistServersHostDelete(host)
} catch (e: ClientException) {
    println("4xx response calling ServerBlocksApi#serverBlocklistServersHostDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerBlocksApi#serverBlocklistServersHostDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **kotlin.String**| server domain to unblock |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverBlocklistServersPost"></a>
# **serverBlocklistServersPost**
> serverBlocklistServersPost(inlineObject26)

Block a server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerBlocksApi()
val inlineObject26 : InlineObject26 =  // InlineObject26 | 
try {
    apiInstance.serverBlocklistServersPost(inlineObject26)
} catch (e: ClientException) {
    println("4xx response calling ServerBlocksApi#serverBlocklistServersPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerBlocksApi#serverBlocklistServersPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject26** | [**InlineObject26**](InlineObject26.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

