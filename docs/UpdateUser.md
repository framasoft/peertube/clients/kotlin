
# UpdateUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | [**Email**](Email.md) | The updated email of the user |  [optional]
**emailVerified** | **kotlin.Boolean** | Set the email as verified |  [optional]
**videoQuota** | **kotlin.Int** | The updated video quota of the user in bytes |  [optional]
**videoQuotaDaily** | **kotlin.Int** | The updated daily video quota of the user in bytes |  [optional]
**pluginAuth** | **kotlin.String** | The auth plugin to use to authenticate the user |  [optional]
**role** | [**UserRole**](UserRole.md) |  |  [optional]
**adminFlags** | [**UserAdminFlags**](UserAdminFlags.md) |  |  [optional]



