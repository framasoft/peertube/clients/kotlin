
# NotificationListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **kotlin.Int** |  |  [optional]
**&#x60;data&#x60;** | [**kotlin.collections.List&lt;Notification&gt;**](Notification.md) |  |  [optional]



