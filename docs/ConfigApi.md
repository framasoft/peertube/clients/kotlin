# ConfigApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delCustomConfig**](ConfigApi.md#delCustomConfig) | **DELETE** /config/custom | Delete instance runtime configuration
[**getAbout**](ConfigApi.md#getAbout) | **GET** /config/about | Get instance \&quot;About\&quot; information
[**getConfig**](ConfigApi.md#getConfig) | **GET** /config | Get instance public configuration
[**getCustomConfig**](ConfigApi.md#getCustomConfig) | **GET** /config/custom | Get instance runtime configuration
[**putCustomConfig**](ConfigApi.md#putCustomConfig) | **PUT** /config/custom | Set instance runtime configuration


<a name="delCustomConfig"></a>
# **delCustomConfig**
> delCustomConfig()

Delete instance runtime configuration

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    apiInstance.delCustomConfig()
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#delCustomConfig")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#delCustomConfig")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAbout"></a>
# **getAbout**
> ServerConfigAbout getAbout()

Get instance \&quot;About\&quot; information

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfigAbout = apiInstance.getAbout()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#getAbout")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#getAbout")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigAbout**](ServerConfigAbout.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getConfig"></a>
# **getConfig**
> ServerConfig getConfig()

Get instance public configuration

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfig = apiInstance.getConfig()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#getConfig")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#getConfig")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfig**](ServerConfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCustomConfig"></a>
# **getCustomConfig**
> ServerConfigCustom getCustomConfig()

Get instance runtime configuration

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfigCustom = apiInstance.getCustomConfig()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#getCustomConfig")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#getCustomConfig")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigCustom**](ServerConfigCustom.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putCustomConfig"></a>
# **putCustomConfig**
> putCustomConfig()

Set instance runtime configuration

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    apiInstance.putCustomConfig()
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#putCustomConfig")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#putCustomConfig")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

