
# InlineObject6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Video thumbnail file |  [optional]
**previewfile** | [**java.io.File**](java.io.File.md) | Video preview file |  [optional]
**category** | **kotlin.Int** | Video category |  [optional]
**licence** | **kotlin.Int** | Video licence |  [optional]
**language** | **kotlin.String** | Video language |  [optional]
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]
**description** | **kotlin.String** | Video description |  [optional]
**waitTranscoding** | **kotlin.String** | Whether or not we wait transcoding before publish the video |  [optional]
**support** | **kotlin.String** | A text tell the audience how to support the video creator |  [optional]
**nsfw** | **kotlin.Boolean** | Whether or not this video contains sensitive content |  [optional]
**name** | **kotlin.String** | Video name |  [optional]
**tags** | **kotlin.collections.List&lt;kotlin.String&gt;** | Video tags (maximum 5 tags each between 2 and 30 characters) |  [optional]
**commentsEnabled** | **kotlin.Boolean** | Enable or disable comments for this video |  [optional]
**originallyPublishedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | Date when the content was originally published |  [optional]
**scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]



