
# InlineResponse2005VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]



