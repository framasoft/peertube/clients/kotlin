
# VideoCaption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**VideoConstantStringMinusLanguage**](VideoConstantStringMinusLanguage.md) |  |  [optional]
**captionPath** | **kotlin.String** |  |  [optional]



