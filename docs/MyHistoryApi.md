# MyHistoryApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMeHistoryVideosGet**](MyHistoryApi.md#usersMeHistoryVideosGet) | **GET** /users/me/history/videos | List watched videos history
[**usersMeHistoryVideosRemovePost**](MyHistoryApi.md#usersMeHistoryVideosRemovePost) | **POST** /users/me/history/videos/remove | Clear video history


<a name="usersMeHistoryVideosGet"></a>
# **usersMeHistoryVideosGet**
> VideoListResponse usersMeHistoryVideosGet(start, count, search)

List watched videos history

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyHistoryApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val search : kotlin.String = search_example // kotlin.String | Plain text search, applied to various parts of the model depending on endpoint
try {
    val result : VideoListResponse = apiInstance.usersMeHistoryVideosGet(start, count, search)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyHistoryApi#usersMeHistoryVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyHistoryApi#usersMeHistoryVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **search** | **kotlin.String**| Plain text search, applied to various parts of the model depending on endpoint | [optional]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeHistoryVideosRemovePost"></a>
# **usersMeHistoryVideosRemovePost**
> usersMeHistoryVideosRemovePost(beforeDate)

Clear video history

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyHistoryApi()
val beforeDate : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | history before this date will be deleted
try {
    apiInstance.usersMeHistoryVideosRemovePost(beforeDate)
} catch (e: ClientException) {
    println("4xx response calling MyHistoryApi#usersMeHistoryVideosRemovePost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyHistoryApi#usersMeHistoryVideosRemovePost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **beforeDate** | **java.time.OffsetDateTime**| history before this date will be deleted | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

