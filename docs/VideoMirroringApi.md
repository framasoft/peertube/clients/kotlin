# VideoMirroringApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delMirroredVideo**](VideoMirroringApi.md#delMirroredVideo) | **DELETE** /server/redundancy/videos/{redundancyId} | Delete a mirror done on a video
[**getMirroredVideos**](VideoMirroringApi.md#getMirroredVideos) | **GET** /server/redundancy/videos | List videos being mirrored
[**putMirroredVideo**](VideoMirroringApi.md#putMirroredVideo) | **POST** /server/redundancy/videos | Mirror a video


<a name="delMirroredVideo"></a>
# **delMirroredVideo**
> delMirroredVideo(redundancyId)

Delete a mirror done on a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoMirroringApi()
val redundancyId : kotlin.String = redundancyId_example // kotlin.String | id of an existing redundancy on a video
try {
    apiInstance.delMirroredVideo(redundancyId)
} catch (e: ClientException) {
    println("4xx response calling VideoMirroringApi#delMirroredVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoMirroringApi#delMirroredVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **redundancyId** | **kotlin.String**| id of an existing redundancy on a video |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMirroredVideos"></a>
# **getMirroredVideos**
> kotlin.collections.List&lt;VideoRedundancy&gt; getMirroredVideos(target, start, count, sort)

List videos being mirrored

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoMirroringApi()
val target : kotlin.String = target_example // kotlin.String | direction of the mirror
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
try {
    val result : kotlin.collections.List<VideoRedundancy> = apiInstance.getMirroredVideos(target, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoMirroringApi#getMirroredVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoMirroringApi#getMirroredVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target** | **kotlin.String**| direction of the mirror | [enum: my-videos, remote-videos]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: name]

### Return type

[**kotlin.collections.List&lt;VideoRedundancy&gt;**](VideoRedundancy.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putMirroredVideo"></a>
# **putMirroredVideo**
> putMirroredVideo(inlineObject28)

Mirror a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoMirroringApi()
val inlineObject28 : InlineObject28 =  // InlineObject28 | 
try {
    apiInstance.putMirroredVideo(inlineObject28)
} catch (e: ClientException) {
    println("4xx response calling VideoMirroringApi#putMirroredVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoMirroringApi#putMirroredVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject28** | [**InlineObject28**](InlineObject28.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

