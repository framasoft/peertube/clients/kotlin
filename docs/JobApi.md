# JobApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getJobs**](JobApi.md#getJobs) | **GET** /jobs/{state} | List instance jobs


<a name="getJobs"></a>
# **getJobs**
> InlineResponse200 getJobs(state, jobType, start, count, sort)

List instance jobs

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = JobApi()
val state : kotlin.String = state_example // kotlin.String | The state of the job ('' for for no filter)
val jobType : kotlin.String = jobType_example // kotlin.String | job type
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : InlineResponse200 = apiInstance.getJobs(state, jobType, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling JobApi#getJobs")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling JobApi#getJobs")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **kotlin.String**| The state of the job (&#39;&#39; for for no filter) | [enum: , active, completed, failed, waiting, delayed]
 **jobType** | **kotlin.String**| job type | [optional] [enum: activitypub-follow, activitypub-http-broadcast, activitypub-http-fetcher, activitypub-http-unicast, email, video-transcoding, video-file-import, video-import, videos-views, activitypub-refresher, video-redundancy, video-live-ending]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

