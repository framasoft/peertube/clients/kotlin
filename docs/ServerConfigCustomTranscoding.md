
# ServerConfigCustomTranscoding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **kotlin.Boolean** |  |  [optional]
**allowAdditionalExtensions** | **kotlin.Boolean** | Allow your users to upload .mkv, .mov, .avi, .wmv, .flv, .f4v, .3g2, .3gp, .mts, m2ts, .mxf, .nut videos |  [optional]
**allowAudioFiles** | **kotlin.Boolean** | If a user uploads an audio file, PeerTube will create a video by merging the preview file and the audio file |  [optional]
**threads** | **kotlin.Int** | Amount of threads used by ffmpeg for 1 transcoding job |  [optional]
**concurrency** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Amount of transcoding jobs to execute in parallel |  [optional]
**profile** | [**inline**](#ProfileEnum) | New profiles can be added by plugins ; available in core PeerTube: &#39;default&#39;.  |  [optional]
**resolutions** | [**ServerConfigCustomTranscodingResolutions**](ServerConfigCustomTranscodingResolutions.md) |  |  [optional]
**webtorrent** | [**ServerConfigCustomTranscodingWebtorrent**](ServerConfigCustomTranscodingWebtorrent.md) |  |  [optional]
**hls** | [**ServerConfigCustomTranscodingHls**](ServerConfigCustomTranscodingHls.md) |  |  [optional]


<a name="ProfileEnum"></a>
## Enum: profile
Name | Value
---- | -----
profile | default



