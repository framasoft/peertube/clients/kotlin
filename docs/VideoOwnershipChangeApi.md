# VideoOwnershipChangeApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosIdGiveOwnershipPost**](VideoOwnershipChangeApi.md#videosIdGiveOwnershipPost) | **POST** /videos/{id}/give-ownership | Request ownership change
[**videosOwnershipGet**](VideoOwnershipChangeApi.md#videosOwnershipGet) | **GET** /videos/ownership | List video ownership changes
[**videosOwnershipIdAcceptPost**](VideoOwnershipChangeApi.md#videosOwnershipIdAcceptPost) | **POST** /videos/ownership/{id}/accept | Accept ownership change request
[**videosOwnershipIdRefusePost**](VideoOwnershipChangeApi.md#videosOwnershipIdRefusePost) | **POST** /videos/ownership/{id}/refuse | Refuse ownership change request


<a name="videosIdGiveOwnershipPost"></a>
# **videosIdGiveOwnershipPost**
> videosIdGiveOwnershipPost(id, username)

Request ownership change

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoOwnershipChangeApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val username : kotlin.String = username_example // kotlin.String | 
try {
    apiInstance.videosIdGiveOwnershipPost(id, username)
} catch (e: ClientException) {
    println("4xx response calling VideoOwnershipChangeApi#videosIdGiveOwnershipPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoOwnershipChangeApi#videosIdGiveOwnershipPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **username** | **kotlin.String**|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

<a name="videosOwnershipGet"></a>
# **videosOwnershipGet**
> videosOwnershipGet()

List video ownership changes

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoOwnershipChangeApi()
try {
    apiInstance.videosOwnershipGet()
} catch (e: ClientException) {
    println("4xx response calling VideoOwnershipChangeApi#videosOwnershipGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoOwnershipChangeApi#videosOwnershipGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosOwnershipIdAcceptPost"></a>
# **videosOwnershipIdAcceptPost**
> videosOwnershipIdAcceptPost(id)

Accept ownership change request

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoOwnershipChangeApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.videosOwnershipIdAcceptPost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoOwnershipChangeApi#videosOwnershipIdAcceptPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoOwnershipChangeApi#videosOwnershipIdAcceptPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosOwnershipIdRefusePost"></a>
# **videosOwnershipIdRefusePost**
> videosOwnershipIdRefusePost(id)

Refuse ownership change request

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoOwnershipChangeApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.videosOwnershipIdRefusePost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoOwnershipChangeApi#videosOwnershipIdRefusePost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoOwnershipChangeApi#videosOwnershipIdRefusePost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

