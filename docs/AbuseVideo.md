
# AbuseVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) |  |  [optional]



