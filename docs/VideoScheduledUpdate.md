
# VideoScheduledUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateAt** | [**java.time.LocalDate**](java.time.LocalDate.md) | When to update the video | 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]



