
# VideoStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**inline**](#IdEnum) | The video state (Published &#x3D; &#x60;1&#x60;, to transcode &#x3D; &#x60;2&#x60;, to import &#x3D; &#x60;3&#x60;) |  [optional]
**label** | **kotlin.String** |  |  [optional]


<a name="IdEnum"></a>
## Enum: id
Name | Value
---- | -----
id | 1, 2, 3



