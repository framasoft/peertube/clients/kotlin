
# VideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional] [readonly]
**targetUrl** | **kotlin.String** | remote URL where to find the import&#39;s source video |  [optional]
**magnetUri** | [**java.net.URI**](java.net.URI.md) | magnet URI allowing to resolve the import&#39;s source video |  [optional]
**torrentfile** | [**java.io.File**](java.io.File.md) | Torrent file containing only the video file |  [optional]
**torrentName** | **kotlin.String** |  |  [optional] [readonly]
**state** | [**VideoImportStateConstant**](VideoImportStateConstant.md) |  |  [optional] [readonly]
**error** | **kotlin.String** |  |  [optional] [readonly]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional] [readonly]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional] [readonly]
**video** | [**Video**](Video.md) |  |  [optional] [readonly]



