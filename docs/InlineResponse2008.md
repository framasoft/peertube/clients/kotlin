
# InlineResponse2008

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **kotlin.Int** |  |  [optional]
**&#x60;data&#x60;** | [**kotlin.collections.List&lt;VideoBlacklist&gt;**](VideoBlacklist.md) |  |  [optional]



