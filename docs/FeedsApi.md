# FeedsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSyndicatedComments**](FeedsApi.md#getSyndicatedComments) | **GET** /feeds/video-comments.{format} | List comments on videos
[**getSyndicatedSubscriptionVideos**](FeedsApi.md#getSyndicatedSubscriptionVideos) | **GET** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token
[**getSyndicatedVideos**](FeedsApi.md#getSyndicatedVideos) | **GET** /feeds/videos.{format} | List videos


<a name="getSyndicatedComments"></a>
# **getSyndicatedComments**
> kotlin.collections.List&lt;kotlin.Any&gt; getSyndicatedComments(format, videoId, accountId, accountName, videoChannelId, videoChannelName)

List comments on videos

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = FeedsApi()
val format : kotlin.String = format_example // kotlin.String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
val videoId : kotlin.String = videoId_example // kotlin.String | limit listing to a specific video
val accountId : kotlin.String = accountId_example // kotlin.String | limit listing to a specific account
val accountName : kotlin.String = accountName_example // kotlin.String | limit listing to a specific account
val videoChannelId : kotlin.String = videoChannelId_example // kotlin.String | limit listing to a specific video channel
val videoChannelName : kotlin.String = videoChannelName_example // kotlin.String | limit listing to a specific video channel
try {
    val result : kotlin.collections.List<kotlin.Any> = apiInstance.getSyndicatedComments(format, videoId, accountId, accountName, videoChannelId, videoChannelName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling FeedsApi#getSyndicatedComments")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling FeedsApi#getSyndicatedComments")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **kotlin.String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | [enum: xml, rss, rss2, atom, atom1, json, json1]
 **videoId** | **kotlin.String**| limit listing to a specific video | [optional]
 **accountId** | **kotlin.String**| limit listing to a specific account | [optional]
 **accountName** | **kotlin.String**| limit listing to a specific account | [optional]
 **videoChannelId** | **kotlin.String**| limit listing to a specific video channel | [optional]
 **videoChannelName** | **kotlin.String**| limit listing to a specific video channel | [optional]

### Return type

[**kotlin.collections.List&lt;kotlin.Any&gt;**](kotlin.Any.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

<a name="getSyndicatedSubscriptionVideos"></a>
# **getSyndicatedSubscriptionVideos**
> kotlin.collections.List&lt;kotlin.Any&gt; getSyndicatedSubscriptionVideos(format, accountId, token, sort, nsfw, filter)

List videos of subscriptions tied to a token

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = FeedsApi()
val format : kotlin.String = format_example // kotlin.String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
val accountId : kotlin.String = accountId_example // kotlin.String | limit listing to a specific account
val token : kotlin.String = token_example // kotlin.String | private token allowing access
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
try {
    val result : kotlin.collections.List<kotlin.Any> = apiInstance.getSyndicatedSubscriptionVideos(format, accountId, token, sort, nsfw, filter)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling FeedsApi#getSyndicatedSubscriptionVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling FeedsApi#getSyndicatedSubscriptionVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **kotlin.String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | [enum: xml, rss, rss2, atom, atom1, json, json1]
 **accountId** | **kotlin.String**| limit listing to a specific account |
 **token** | **kotlin.String**| private token allowing access |
 **sort** | **kotlin.String**| Sort column | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]

### Return type

[**kotlin.collections.List&lt;kotlin.Any&gt;**](kotlin.Any.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

<a name="getSyndicatedVideos"></a>
# **getSyndicatedVideos**
> kotlin.collections.List&lt;kotlin.Any&gt; getSyndicatedVideos(format, accountId, accountName, videoChannelId, videoChannelName, sort, nsfw, filter)

List videos

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = FeedsApi()
val format : kotlin.String = format_example // kotlin.String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
val accountId : kotlin.String = accountId_example // kotlin.String | limit listing to a specific account
val accountName : kotlin.String = accountName_example // kotlin.String | limit listing to a specific account
val videoChannelId : kotlin.String = videoChannelId_example // kotlin.String | limit listing to a specific video channel
val videoChannelName : kotlin.String = videoChannelName_example // kotlin.String | limit listing to a specific video channel
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
try {
    val result : kotlin.collections.List<kotlin.Any> = apiInstance.getSyndicatedVideos(format, accountId, accountName, videoChannelId, videoChannelName, sort, nsfw, filter)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling FeedsApi#getSyndicatedVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling FeedsApi#getSyndicatedVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **kotlin.String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | [enum: xml, rss, rss2, atom, atom1, json, json1]
 **accountId** | **kotlin.String**| limit listing to a specific account | [optional]
 **accountName** | **kotlin.String**| limit listing to a specific account | [optional]
 **videoChannelId** | **kotlin.String**| limit listing to a specific video channel | [optional]
 **videoChannelName** | **kotlin.String**| limit listing to a specific video channel | [optional]
 **sort** | **kotlin.String**| Sort column | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]

### Return type

[**kotlin.collections.List&lt;kotlin.Any&gt;**](kotlin.Any.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

