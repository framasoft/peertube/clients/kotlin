
# InlineResponse2007VideoId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistElementId** | **kotlin.Int** |  |  [optional]
**playlistId** | **kotlin.Int** |  |  [optional]
**startTimestamp** | **kotlin.Int** |  |  [optional]
**stopTimestamp** | **kotlin.Int** |  |  [optional]



