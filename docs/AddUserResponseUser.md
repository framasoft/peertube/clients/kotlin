
# AddUserResponseUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**account** | [**InlineResponse2006Abuse**](InlineResponse2006Abuse.md) |  |  [optional]



