
# Follow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**follower** | [**Actor**](Actor.md) |  |  [optional]
**following** | [**Actor**](Actor.md) |  |  [optional]
**score** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | score reflecting the reachability of the actor, with steps of &#x60;10&#x60; and a base score of &#x60;1000&#x60;. |  [optional]
**state** | [**inline**](#StateEnum) |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]


<a name="StateEnum"></a>
## Enum: state
Name | Value
---- | -----
state | pending, accepted



