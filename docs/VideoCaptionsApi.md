# VideoCaptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addVideoCaption**](VideoCaptionsApi.md#addVideoCaption) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**delVideoCaption**](VideoCaptionsApi.md#delVideoCaption) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**getVideoCaptions**](VideoCaptionsApi.md#getVideoCaptions) | **GET** /videos/{id}/captions | List captions of a video


<a name="addVideoCaption"></a>
# **addVideoCaption**
> addVideoCaption(id, captionLanguage, captionfile)

Add or replace a video caption

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val captionLanguage : kotlin.String = captionLanguage_example // kotlin.String | The caption language
val captionfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload.
try {
    apiInstance.addVideoCaption(id, captionLanguage, captionfile)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionsApi#addVideoCaption")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionsApi#addVideoCaption")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **captionLanguage** | **kotlin.String**| The caption language |
 **captionfile** | **java.io.File**| The file to upload. | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

<a name="delVideoCaption"></a>
# **delVideoCaption**
> delVideoCaption(id, captionLanguage)

Delete a video caption

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val captionLanguage : kotlin.String = captionLanguage_example // kotlin.String | The caption language
try {
    apiInstance.delVideoCaption(id, captionLanguage)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionsApi#delVideoCaption")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionsApi#delVideoCaption")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **captionLanguage** | **kotlin.String**| The caption language |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVideoCaptions"></a>
# **getVideoCaptions**
> InlineResponse2009 getVideoCaptions(id)

List captions of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    val result : InlineResponse2009 = apiInstance.getVideoCaptions(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionsApi#getVideoCaptions")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionsApi#getVideoCaptions")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

