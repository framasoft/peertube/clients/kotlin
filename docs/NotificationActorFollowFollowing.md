
# NotificationActorFollowFollowing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**inline**](#TypeEnum) |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**host** | **kotlin.String** |  |  [optional]


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | account, channel, instance



