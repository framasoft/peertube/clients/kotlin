
# ServerConfigInstanceCustomizations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**javascript** | **kotlin.String** |  |  [optional]
**css** | **kotlin.String** |  |  [optional]



