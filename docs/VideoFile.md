
# VideoFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**magnetUri** | [**java.net.URI**](java.net.URI.md) | magnet URI allowing to resolve the video via BitTorrent without a metainfo file |  [optional]
**resolution** | [**VideoResolutionConstant**](VideoResolutionConstant.md) |  |  [optional]
**propertySize** | **kotlin.Int** | Video file size in bytes |  [optional]
**torrentUrl** | **kotlin.String** | Direct URL of the torrent file |  [optional]
**torrentDownloadUrl** | **kotlin.String** | URL endpoint that transfers the torrent file as an attachment (so that the browser opens a download dialog) |  [optional]
**fileUrl** | **kotlin.String** | Direct URL of the video |  [optional]
**fileDownloadUrl** | **kotlin.String** | URL endpoint that transfers the video file as an attachment (so that the browser opens a download dialog) |  [optional]
**fps** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Frames per second of the video file |  [optional]
**metadataUrl** | **kotlin.String** | URL dereferencing the output of ffprobe on the file |  [optional]



