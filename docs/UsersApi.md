# UsersApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addUser**](UsersApi.md#addUser) | **POST** /users | Create a user
[**delUser**](UsersApi.md#delUser) | **DELETE** /users/{id} | Delete a user
[**getUser**](UsersApi.md#getUser) | **GET** /users/{id} | Get a user
[**getUsers**](UsersApi.md#getUsers) | **GET** /users | List users
[**putUser**](UsersApi.md#putUser) | **PUT** /users/{id} | Update a user
[**registerUser**](UsersApi.md#registerUser) | **POST** /users/register | Register a user
[**resendEmailToVerifyUser**](UsersApi.md#resendEmailToVerifyUser) | **POST** /users/ask-send-verify-email | Resend user verification link
[**verifyUser**](UsersApi.md#verifyUser) | **POST** /users/{id}/verify-email | Verify a user


<a name="addUser"></a>
# **addUser**
> AddUserResponse addUser(addUser)

Create a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val addUser : AddUser =  // AddUser | If the smtp server is configured, you can leave the password empty and an email will be sent asking the user to set it first. 
try {
    val result : AddUserResponse = apiInstance.addUser(addUser)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#addUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#addUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addUser** | [**AddUser**](AddUser.md)| If the smtp server is configured, you can leave the password empty and an email will be sent asking the user to set it first.  |

### Return type

[**AddUserResponse**](AddUserResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="delUser"></a>
# **delUser**
> delUser(id)

Delete a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val id : kotlin.Int = 56 // kotlin.Int | The user id
try {
    apiInstance.delUser(id)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#delUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#delUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| The user id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getUser"></a>
# **getUser**
> OneOfLessThanUserCommaUserWithStatsGreaterThan getUser(id, withStats)

Get a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val id : kotlin.Int = 56 // kotlin.Int | The user id
val withStats : kotlin.Boolean = true // kotlin.Boolean | include statistics about the user (only available as a moderator/admin)
try {
    val result : OneOfLessThanUserCommaUserWithStatsGreaterThan = apiInstance.getUser(id, withStats)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#getUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#getUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| The user id |
 **withStats** | **kotlin.Boolean**| include statistics about the user (only available as a moderator/admin) | [optional]

### Return type

[**OneOfLessThanUserCommaUserWithStatsGreaterThan**](OneOfLessThanUserCommaUserWithStatsGreaterThan.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsers"></a>
# **getUsers**
> kotlin.collections.List&lt;User&gt; getUsers(search, blocked, start, count, sort)

List users

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val search : kotlin.String = search_example // kotlin.String | Plain text search that will match with user usernames or emails
val blocked : kotlin.Boolean = true // kotlin.Boolean | Filter results down to (un)banned users
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort users by criteria
try {
    val result : kotlin.collections.List<User> = apiInstance.getUsers(search, blocked, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#getUsers")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#getUsers")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **kotlin.String**| Plain text search that will match with user usernames or emails | [optional]
 **blocked** | **kotlin.Boolean**| Filter results down to (un)banned users | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort users by criteria | [optional] [enum: -id, -username, -createdAt]

### Return type

[**kotlin.collections.List&lt;User&gt;**](User.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putUser"></a>
# **putUser**
> putUser(id, updateUser)

Update a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val id : kotlin.Int = 56 // kotlin.Int | The user id
val updateUser : UpdateUser =  // UpdateUser | 
try {
    apiInstance.putUser(id, updateUser)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#putUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#putUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| The user id |
 **updateUser** | [**UpdateUser**](UpdateUser.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="registerUser"></a>
# **registerUser**
> registerUser(registerUser)

Register a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val registerUser : RegisterUser =  // RegisterUser | 
try {
    apiInstance.registerUser(registerUser)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#registerUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#registerUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerUser** | [**RegisterUser**](RegisterUser.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="resendEmailToVerifyUser"></a>
# **resendEmailToVerifyUser**
> resendEmailToVerifyUser()

Resend user verification link

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
try {
    apiInstance.resendEmailToVerifyUser()
} catch (e: ClientException) {
    println("4xx response calling UsersApi#resendEmailToVerifyUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#resendEmailToVerifyUser")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="verifyUser"></a>
# **verifyUser**
> verifyUser(id, inlineObject2)

Verify a user

Following a user registration, the new user will receive an email asking to click a link containing a secret. 

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UsersApi()
val id : kotlin.Int = 56 // kotlin.Int | The user id
val inlineObject2 : InlineObject2 =  // InlineObject2 | 
try {
    apiInstance.verifyUser(id, inlineObject2)
} catch (e: ClientException) {
    println("4xx response calling UsersApi#verifyUser")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UsersApi#verifyUser")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| The user id |
 **inlineObject2** | [**InlineObject2**](InlineObject2.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

