
# InlineObject19

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoId** | [**OneOfLessThanUuidCommaIntegerGreaterThan**](OneOfLessThanUuidCommaIntegerGreaterThan.md) | Video to add in the playlist | 
**startTimestamp** | **kotlin.Int** | Start the video at this specific timestamp |  [optional]
**stopTimestamp** | **kotlin.Int** | Stop the video at this specific timestamp |  [optional]



