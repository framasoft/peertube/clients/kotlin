# MySubscriptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMeSubscriptionsExistGet**](MySubscriptionsApi.md#usersMeSubscriptionsExistGet) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for my user
[**usersMeSubscriptionsGet**](MySubscriptionsApi.md#usersMeSubscriptionsGet) | **GET** /users/me/subscriptions | Get my user subscriptions
[**usersMeSubscriptionsPost**](MySubscriptionsApi.md#usersMeSubscriptionsPost) | **POST** /users/me/subscriptions | Add subscription to my user
[**usersMeSubscriptionsSubscriptionHandleDelete**](MySubscriptionsApi.md#usersMeSubscriptionsSubscriptionHandleDelete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
[**usersMeSubscriptionsSubscriptionHandleGet**](MySubscriptionsApi.md#usersMeSubscriptionsSubscriptionHandleGet) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
[**usersMeSubscriptionsVideosGet**](MySubscriptionsApi.md#usersMeSubscriptionsVideosGet) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user


<a name="usersMeSubscriptionsExistGet"></a>
# **usersMeSubscriptionsExistGet**
> kotlin.Any usersMeSubscriptionsExistGet(uris)

Get if subscriptions exist for my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val uris : kotlin.collections.List<java.net.URI> =  // kotlin.collections.List<java.net.URI> | list of uris to check if each is part of the user subscriptions
try {
    val result : kotlin.Any = apiInstance.usersMeSubscriptionsExistGet(uris)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsExistGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsExistGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uris** | [**kotlin.collections.List&lt;java.net.URI&gt;**](java.net.URI.md)| list of uris to check if each is part of the user subscriptions |

### Return type

[**kotlin.Any**](kotlin.Any.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsGet"></a>
# **usersMeSubscriptionsGet**
> VideoChannelList usersMeSubscriptionsGet(start, count, sort)

Get my user subscriptions

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoChannelList = apiInstance.usersMeSubscriptionsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsPost"></a>
# **usersMeSubscriptionsPost**
> usersMeSubscriptionsPost(inlineObject3)

Add subscription to my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val inlineObject3 : InlineObject3 =  // InlineObject3 | 
try {
    apiInstance.usersMeSubscriptionsPost(inlineObject3)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject3** | [**InlineObject3**](InlineObject3.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersMeSubscriptionsSubscriptionHandleDelete"></a>
# **usersMeSubscriptionsSubscriptionHandleDelete**
> usersMeSubscriptionsSubscriptionHandleDelete(subscriptionHandle)

Delete subscription of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val subscriptionHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The subscription handle
try {
    apiInstance.usersMeSubscriptionsSubscriptionHandleDelete(subscriptionHandle)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsSubscriptionHandleDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsSubscriptionHandleDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionHandle** | **kotlin.String**| The subscription handle |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeSubscriptionsSubscriptionHandleGet"></a>
# **usersMeSubscriptionsSubscriptionHandleGet**
> VideoChannel usersMeSubscriptionsSubscriptionHandleGet(subscriptionHandle)

Get subscription of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val subscriptionHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The subscription handle
try {
    val result : VideoChannel = apiInstance.usersMeSubscriptionsSubscriptionHandleGet(subscriptionHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsSubscriptionHandleGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsSubscriptionHandleGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionHandle** | **kotlin.String**| The subscription handle |

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsVideosGet"></a>
# **usersMeSubscriptionsVideosGet**
> VideoListResponse usersMeSubscriptionsVideosGet(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of subscriptions of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MySubscriptionsApi()
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.usersMeSubscriptionsVideosGet(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MySubscriptionsApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MySubscriptionsApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

