
# InlineObject20

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startPosition** | **kotlin.Int** | Start position of the element to reorder | 
**insertAfterPosition** | **kotlin.Int** | New position for the block to reorder, to add the block before the first element | 
**reorderLength** | **kotlin.Int** | How many element from &#x60;startPosition&#x60; to reorder |  [optional]



