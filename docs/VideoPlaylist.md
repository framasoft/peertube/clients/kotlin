
# VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) |  |  [optional]
**shortUUID** | **kotlin.String** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**videoLength** | **kotlin.Int** |  |  [optional]
**thumbnailPath** | **kotlin.String** |  |  [optional]
**privacy** | [**VideoPlaylistPrivacyConstant**](VideoPlaylistPrivacyConstant.md) |  |  [optional]
**type** | [**VideoPlaylistTypeConstant**](VideoPlaylistTypeConstant.md) |  |  [optional]
**ownerAccount** | [**AccountSummary**](AccountSummary.md) |  |  [optional]
**videoChannel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  |  [optional]



