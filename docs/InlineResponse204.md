
# InlineResponse204

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoChannel** | [**InlineResponse204VideoChannel**](InlineResponse204VideoChannel.md) |  |  [optional]



