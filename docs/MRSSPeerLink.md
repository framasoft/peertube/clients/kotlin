
# MRSSPeerLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **kotlin.String** |  |  [optional]
**type** | [**inline**](#TypeEnum) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | application/x-bittorrent



