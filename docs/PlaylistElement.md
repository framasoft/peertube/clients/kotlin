
# PlaylistElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **kotlin.Int** |  |  [optional]
**startTimestamp** | **kotlin.Int** |  |  [optional]
**stopTimestamp** | **kotlin.Int** |  |  [optional]
**video** | [**Video**](Video.md) |  |  [optional]



