
# VideoAbuseStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoAbuseStateSet**](VideoAbuseStateSet.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



