
# VideoChannelOwnerAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) |  |  [optional]



