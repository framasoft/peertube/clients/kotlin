
# RegisterUserChannel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | immutable name of the channel, used to interact with its actor |  [optional]
**displayName** | [**DisplayName**](DisplayName.md) |  |  [optional]



