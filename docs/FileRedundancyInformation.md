
# FileRedundancyInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**fileUrl** | **kotlin.String** |  |  [optional]
**strategy** | [**inline**](#StrategyEnum) |  |  [optional]
**propertySize** | **kotlin.Int** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**expiresOn** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]


<a name="StrategyEnum"></a>
## Enum: strategy
Name | Value
---- | -----
strategy | manual, most-views, trending, recently-added



