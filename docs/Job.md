
# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**state** | [**inline**](#StateEnum) |  |  [optional]
**type** | [**inline**](#TypeEnum) |  |  [optional]
**&#x60;data&#x60;** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt;**](kotlin.Any.md) |  |  [optional]
**error** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt;**](kotlin.Any.md) |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**finishedOn** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**processedOn** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]


<a name="StateEnum"></a>
## Enum: state
Name | Value
---- | -----
state | active, completed, failed, waiting, delayed


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | activitypub-http-unicast, activitypub-http-broadcast, activitypub-http-fetcher, activitypub-follow, video-file-import, video-transcoding, email, video-import, videos-views, activitypub-refresher, video-redundancy



