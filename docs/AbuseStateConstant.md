
# AbuseStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**AbuseStateSet**](AbuseStateSet.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



