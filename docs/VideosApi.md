# VideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addVideoPlaylistVideo**](VideosApi.md#addVideoPlaylistVideo) | **POST** /video-playlists/{playlistId}/videos | Add a video in a playlist
[**getVideoPlaylistVideos**](VideosApi.md#getVideoPlaylistVideos) | **GET** /video-playlists/{playlistId}/videos | List videos of a playlist
[**usersMeSubscriptionsVideosGet**](VideosApi.md#usersMeSubscriptionsVideosGet) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
[**usersMeVideosGet**](VideosApi.md#usersMeVideosGet) | **GET** /users/me/videos | Get videos of my user
[**usersMeVideosImportsGet**](VideosApi.md#usersMeVideosImportsGet) | **GET** /users/me/videos/imports | Get video imports of my user


<a name="addVideoPlaylistVideo"></a>
# **addVideoPlaylistVideo**
> InlineResponse20013 addVideoPlaylistVideo(playlistId, inlineObject19)

Add a video in a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideosApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val inlineObject19 : InlineObject19 =  // InlineObject19 | 
try {
    val result : InlineResponse20013 = apiInstance.addVideoPlaylistVideo(playlistId, inlineObject19)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideosApi#addVideoPlaylistVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideosApi#addVideoPlaylistVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **inlineObject19** | [**InlineObject19**](InlineObject19.md)|  | [optional]

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getVideoPlaylistVideos"></a>
# **getVideoPlaylistVideos**
> VideoListResponse getVideoPlaylistVideos(playlistId)

List videos of a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideosApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
try {
    val result : VideoListResponse = apiInstance.getVideoPlaylistVideos(playlistId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideosApi#getVideoPlaylistVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideosApi#getVideoPlaylistVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsVideosGet"></a>
# **usersMeSubscriptionsVideosGet**
> VideoListResponse usersMeSubscriptionsVideosGet(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of subscriptions of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideosApi()
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.usersMeSubscriptionsVideosGet(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideosApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideosApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosGet"></a>
# **usersMeVideosGet**
> VideoListResponse usersMeVideosGet(start, count, sort)

Get videos of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideosApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoListResponse = apiInstance.usersMeVideosGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideosApi#usersMeVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideosApi#usersMeVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosImportsGet"></a>
# **usersMeVideosImportsGet**
> VideoImportsList usersMeVideosImportsGet(start, count, sort)

Get video imports of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideosApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoImportsList = apiInstance.usersMeVideosImportsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideosApi#usersMeVideosImportsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideosApi#usersMeVideosImportsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoImportsList**](VideoImportsList.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

