
# VideoComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**text** | **kotlin.String** | Text of the comment |  [optional]
**threadId** | **kotlin.Int** |  |  [optional]
**inReplyToCommentId** | **kotlin.Int** |  |  [optional]
**videoId** | **kotlin.Int** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**deletedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**isDeleted** | **kotlin.Boolean** |  |  [optional]
**totalRepliesFromVideoAuthor** | **kotlin.Int** |  |  [optional]
**totalReplies** | **kotlin.Int** |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]



