
# AbusesVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | Video id to report |  [optional]
**startAt** | **kotlin.Int** | Timestamp in the video that marks the beginning of the report |  [optional]
**endAt** | **kotlin.Int** | Timestamp in the video that marks the ending of the report |  [optional]



