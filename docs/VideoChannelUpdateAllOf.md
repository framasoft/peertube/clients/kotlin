
# VideoChannelUpdateAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bulkVideosSupportUpdate** | **kotlin.Boolean** | Update the support field for all videos of this channel |  [optional]



