
# UserWithStatsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videosCount** | **kotlin.Int** | Count of videos published |  [optional]
**abusesCount** | **kotlin.Int** | Count of reports/abuses of which the user is a target |  [optional]
**abusesAcceptedCount** | **kotlin.Int** | Count of reports/abuses created by the user and accepted/acted upon by the moderation team |  [optional]
**abusesCreatedCount** | **kotlin.Int** | Count of reports/abuses created by the user |  [optional]
**videoCommentsCount** | **kotlin.Int** | Count of comments published |  [optional]



