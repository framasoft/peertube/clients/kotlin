
# VideoPrivacyConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



