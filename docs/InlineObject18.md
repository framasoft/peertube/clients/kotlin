
# InlineObject18

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimestamp** | **kotlin.Int** | Start the video at this specific timestamp (in seconds) |  [optional]
**stopTimestamp** | **kotlin.Int** | Stop the video at this specific timestamp (in seconds) |  [optional]



