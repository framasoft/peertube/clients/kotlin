
# UpdateMe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **kotlin.String** |  |  [optional]
**currentPassword** | **kotlin.String** |  |  [optional]
**email** | [**Email**](Email.md) | new email used for login and service communications |  [optional]
**displayName** | **kotlin.String** | new name of the user in its representations |  [optional]
**displayNSFW** | [**inline**](#DisplayNSFWEnum) | new NSFW display policy |  [optional]
**webTorrentEnabled** | **kotlin.Boolean** | whether to enable P2P in the player or not |  [optional]
**autoPlayVideo** | **kotlin.Boolean** | new preference regarding playing videos automatically |  [optional]
**autoPlayNextVideo** | **kotlin.Boolean** | new preference regarding playing following videos automatically |  [optional]
**autoPlayNextVideoPlaylist** | **kotlin.Boolean** | new preference regarding playing following playlist videos automatically |  [optional]
**videosHistoryEnabled** | **kotlin.Boolean** | whether to keep track of watched history or not |  [optional]
**videoLanguages** | **kotlin.collections.List&lt;kotlin.String&gt;** | list of languages to filter videos down to |  [optional]
**theme** | **kotlin.String** |  |  [optional]
**noInstanceConfigWarningModal** | **kotlin.Boolean** |  |  [optional]
**noAccountSetupWarningModal** | **kotlin.Boolean** |  |  [optional]
**noWelcomeModal** | **kotlin.Boolean** |  |  [optional]


<a name="DisplayNSFWEnum"></a>
## Enum: displayNSFW
Name | Value
---- | -----
displayNSFW | true, false, both



