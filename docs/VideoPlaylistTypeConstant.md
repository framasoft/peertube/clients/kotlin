
# VideoPlaylistTypeConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPlaylistTypeSet**](VideoPlaylistTypeSet.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



