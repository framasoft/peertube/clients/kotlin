# VideoApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLive**](VideoApi.md#addLive) | **POST** /videos/live | Create a live
[**addView**](VideoApi.md#addView) | **POST** /videos/{id}/views | Add a view to a video
[**delVideo**](VideoApi.md#delVideo) | **DELETE** /videos/{id} | Delete a video
[**getAccountVideos**](VideoApi.md#getAccountVideos) | **GET** /accounts/{name}/videos | List videos of an account
[**getCategories**](VideoApi.md#getCategories) | **GET** /videos/categories | List available video categories
[**getLanguages**](VideoApi.md#getLanguages) | **GET** /videos/languages | List available video languages
[**getLicences**](VideoApi.md#getLicences) | **GET** /videos/licences | List available video licences
[**getLiveId**](VideoApi.md#getLiveId) | **GET** /videos/live/{id} | Get information about a live
[**getPrivacyPolicies**](VideoApi.md#getPrivacyPolicies) | **GET** /videos/privacies | List available video privacy policies
[**getVideo**](VideoApi.md#getVideo) | **GET** /videos/{id} | Get a video
[**getVideoChannelVideos**](VideoApi.md#getVideoChannelVideos) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
[**getVideoDesc**](VideoApi.md#getVideoDesc) | **GET** /videos/{id}/description | Get complete video description
[**getVideos**](VideoApi.md#getVideos) | **GET** /videos | List videos
[**importVideo**](VideoApi.md#importVideo) | **POST** /videos/imports | Import a video
[**putVideo**](VideoApi.md#putVideo) | **PUT** /videos/{id} | Update a video
[**setProgress**](VideoApi.md#setProgress) | **PUT** /videos/{id}/watching | Set watching progress of a video
[**updateLiveId**](VideoApi.md#updateLiveId) | **PUT** /videos/live/{id} | Update information about a live
[**uploadLegacy**](VideoApi.md#uploadLegacy) | **POST** /videos/upload | Upload a video
[**uploadResumable**](VideoApi.md#uploadResumable) | **PUT** /videos/upload-resumable | Send chunk for the resumable upload of a video
[**uploadResumableCancel**](VideoApi.md#uploadResumableCancel) | **DELETE** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
[**uploadResumableInit**](VideoApi.md#uploadResumableInit) | **POST** /videos/upload-resumable | Initialize the resumable upload of a video


<a name="addLive"></a>
# **addLive**
> VideoUploadResponse addLive(channelId, name, saveReplay, permanentLive, thumbnailfile, previewfile, privacy, category, licence, language, description, support, nsfw, tags, commentsEnabled, downloadEnabled)

Create a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this live video
val name : kotlin.String = name_example // kotlin.String | Live video/replay name
val saveReplay : kotlin.Boolean = true // kotlin.Boolean | 
val permanentLive : kotlin.Boolean = true // kotlin.Boolean | User can stream multiple times in a permanent live
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Live video/replay thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Live video/replay preview file
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Live video/replay description
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this live video/replay contains sensitive content
val tags : kotlin.collections.List<kotlin.String> = tags_example // kotlin.collections.List<kotlin.String> | Live video/replay tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this live video/replay
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for the replay of this live video
try {
    val result : VideoUploadResponse = apiInstance.addLive(channelId, name, saveReplay, permanentLive, thumbnailfile, previewfile, privacy, category, licence, language, description, support, nsfw, tags, commentsEnabled, downloadEnabled)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#addLive")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#addLive")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.Int**| Channel id that will contain this live video |
 **name** | **kotlin.String**| Live video/replay name |
 **saveReplay** | **kotlin.Boolean**|  | [optional]
 **permanentLive** | **kotlin.Boolean**| User can stream multiple times in a permanent live | [optional]
 **thumbnailfile** | **java.io.File**| Live video/replay thumbnail file | [optional]
 **previewfile** | **java.io.File**| Live video/replay preview file | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Live video/replay description | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this live video/replay contains sensitive content | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this live video/replay | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for the replay of this live video | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="addView"></a>
# **addView**
> addView(id)

Add a view to a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.addView(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#addView")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#addView")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delVideo"></a>
# **delVideo**
> delVideo(id)

Delete a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    apiInstance.delVideo(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#delVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#delVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAccountVideos"></a>
# **getAccountVideos**
> VideoListResponse getAccountVideos(name, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.getAccountVideos(name, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getAccountVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getAccountVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCategories"></a>
# **getCategories**
> kotlin.collections.List&lt;kotlin.String&gt; getCategories()

List available video categories

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getCategories()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getCategories")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getCategories")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLanguages"></a>
# **getLanguages**
> kotlin.collections.List&lt;kotlin.String&gt; getLanguages()

List available video languages

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getLanguages()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getLanguages")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getLanguages")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLicences"></a>
# **getLicences**
> kotlin.collections.List&lt;kotlin.String&gt; getLicences()

List available video licences

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getLicences()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getLicences")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getLicences")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLiveId"></a>
# **getLiveId**
> LiveVideoResponse getLiveId(id)

Get information about a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    val result : LiveVideoResponse = apiInstance.getLiveId(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getLiveId")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getLiveId")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPrivacyPolicies"></a>
# **getPrivacyPolicies**
> kotlin.collections.List&lt;kotlin.String&gt; getPrivacyPolicies()

List available video privacy policies

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getPrivacyPolicies()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getPrivacyPolicies")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getPrivacyPolicies")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideo"></a>
# **getVideo**
> VideoDetails getVideo(id)

Get a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    val result : VideoDetails = apiInstance.getVideo(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideoChannelVideos"></a>
# **getVideoChannelVideos**
> VideoListResponse getVideoChannelVideos(channelHandle, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of a video channel

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val channelHandle : kotlin.String = my_username | my_username@example.com // kotlin.String | The video channel handle
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.getVideoChannelVideos(channelHandle, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getVideoChannelVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getVideoChannelVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle |
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideoDesc"></a>
# **getVideoDesc**
> kotlin.String getVideoDesc(id)

Get complete video description

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    val result : kotlin.String = apiInstance.getVideoDesc(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getVideoDesc")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getVideoDesc")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideos"></a>
# **getVideos**
> VideoListResponse getVideos(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.getVideos(categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#getVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#getVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="importVideo"></a>
# **importVideo**
> VideoUploadResponse importVideo(name, channelId, targetUrl, magnetUri, torrentfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile)

Import a video

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val name : kotlin.String = name_example // kotlin.String | Video name
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this video
val targetUrl : TargetUrl =  // TargetUrl | 
val magnetUri : MagnetUri =  // MagnetUri | 
val torrentfile : Torrentfile =  // Torrentfile | 
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.Boolean = true // kotlin.Boolean | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the video creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this video contains sensitive content
val tags : kotlin.collections.Set<kotlin.String> = tags_example // kotlin.collections.Set<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this video
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
try {
    val result : VideoUploadResponse = apiInstance.importVideo(name, channelId, targetUrl, magnetUri, torrentfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#importVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#importVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Video name |
 **channelId** | **kotlin.Int**| Channel id that will contain this video |
 **targetUrl** | [**TargetUrl**](TargetUrl.md)|  | [optional]
 **magnetUri** | [**MagnetUri**](MagnetUri.md)|  | [optional]
 **torrentfile** | [**Torrentfile**](Torrentfile.md)|  | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.Boolean**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the video creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.collections.Set&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this video | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putVideo"></a>
# **putVideo**
> putVideo(id, thumbnailfile, previewfile, category, licence, language, privacy, description, waitTranscoding, support, nsfw, name, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate)

Update a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.String = waitTranscoding_example // kotlin.String | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the video creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this video contains sensitive content
val name : kotlin.String = name_example // kotlin.String | Video name
val tags : kotlin.collections.List<kotlin.String> = tags_example // kotlin.collections.List<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this video
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
try {
    apiInstance.putVideo(id, thumbnailfile, previewfile, category, licence, language, privacy, description, waitTranscoding, support, nsfw, name, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#putVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#putVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.String**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the video creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this video contains sensitive content | [optional]
 **name** | **kotlin.String**| Video name | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this video | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

<a name="setProgress"></a>
# **setProgress**
> setProgress(id, userWatchingVideo)

Set watching progress of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val userWatchingVideo : UserWatchingVideo =  // UserWatchingVideo | 
try {
    apiInstance.setProgress(id, userWatchingVideo)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#setProgress")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#setProgress")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **userWatchingVideo** | [**UserWatchingVideo**](UserWatchingVideo.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="updateLiveId"></a>
# **updateLiveId**
> updateLiveId(id, liveVideoUpdate)

Update information about a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val liveVideoUpdate : LiveVideoUpdate =  // LiveVideoUpdate | 
try {
    apiInstance.updateLiveId(id, liveVideoUpdate)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#updateLiveId")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#updateLiveId")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **liveVideoUpdate** | [**LiveVideoUpdate**](LiveVideoUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="uploadLegacy"></a>
# **uploadLegacy**
> VideoUploadResponse uploadLegacy(name, channelId, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile, videofile)

Upload a video

Uses a single request to upload a video.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val name : kotlin.String = name_example // kotlin.String | Video name
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this video
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.Boolean = true // kotlin.Boolean | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the video creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this video contains sensitive content
val tags : kotlin.collections.Set<kotlin.String> = tags_example // kotlin.collections.Set<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this video
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val videofile : java.io.File = BINARY_DATA_HERE // java.io.File | Video file
try {
    val result : VideoUploadResponse = apiInstance.uploadLegacy(name, channelId, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile, videofile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#uploadLegacy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#uploadLegacy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Video name | [optional]
 **channelId** | **kotlin.Int**| Channel id that will contain this video | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.Boolean**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the video creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.collections.Set&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this video | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **videofile** | **java.io.File**| Video file | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadResumable"></a>
# **uploadResumable**
> VideoUploadResponse uploadResumable(uploadId, contentRange, contentLength, body)

Send chunk for the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to continue, pause or resume the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val uploadId : kotlin.String = uploadId_example // kotlin.String | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload. 
val contentRange : kotlin.String = bytes 0-262143/2469036 // kotlin.String | Specifies the bytes in the file that the request is uploading.  For example, a value of `bytes 0-262143/1000000` shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file. 
val contentLength : java.math.BigDecimal = 262144 // java.math.BigDecimal | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn't mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube's web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health. 
val body : java.io.File = BINARY_DATA_HERE // java.io.File | 
try {
    val result : VideoUploadResponse = apiInstance.uploadResumable(uploadId, contentRange, contentLength, body)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#uploadResumable")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#uploadResumable")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **kotlin.String**| Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  |
 **contentRange** | **kotlin.String**| Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  |
 **contentLength** | **java.math.BigDecimal**| Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  |
 **body** | **java.io.File**|  | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

<a name="uploadResumableCancel"></a>
# **uploadResumableCancel**
> uploadResumableCancel(uploadId, contentLength)

Cancel the resumable upload of a video, deleting any data uploaded so far

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to cancel the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val uploadId : kotlin.String = uploadId_example // kotlin.String | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-) 
val contentLength : java.math.BigDecimal = 0 // java.math.BigDecimal | 
try {
    apiInstance.uploadResumableCancel(uploadId, contentLength)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#uploadResumableCancel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#uploadResumableCancel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **kotlin.String**| Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  |
 **contentLength** | **java.math.BigDecimal**|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="uploadResumableInit"></a>
# **uploadResumableInit**
> uploadResumableInit(xUploadContentLength, xUploadContentType, videoUploadRequestResumable)

Initialize the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to initialize the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val xUploadContentLength : java.math.BigDecimal = 2469036 // java.math.BigDecimal | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading.
val xUploadContentType : kotlin.String = video/mp4 // kotlin.String | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary.
val videoUploadRequestResumable : VideoUploadRequestResumable =  // VideoUploadRequestResumable | 
try {
    apiInstance.uploadResumableInit(xUploadContentLength, xUploadContentType, videoUploadRequestResumable)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#uploadResumableInit")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#uploadResumableInit")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xUploadContentLength** | **java.math.BigDecimal**| Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. |
 **xUploadContentType** | **kotlin.String**| MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. |
 **videoUploadRequestResumable** | [**VideoUploadRequestResumable**](VideoUploadRequestResumable.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

