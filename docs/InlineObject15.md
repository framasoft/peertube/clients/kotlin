
# InlineObject15

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **kotlin.String** | Video playlist display name |  [optional]
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Video playlist thumbnail file |  [optional]
**privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  |  [optional]
**description** | **kotlin.String** | Video playlist description |  [optional]
**videoChannelId** | **kotlin.Int** | Video channel in which the playlist will be published |  [optional]



