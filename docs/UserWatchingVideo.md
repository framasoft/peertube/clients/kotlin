
# UserWatchingVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentTime** | **kotlin.Int** | timestamp within the video, in seconds |  [optional]



