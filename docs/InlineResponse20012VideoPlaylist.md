
# InlineResponse20012VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**uuid** | [**Uuid**](Uuid.md) |  |  [optional]
**shortUUID** | **kotlin.String** | translation of a uuid v4 with a bigger alphabet to have a shorter uuid |  [optional]



