
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**Account**](Account.md) |  |  [optional]
**autoPlayNextVideo** | **kotlin.Boolean** | Automatically start playing the upcoming video after the currently playing video |  [optional]
**autoPlayNextVideoPlaylist** | **kotlin.Boolean** | Automatically start playing the video on the playlist after the currently playing video |  [optional]
**autoPlayVideo** | **kotlin.Boolean** | Automatically start playing the video on the watch page |  [optional]
**blocked** | **kotlin.Boolean** |  |  [optional]
**blockedReason** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**email** | **kotlin.String** | The user email |  [optional]
**emailVerified** | **kotlin.Boolean** | Has the user confirmed their email address? |  [optional]
**id** | **kotlin.Int** |  |  [optional] [readonly]
**pluginAuth** | **kotlin.String** | Auth plugin to use to authenticate the user |  [optional]
**lastLoginDate** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**noInstanceConfigWarningModal** | **kotlin.Boolean** |  |  [optional]
**noAccountSetupWarningModal** | **kotlin.Boolean** |  |  [optional]
**noWelcomeModal** | **kotlin.Boolean** |  |  [optional]
**nsfwPolicy** | [**NSFWPolicy**](NSFWPolicy.md) |  |  [optional]
**role** | [**UserRole**](UserRole.md) |  |  [optional]
**roleLabel** | [**inline**](#RoleLabelEnum) |  |  [optional]
**theme** | **kotlin.String** | Theme enabled by this user |  [optional]
**username** | **kotlin.String** | immutable name of the user, used to find or mention its actor |  [optional]
**videoChannels** | [**kotlin.collections.List&lt;VideoChannel&gt;**](VideoChannel.md) |  |  [optional]
**videoQuota** | **kotlin.Int** | The user video quota in bytes |  [optional]
**videoQuotaDaily** | **kotlin.Int** | The user daily video quota in bytes |  [optional]
**webtorrentEnabled** | **kotlin.Boolean** | Enable P2P in the player |  [optional]


<a name="RoleLabelEnum"></a>
## Enum: roleLabel
Name | Value
---- | -----
roleLabel | User, Moderator, Administrator



