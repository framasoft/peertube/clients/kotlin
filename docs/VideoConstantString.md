
# VideoConstantString

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



