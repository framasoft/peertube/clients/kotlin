
# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoPlaylist** | [**InlineResponse20012VideoPlaylist**](InlineResponse20012VideoPlaylist.md) |  |  [optional]



