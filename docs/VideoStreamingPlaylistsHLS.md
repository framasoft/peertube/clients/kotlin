
# VideoStreamingPlaylistsMinusHLS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlistUrl** | **kotlin.String** |  |  [optional]
**segmentsSha256Url** | **kotlin.String** |  |  [optional]
**files** | [**kotlin.collections.List&lt;VideoFile&gt;**](VideoFile.md) | Video files associated to this playlist.  The difference with the root &#x60;files&#x60; property is that these files are fragmented, so they can be used in this streaming playlist (HLS, etc.)  |  [optional]
**redundancies** | [**kotlin.collections.List&lt;VideoStreamingPlaylistsHLSRedundancies&gt;**](VideoStreamingPlaylistsHLSRedundancies.md) |  |  [optional]



