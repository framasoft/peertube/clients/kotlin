
# InlineObject24

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rating** | [**inline**](#RatingEnum) |  | 


<a name="RatingEnum"></a>
## Enum: rating
Name | Value
---- | -----
rating | like, dislike



