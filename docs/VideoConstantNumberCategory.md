
# VideoConstantNumberMinusCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | category id of the video (see [/videos/categories](#operation/getCategories)) |  [optional]
**label** | **kotlin.String** |  |  [optional]



