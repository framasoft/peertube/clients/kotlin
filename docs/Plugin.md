
# Plugin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**type** | [**inline**](#TypeEnum) | - &#x60;1&#x60;: PLUGIN - &#x60;2&#x60;: THEME  |  [optional]
**latestVersion** | **kotlin.String** |  |  [optional]
**version** | **kotlin.String** |  |  [optional]
**enabled** | **kotlin.Boolean** |  |  [optional]
**uninstalled** | **kotlin.Boolean** |  |  [optional]
**peertubeEngine** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**homepage** | **kotlin.String** |  |  [optional]
**settings** | [**kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt;**](kotlin.Any.md) |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | 1, 2



