# AbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abusesAbuseIdDelete**](AbusesApi.md#abusesAbuseIdDelete) | **DELETE** /abuses/{abuseId} | Delete an abuse
[**abusesAbuseIdMessagesAbuseMessageIdDelete**](AbusesApi.md#abusesAbuseIdMessagesAbuseMessageIdDelete) | **DELETE** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
[**abusesAbuseIdMessagesGet**](AbusesApi.md#abusesAbuseIdMessagesGet) | **GET** /abuses/{abuseId}/messages | List messages of an abuse
[**abusesAbuseIdMessagesPost**](AbusesApi.md#abusesAbuseIdMessagesPost) | **POST** /abuses/{abuseId}/messages | Add message to an abuse
[**abusesAbuseIdPut**](AbusesApi.md#abusesAbuseIdPut) | **PUT** /abuses/{abuseId} | Update an abuse
[**abusesPost**](AbusesApi.md#abusesPost) | **POST** /abuses | Report an abuse
[**getAbuses**](AbusesApi.md#getAbuses) | **GET** /abuses | List abuses
[**getMyAbuses**](AbusesApi.md#getMyAbuses) | **GET** /users/me/abuses | List my abuses


<a name="abusesAbuseIdDelete"></a>
# **abusesAbuseIdDelete**
> abusesAbuseIdDelete(abuseId)

Delete an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val abuseId : kotlin.Int = 56 // kotlin.Int | Abuse id
try {
    apiInstance.abusesAbuseIdDelete(abuseId)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesAbuseIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesAbuseIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuseId** | **kotlin.Int**| Abuse id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="abusesAbuseIdMessagesAbuseMessageIdDelete"></a>
# **abusesAbuseIdMessagesAbuseMessageIdDelete**
> abusesAbuseIdMessagesAbuseMessageIdDelete(abuseId, abuseMessageId)

Delete an abuse message

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val abuseId : kotlin.Int = 56 // kotlin.Int | Abuse id
val abuseMessageId : kotlin.Int = 56 // kotlin.Int | Abuse message id
try {
    apiInstance.abusesAbuseIdMessagesAbuseMessageIdDelete(abuseId, abuseMessageId)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesAbuseIdMessagesAbuseMessageIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesAbuseIdMessagesAbuseMessageIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuseId** | **kotlin.Int**| Abuse id |
 **abuseMessageId** | **kotlin.Int**| Abuse message id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="abusesAbuseIdMessagesGet"></a>
# **abusesAbuseIdMessagesGet**
> InlineResponse2007 abusesAbuseIdMessagesGet(abuseId)

List messages of an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val abuseId : kotlin.Int = 56 // kotlin.Int | Abuse id
try {
    val result : InlineResponse2007 = apiInstance.abusesAbuseIdMessagesGet(abuseId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesAbuseIdMessagesGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesAbuseIdMessagesGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuseId** | **kotlin.Int**| Abuse id |

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="abusesAbuseIdMessagesPost"></a>
# **abusesAbuseIdMessagesPost**
> abusesAbuseIdMessagesPost(abuseId, inlineObject13)

Add message to an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val abuseId : kotlin.Int = 56 // kotlin.Int | Abuse id
val inlineObject13 : InlineObject13 =  // InlineObject13 | 
try {
    apiInstance.abusesAbuseIdMessagesPost(abuseId, inlineObject13)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesAbuseIdMessagesPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesAbuseIdMessagesPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuseId** | **kotlin.Int**| Abuse id |
 **inlineObject13** | [**InlineObject13**](InlineObject13.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="abusesAbuseIdPut"></a>
# **abusesAbuseIdPut**
> abusesAbuseIdPut(abuseId, inlineObject12)

Update an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val abuseId : kotlin.Int = 56 // kotlin.Int | Abuse id
val inlineObject12 : InlineObject12 =  // InlineObject12 | 
try {
    apiInstance.abusesAbuseIdPut(abuseId, inlineObject12)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesAbuseIdPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesAbuseIdPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuseId** | **kotlin.Int**| Abuse id |
 **inlineObject12** | [**InlineObject12**](InlineObject12.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="abusesPost"></a>
# **abusesPost**
> InlineResponse2006 abusesPost(inlineObject11)

Report an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val inlineObject11 : InlineObject11 =  // InlineObject11 | 
try {
    val result : InlineResponse2006 = apiInstance.abusesPost(inlineObject11)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#abusesPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#abusesPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject11** | [**InlineObject11**](InlineObject11.md)|  |

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAbuses"></a>
# **getAbuses**
> InlineResponse2005 getAbuses(id, predefinedReason, search, state, searchReporter, searchReportee, searchVideo, searchVideoChannel, videoIs, filter, start, count, sort)

List abuses

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val id : kotlin.Int = 56 // kotlin.Int | only list the report with this id
val predefinedReason : kotlin.collections.List<kotlin.String> =  // kotlin.collections.List<kotlin.String> | predefined reason the listed reports should contain
val search : kotlin.String = search_example // kotlin.String | plain search that will match with video titles, reporter names and more
val state : AbuseStateSet =  // AbuseStateSet | 
val searchReporter : kotlin.String = searchReporter_example // kotlin.String | only list reports of a specific reporter
val searchReportee : kotlin.String = searchReportee_example // kotlin.String | only list reports of a specific reportee
val searchVideo : kotlin.String = searchVideo_example // kotlin.String | only list reports of a specific video
val searchVideoChannel : kotlin.String = searchVideoChannel_example // kotlin.String | only list reports of a specific video channel
val videoIs : kotlin.String = videoIs_example // kotlin.String | only list deleted or blocklisted videos
val filter : kotlin.String = filter_example // kotlin.String | only list account, comment or video reports
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
try {
    val result : InlineResponse2005 = apiInstance.getAbuses(id, predefinedReason, search, state, searchReporter, searchReportee, searchVideo, searchVideoChannel, videoIs, filter, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#getAbuses")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#getAbuses")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| only list the report with this id | [optional]
 **predefinedReason** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| predefined reason the listed reports should contain | [optional] [enum: violentOrAbusive, hatefulOrAbusive, spamOrMisleading, privacy, rights, serverRules, thumbnails, captions]
 **search** | **kotlin.String**| plain search that will match with video titles, reporter names and more | [optional]
 **state** | [**AbuseStateSet**](.md)|  | [optional] [enum: 1, 2, 3]
 **searchReporter** | **kotlin.String**| only list reports of a specific reporter | [optional]
 **searchReportee** | **kotlin.String**| only list reports of a specific reportee | [optional]
 **searchVideo** | **kotlin.String**| only list reports of a specific video | [optional]
 **searchVideoChannel** | **kotlin.String**| only list reports of a specific video channel | [optional]
 **videoIs** | **kotlin.String**| only list deleted or blocklisted videos | [optional] [enum: deleted, blacklisted]
 **filter** | **kotlin.String**| only list account, comment or video reports | [optional] [enum: video, comment, account]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: -id, -createdAt, -state]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMyAbuses"></a>
# **getMyAbuses**
> InlineResponse2005 getMyAbuses(id, state, sort, start, count)

List my abuses

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AbusesApi()
val id : kotlin.Int = 56 // kotlin.Int | only list the report with this id
val state : AbuseStateSet =  // AbuseStateSet | 
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
try {
    val result : InlineResponse2005 = apiInstance.getMyAbuses(id, state, sort, start, count)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AbusesApi#getMyAbuses")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AbusesApi#getMyAbuses")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| only list the report with this id | [optional]
 **state** | [**AbuseStateSet**](.md)|  | [optional] [enum: 1, 2, 3]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: -id, -createdAt, -state]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

