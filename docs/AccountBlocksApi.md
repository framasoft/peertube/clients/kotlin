# AccountBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serverBlocklistAccountsAccountNameDelete**](AccountBlocksApi.md#serverBlocklistAccountsAccountNameDelete) | **DELETE** /server/blocklist/accounts/{accountName} | Unblock an account by its handle
[**serverBlocklistAccountsGet**](AccountBlocksApi.md#serverBlocklistAccountsGet) | **GET** /server/blocklist/accounts | List account blocks
[**serverBlocklistAccountsPost**](AccountBlocksApi.md#serverBlocklistAccountsPost) | **POST** /server/blocklist/accounts | Block an account


<a name="serverBlocklistAccountsAccountNameDelete"></a>
# **serverBlocklistAccountsAccountNameDelete**
> serverBlocklistAccountsAccountNameDelete(accountName)

Unblock an account by its handle

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountBlocksApi()
val accountName : kotlin.String = accountName_example // kotlin.String | account to unblock, in the form `username@domain`
try {
    apiInstance.serverBlocklistAccountsAccountNameDelete(accountName)
} catch (e: ClientException) {
    println("4xx response calling AccountBlocksApi#serverBlocklistAccountsAccountNameDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountBlocksApi#serverBlocklistAccountsAccountNameDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountName** | **kotlin.String**| account to unblock, in the form &#x60;username@domain&#x60; |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverBlocklistAccountsGet"></a>
# **serverBlocklistAccountsGet**
> serverBlocklistAccountsGet(start, count, sort)

List account blocks

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountBlocksApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    apiInstance.serverBlocklistAccountsGet(start, count, sort)
} catch (e: ClientException) {
    println("4xx response calling AccountBlocksApi#serverBlocklistAccountsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountBlocksApi#serverBlocklistAccountsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverBlocklistAccountsPost"></a>
# **serverBlocklistAccountsPost**
> serverBlocklistAccountsPost(inlineObject25)

Block an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountBlocksApi()
val inlineObject25 : InlineObject25 =  // InlineObject25 | 
try {
    apiInstance.serverBlocklistAccountsPost(inlineObject25)
} catch (e: ClientException) {
    println("4xx response calling AccountBlocksApi#serverBlocklistAccountsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountBlocksApi#serverBlocklistAccountsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject25** | [**InlineObject25**](InlineObject25.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

