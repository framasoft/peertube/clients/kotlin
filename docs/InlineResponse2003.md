
# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoQuotaUsed** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | The user video quota used so far in bytes |  [optional]
**videoQuotaUsedDaily** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | The user video quota used today in bytes |  [optional]



