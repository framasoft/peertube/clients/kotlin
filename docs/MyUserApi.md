# MyUserApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMyAbuses**](MyUserApi.md#getMyAbuses) | **GET** /users/me/abuses | List my abuses
[**getUserInfo**](MyUserApi.md#getUserInfo) | **GET** /users/me | Get my user information
[**putUserInfo**](MyUserApi.md#putUserInfo) | **PUT** /users/me | Update my user information
[**usersMeAvatarDelete**](MyUserApi.md#usersMeAvatarDelete) | **DELETE** /users/me/avatar | Delete my avatar
[**usersMeAvatarPickPost**](MyUserApi.md#usersMeAvatarPickPost) | **POST** /users/me/avatar/pick | Update my user avatar
[**usersMeVideoQuotaUsedGet**](MyUserApi.md#usersMeVideoQuotaUsedGet) | **GET** /users/me/video-quota-used | Get my user used quota
[**usersMeVideosGet**](MyUserApi.md#usersMeVideosGet) | **GET** /users/me/videos | Get videos of my user
[**usersMeVideosImportsGet**](MyUserApi.md#usersMeVideosImportsGet) | **GET** /users/me/videos/imports | Get video imports of my user
[**usersMeVideosVideoIdRatingGet**](MyUserApi.md#usersMeVideosVideoIdRatingGet) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video


<a name="getMyAbuses"></a>
# **getMyAbuses**
> InlineResponse2005 getMyAbuses(id, state, sort, start, count)

List my abuses

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val id : kotlin.Int = 56 // kotlin.Int | only list the report with this id
val state : AbuseStateSet =  // AbuseStateSet | 
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
try {
    val result : InlineResponse2005 = apiInstance.getMyAbuses(id, state, sort, start, count)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#getMyAbuses")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#getMyAbuses")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| only list the report with this id | [optional]
 **state** | [**AbuseStateSet**](.md)|  | [optional] [enum: 1, 2, 3]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: -id, -createdAt, -state]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserInfo"></a>
# **getUserInfo**
> kotlin.collections.List&lt;User&gt; getUserInfo()

Get my user information

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    val result : kotlin.collections.List<User> = apiInstance.getUserInfo()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#getUserInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#getUserInfo")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.collections.List&lt;User&gt;**](User.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putUserInfo"></a>
# **putUserInfo**
> putUserInfo(updateMe)

Update my user information

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val updateMe : UpdateMe =  // UpdateMe | 
try {
    apiInstance.putUserInfo(updateMe)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#putUserInfo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#putUserInfo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateMe** | [**UpdateMe**](UpdateMe.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersMeAvatarDelete"></a>
# **usersMeAvatarDelete**
> usersMeAvatarDelete()

Delete my avatar

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    apiInstance.usersMeAvatarDelete()
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeAvatarDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeAvatarDelete")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeAvatarPickPost"></a>
# **usersMeAvatarPickPost**
> InlineResponse2004 usersMeAvatarPickPost(avatarfile)

Update my user avatar

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val avatarfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload
try {
    val result : InlineResponse2004 = apiInstance.usersMeAvatarPickPost(avatarfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeAvatarPickPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeAvatarPickPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | **java.io.File**| The file to upload | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="usersMeVideoQuotaUsedGet"></a>
# **usersMeVideoQuotaUsedGet**
> InlineResponse2003 usersMeVideoQuotaUsedGet()

Get my user used quota

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    val result : InlineResponse2003 = apiInstance.usersMeVideoQuotaUsedGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideoQuotaUsedGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideoQuotaUsedGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosGet"></a>
# **usersMeVideosGet**
> VideoListResponse usersMeVideosGet(start, count, sort)

Get videos of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoListResponse = apiInstance.usersMeVideosGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosImportsGet"></a>
# **usersMeVideosImportsGet**
> VideoImportsList usersMeVideosImportsGet(start, count, sort)

Get video imports of my user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoImportsList = apiInstance.usersMeVideosImportsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosImportsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosImportsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoImportsList**](VideoImportsList.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosVideoIdRatingGet"></a>
# **usersMeVideosVideoIdRatingGet**
> GetMeVideoRating usersMeVideosVideoIdRatingGet(videoId)

Get rate of my user for a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val videoId : kotlin.Int = 56 // kotlin.Int | The video id
try {
    val result : GetMeVideoRating = apiInstance.usersMeVideosVideoIdRatingGet(videoId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **kotlin.Int**| The video id |

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

