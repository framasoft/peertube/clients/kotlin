
# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **kotlin.String** | content of the homepage, that will be injected in the client |  [optional]



