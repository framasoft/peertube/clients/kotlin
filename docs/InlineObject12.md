
# InlineObject12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**AbuseStateSet**](AbuseStateSet.md) |  |  [optional]
**moderationComment** | **kotlin.String** | Update the report comment visible only to the moderation team |  [optional]



