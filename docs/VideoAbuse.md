
# VideoAbuse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**reason** | **kotlin.String** |  |  [optional]
**predefinedReasons** | [**inline**](#kotlin.Array&lt;PredefinedReasonsEnum&gt;) |  |  [optional]
**reporterAccount** | [**Account**](Account.md) |  |  [optional]
**state** | [**AbuseStateConstant**](AbuseStateConstant.md) |  |  [optional]
**moderationComment** | **kotlin.String** |  |  [optional]
**video** | [**VideoAbuseVideo**](VideoAbuseVideo.md) |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]


<a name="kotlin.Array<PredefinedReasonsEnum>"></a>
## Enum: predefinedReasons
Name | Value
---- | -----
predefinedReasons | violentOrAbusive, hatefulOrAbusive, spamOrMisleading, privacy, rights, serverRules, thumbnails, captions



