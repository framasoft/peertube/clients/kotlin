
# AccountAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **kotlin.Int** | object id for the user tied to this account |  [optional]
**displayName** | **kotlin.String** | editable name of the account, displayed in its representations |  [optional]
**description** | **kotlin.String** | text or bio displayed on the account&#39;s profile |  [optional]



