
# VideoBlacklist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**videoId** | **kotlin.Int** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**duration** | **kotlin.Int** |  |  [optional]
**views** | **kotlin.Int** |  |  [optional]
**likes** | **kotlin.Int** |  |  [optional]
**dislikes** | **kotlin.Int** |  |  [optional]
**nsfw** | **kotlin.Boolean** |  |  [optional]



