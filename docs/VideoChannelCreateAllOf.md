
# VideoChannelCreateAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | username of the channel to create |  [optional]



