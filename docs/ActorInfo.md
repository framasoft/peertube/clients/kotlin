
# ActorInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**host** | **kotlin.String** |  |  [optional]
**avatar** | [**ActorInfoAvatar**](ActorInfoAvatar.md) |  |  [optional]



