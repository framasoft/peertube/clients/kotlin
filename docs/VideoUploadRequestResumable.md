
# VideoUploadRequestResumable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | Video name | 
**channelId** | **kotlin.Int** | Channel id that will contain this video | 
**filename** | **kotlin.String** | Video filename including extension | 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]
**category** | **kotlin.Int** | category id of the video (see [/videos/categories](#operation/getCategories)) |  [optional]
**licence** | **kotlin.Int** | licence id of the video (see [/videos/licences](#operation/getLicences)) |  [optional]
**language** | **kotlin.String** | language id of the video (see [/videos/languages](#operation/getLanguages)) |  [optional]
**description** | **kotlin.String** | Video description |  [optional]
**waitTranscoding** | **kotlin.Boolean** | Whether or not we wait transcoding before publish the video |  [optional]
**support** | **kotlin.String** | A text tell the audience how to support the video creator |  [optional]
**nsfw** | **kotlin.Boolean** | Whether or not this video contains sensitive content |  [optional]
**tags** | **kotlin.collections.Set&lt;kotlin.String&gt;** | Video tags (maximum 5 tags each between 2 and 30 characters) |  [optional]
**commentsEnabled** | **kotlin.Boolean** | Enable or disable comments for this video |  [optional]
**downloadEnabled** | **kotlin.Boolean** | Enable or disable downloading for this video |  [optional]
**originallyPublishedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | Date when the content was originally published |  [optional]
**scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Video thumbnail file |  [optional]
**previewfile** | [**java.io.File**](java.io.File.md) | Video preview file |  [optional]



