
# VideoRedundancyRedundancies

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**kotlin.collections.List&lt;FileRedundancyInformation&gt;**](FileRedundancyInformation.md) |  |  [optional]
**streamingPlaylists** | [**kotlin.collections.List&lt;FileRedundancyInformation&gt;**](FileRedundancyInformation.md) |  |  [optional]



