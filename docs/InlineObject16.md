
# InlineObject16

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoId** | **kotlin.Int** | Video to add in the playlist | 
**startTimestamp** | **kotlin.Int** | Start the video at this specific timestamp (in seconds) |  [optional]
**stopTimestamp** | **kotlin.Int** | Stop the video at this specific timestamp (in seconds) |  [optional]



