
# VideoChannel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **kotlin.String** | editable name of the channel, displayed in its representations |  [optional]
**description** | **kotlin.String** |  |  [optional]
**support** | **kotlin.String** | text shown by default on all videos of this channel, to tell the audience how to support it |  [optional]
**id** | **kotlin.Int** |  |  [optional] [readonly]
**isLocal** | **kotlin.Boolean** |  |  [optional] [readonly]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional] [readonly]
**ownerAccount** | [**VideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  |  [optional]



