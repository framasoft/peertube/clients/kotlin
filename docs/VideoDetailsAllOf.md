
# VideoDetailsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descriptionPath** | **kotlin.String** | path at which to get the full description of maximum &#x60;10000&#x60; characters |  [optional]
**support** | **kotlin.String** | A text tell the audience how to support the video creator |  [optional]
**channel** | [**VideoChannel**](VideoChannel.md) |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]
**tags** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**commentsEnabled** | **kotlin.Boolean** |  |  [optional]
**downloadEnabled** | **kotlin.Boolean** |  |  [optional]
**trackerUrls** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]
**files** | [**kotlin.collections.List&lt;VideoFile&gt;**](VideoFile.md) | WebTorrent/raw video files. If WebTorrent is disabled on the server:  - field will be empty - video files will be found in &#x60;streamingPlaylists[].files&#x60; field  |  [optional]
**streamingPlaylists** | [**kotlin.collections.List&lt;VideoStreamingPlaylists&gt;**](VideoStreamingPlaylists.md) | HLS playlists/manifest files. If HLS is disabled on the server:  - field will be empty - video files will be found in &#x60;files&#x60; field  |  [optional]



