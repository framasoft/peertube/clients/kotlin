
# ServerConfigVideoCaptionFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertySize** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  |  [optional]
**extensions** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



