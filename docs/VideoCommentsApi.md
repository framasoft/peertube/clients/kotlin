# VideoCommentsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosIdCommentThreadsGet**](VideoCommentsApi.md#videosIdCommentThreadsGet) | **GET** /videos/{id}/comment-threads | List threads of a video
[**videosIdCommentThreadsPost**](VideoCommentsApi.md#videosIdCommentThreadsPost) | **POST** /videos/{id}/comment-threads | Create a thread
[**videosIdCommentThreadsThreadIdGet**](VideoCommentsApi.md#videosIdCommentThreadsThreadIdGet) | **GET** /videos/{id}/comment-threads/{threadId} | Get a thread
[**videosIdCommentsCommentIdDelete**](VideoCommentsApi.md#videosIdCommentsCommentIdDelete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment or a reply
[**videosIdCommentsCommentIdPost**](VideoCommentsApi.md#videosIdCommentsCommentIdPost) | **POST** /videos/{id}/comments/{commentId} | Reply to a thread of a video


<a name="videosIdCommentThreadsGet"></a>
# **videosIdCommentThreadsGet**
> CommentThreadResponse videosIdCommentThreadsGet(id, start, count, sort)

List threads of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort comments by criteria
try {
    val result : CommentThreadResponse = apiInstance.videosIdCommentThreadsGet(id, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentsApi#videosIdCommentThreadsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentsApi#videosIdCommentThreadsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort comments by criteria | [optional] [enum: -createdAt, -totalReplies]

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdCommentThreadsPost"></a>
# **videosIdCommentThreadsPost**
> CommentThreadPostResponse videosIdCommentThreadsPost(id, inlineObject22)

Create a thread

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val inlineObject22 : InlineObject22 =  // InlineObject22 | 
try {
    val result : CommentThreadPostResponse = apiInstance.videosIdCommentThreadsPost(id, inlineObject22)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentsApi#videosIdCommentThreadsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentsApi#videosIdCommentThreadsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **inlineObject22** | [**InlineObject22**](InlineObject22.md)|  | [optional]

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="videosIdCommentThreadsThreadIdGet"></a>
# **videosIdCommentThreadsThreadIdGet**
> VideoCommentThreadTree videosIdCommentThreadsThreadIdGet(id, threadId)

Get a thread

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val threadId : kotlin.Int = 56 // kotlin.Int | The thread id (root comment id)
try {
    val result : VideoCommentThreadTree = apiInstance.videosIdCommentThreadsThreadIdGet(id, threadId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentsApi#videosIdCommentThreadsThreadIdGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentsApi#videosIdCommentThreadsThreadIdGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **threadId** | **kotlin.Int**| The thread id (root comment id) |

### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdCommentsCommentIdDelete"></a>
# **videosIdCommentsCommentIdDelete**
> videosIdCommentsCommentIdDelete(id, commentId)

Delete a comment or a reply

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val commentId : kotlin.Int = 56 // kotlin.Int | The comment id
try {
    apiInstance.videosIdCommentsCommentIdDelete(id, commentId)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentsApi#videosIdCommentsCommentIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentsApi#videosIdCommentsCommentIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **commentId** | **kotlin.Int**| The comment id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdCommentsCommentIdPost"></a>
# **videosIdCommentsCommentIdPost**
> CommentThreadPostResponse videosIdCommentsCommentIdPost(id, commentId, inlineObject23)

Reply to a thread of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCommentsApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val commentId : kotlin.Int = 56 // kotlin.Int | The comment id
val inlineObject23 : InlineObject23 =  // InlineObject23 | 
try {
    val result : CommentThreadPostResponse = apiInstance.videosIdCommentsCommentIdPost(id, commentId, inlineObject23)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCommentsApi#videosIdCommentsCommentIdPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCommentsApi#videosIdCommentsCommentIdPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **commentId** | **kotlin.Int**| The comment id |
 **inlineObject23** | [**InlineObject23**](InlineObject23.md)|  | [optional]

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

