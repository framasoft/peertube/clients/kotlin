# VideoUploadApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**importVideo**](VideoUploadApi.md#importVideo) | **POST** /videos/imports | Import a video
[**uploadLegacy**](VideoUploadApi.md#uploadLegacy) | **POST** /videos/upload | Upload a video
[**uploadResumable**](VideoUploadApi.md#uploadResumable) | **PUT** /videos/upload-resumable | Send chunk for the resumable upload of a video
[**uploadResumableCancel**](VideoUploadApi.md#uploadResumableCancel) | **DELETE** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
[**uploadResumableInit**](VideoUploadApi.md#uploadResumableInit) | **POST** /videos/upload-resumable | Initialize the resumable upload of a video


<a name="importVideo"></a>
# **importVideo**
> VideoUploadResponse importVideo(name, channelId, targetUrl, magnetUri, torrentfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile)

Import a video

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoUploadApi()
val name : kotlin.String = name_example // kotlin.String | Video name
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this video
val targetUrl : TargetUrl =  // TargetUrl | 
val magnetUri : MagnetUri =  // MagnetUri | 
val torrentfile : Torrentfile =  // Torrentfile | 
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.Boolean = true // kotlin.Boolean | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the video creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this video contains sensitive content
val tags : kotlin.collections.Set<kotlin.String> = tags_example // kotlin.collections.Set<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this video
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
try {
    val result : VideoUploadResponse = apiInstance.importVideo(name, channelId, targetUrl, magnetUri, torrentfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoUploadApi#importVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoUploadApi#importVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Video name |
 **channelId** | **kotlin.Int**| Channel id that will contain this video |
 **targetUrl** | [**TargetUrl**](TargetUrl.md)|  | [optional]
 **magnetUri** | [**MagnetUri**](MagnetUri.md)|  | [optional]
 **torrentfile** | [**Torrentfile**](Torrentfile.md)|  | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.Boolean**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the video creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.collections.Set&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this video | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadLegacy"></a>
# **uploadLegacy**
> VideoUploadResponse uploadLegacy(name, channelId, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile, videofile)

Upload a video

Uses a single request to upload a video.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoUploadApi()
val name : kotlin.String = name_example // kotlin.String | Video name
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this video
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.Boolean = true // kotlin.Boolean | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the video creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this video contains sensitive content
val tags : kotlin.collections.Set<kotlin.String> = tags_example // kotlin.collections.Set<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this video
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val videofile : java.io.File = BINARY_DATA_HERE // java.io.File | Video file
try {
    val result : VideoUploadResponse = apiInstance.uploadLegacy(name, channelId, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, downloadEnabled, originallyPublishedAt, scheduleUpdate, thumbnailfile, previewfile, videofile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoUploadApi#uploadLegacy")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoUploadApi#uploadLegacy")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| Video name | [optional]
 **channelId** | **kotlin.Int**| Channel id that will contain this video | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.Boolean**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the video creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.collections.Set&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this video | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **videofile** | **java.io.File**| Video file | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadResumable"></a>
# **uploadResumable**
> VideoUploadResponse uploadResumable(uploadId, contentRange, contentLength, body)

Send chunk for the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to continue, pause or resume the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoUploadApi()
val uploadId : kotlin.String = uploadId_example // kotlin.String | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload. 
val contentRange : kotlin.String = bytes 0-262143/2469036 // kotlin.String | Specifies the bytes in the file that the request is uploading.  For example, a value of `bytes 0-262143/1000000` shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file. 
val contentLength : java.math.BigDecimal = 262144 // java.math.BigDecimal | Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn't mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube's web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health. 
val body : java.io.File = BINARY_DATA_HERE // java.io.File | 
try {
    val result : VideoUploadResponse = apiInstance.uploadResumable(uploadId, contentRange, contentLength, body)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoUploadApi#uploadResumable")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoUploadApi#uploadResumable")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **kotlin.String**| Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  |
 **contentRange** | **kotlin.String**| Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  |
 **contentLength** | **java.math.BigDecimal**| Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  |
 **body** | **java.io.File**|  | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

<a name="uploadResumableCancel"></a>
# **uploadResumableCancel**
> uploadResumableCancel(uploadId, contentLength)

Cancel the resumable upload of a video, deleting any data uploaded so far

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to cancel the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoUploadApi()
val uploadId : kotlin.String = uploadId_example // kotlin.String | Created session id to proceed with. If you didn't send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-) 
val contentLength : java.math.BigDecimal = 0 // java.math.BigDecimal | 
try {
    apiInstance.uploadResumableCancel(uploadId, contentLength)
} catch (e: ClientException) {
    println("4xx response calling VideoUploadApi#uploadResumableCancel")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoUploadApi#uploadResumableCancel")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadId** | **kotlin.String**| Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  |
 **contentLength** | **java.math.BigDecimal**|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="uploadResumableInit"></a>
# **uploadResumableInit**
> uploadResumableInit(xUploadContentLength, xUploadContentType, videoUploadRequestResumable)

Initialize the resumable upload of a video

Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to initialize the upload of a video

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoUploadApi()
val xUploadContentLength : java.math.BigDecimal = 2469036 // java.math.BigDecimal | Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading.
val xUploadContentType : kotlin.String = video/mp4 // kotlin.String | MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary.
val videoUploadRequestResumable : VideoUploadRequestResumable =  // VideoUploadRequestResumable | 
try {
    apiInstance.uploadResumableInit(xUploadContentLength, xUploadContentType, videoUploadRequestResumable)
} catch (e: ClientException) {
    println("4xx response calling VideoUploadApi#uploadResumableInit")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoUploadApi#uploadResumableInit")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xUploadContentLength** | **java.math.BigDecimal**| Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. |
 **xUploadContentType** | **kotlin.String**| MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. |
 **videoUploadRequestResumable** | [**VideoUploadRequestResumable**](VideoUploadRequestResumable.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

