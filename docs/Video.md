
# Video

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | object id for the video |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) | universal identifier for the video, that can be used across instances |  [optional]
**shortUUID** | **kotlin.String** |  |  [optional]
**isLive** | **kotlin.Boolean** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | time at which the video object was first drafted |  [optional]
**publishedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | time at which the video was marked as ready for playback (with restrictions depending on &#x60;privacy&#x60;). Usually set after a &#x60;state&#x60; evolution. |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | last time the video&#39;s metadata was modified |  [optional]
**originallyPublishedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | used to represent a date of first publication, prior to the practical publication date of &#x60;publishedAt&#x60; |  [optional]
**category** | [**VideoConstantNumberMinusCategory**](VideoConstantNumberMinusCategory.md) | category in which the video is classified |  [optional]
**licence** | [**VideoConstantNumberMinusLicence**](VideoConstantNumberMinusLicence.md) | licence under which the video is distributed |  [optional]
**language** | [**VideoConstantStringMinusLanguage**](VideoConstantStringMinusLanguage.md) | main language used in the video |  [optional]
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) | privacy policy used to distribute the video |  [optional]
**description** | **kotlin.String** | truncated description of the video, written in Markdown. Resolve &#x60;descriptionPath&#x60; to get the full description of maximum &#x60;10000&#x60; characters.  |  [optional]
**duration** | **kotlin.Int** | duration of the video in seconds |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**name** | **kotlin.String** | title of the video |  [optional]
**thumbnailPath** | **kotlin.String** |  |  [optional]
**previewPath** | **kotlin.String** |  |  [optional]
**embedPath** | **kotlin.String** |  |  [optional]
**views** | **kotlin.Int** |  |  [optional]
**likes** | **kotlin.Int** |  |  [optional]
**dislikes** | **kotlin.Int** |  |  [optional]
**nsfw** | **kotlin.Boolean** |  |  [optional]
**waitTranscoding** | **kotlin.Boolean** |  |  [optional]
**state** | [**VideoStateConstant**](VideoStateConstant.md) | represents the internal state of the video processing within the PeerTube instance |  [optional]
**scheduledUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]
**blacklisted** | **kotlin.Boolean** |  |  [optional]
**blacklistedReason** | **kotlin.String** |  |  [optional]
**account** | [**AccountSummary**](AccountSummary.md) |  |  [optional]
**channel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  |  [optional]
**userHistory** | [**VideoUserHistory**](VideoUserHistory.md) |  |  [optional]



