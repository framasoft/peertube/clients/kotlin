# LiveVideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLive**](LiveVideosApi.md#addLive) | **POST** /videos/live | Create a live
[**getLiveId**](LiveVideosApi.md#getLiveId) | **GET** /videos/live/{id} | Get information about a live
[**updateLiveId**](LiveVideosApi.md#updateLiveId) | **PUT** /videos/live/{id} | Update information about a live


<a name="addLive"></a>
# **addLive**
> VideoUploadResponse addLive(channelId, name, saveReplay, permanentLive, thumbnailfile, previewfile, privacy, category, licence, language, description, support, nsfw, tags, commentsEnabled, downloadEnabled)

Create a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = LiveVideosApi()
val channelId : kotlin.Int = 56 // kotlin.Int | Channel id that will contain this live video
val name : kotlin.String = name_example // kotlin.String | Live video/replay name
val saveReplay : kotlin.Boolean = true // kotlin.Boolean | 
val permanentLive : kotlin.Boolean = true // kotlin.Boolean | User can stream multiple times in a permanent live
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Live video/replay thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Live video/replay preview file
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.Int = 56 // kotlin.Int | category id of the video (see [/videos/categories](#operation/getCategories))
val licence : kotlin.Int = 56 // kotlin.Int | licence id of the video (see [/videos/licences](#operation/getLicences))
val language : kotlin.String = language_example // kotlin.String | language id of the video (see [/videos/languages](#operation/getLanguages))
val description : kotlin.String = description_example // kotlin.String | Live video/replay description
val support : kotlin.String = support_example // kotlin.String | A text tell the audience how to support the creator
val nsfw : kotlin.Boolean = true // kotlin.Boolean | Whether or not this live video/replay contains sensitive content
val tags : kotlin.collections.List<kotlin.String> = tags_example // kotlin.collections.List<kotlin.String> | Live video/replay tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable comments for this live video/replay
val downloadEnabled : kotlin.Boolean = true // kotlin.Boolean | Enable or disable downloading for the replay of this live video
try {
    val result : VideoUploadResponse = apiInstance.addLive(channelId, name, saveReplay, permanentLive, thumbnailfile, previewfile, privacy, category, licence, language, description, support, nsfw, tags, commentsEnabled, downloadEnabled)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveVideosApi#addLive")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveVideosApi#addLive")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **kotlin.Int**| Channel id that will contain this live video |
 **name** | **kotlin.String**| Live video/replay name |
 **saveReplay** | **kotlin.Boolean**|  | [optional]
 **permanentLive** | **kotlin.Boolean**| User can stream multiple times in a permanent live | [optional]
 **thumbnailfile** | **java.io.File**| Live video/replay thumbnail file | [optional]
 **previewfile** | **java.io.File**| Live video/replay preview file | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3, 4]
 **category** | **kotlin.Int**| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **licence** | **kotlin.Int**| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **language** | **kotlin.String**| language id of the video (see [/videos/languages](#operation/getLanguages)) | [optional]
 **description** | **kotlin.String**| Live video/replay description | [optional]
 **support** | **kotlin.String**| A text tell the audience how to support the creator | [optional]
 **nsfw** | **kotlin.Boolean**| Whether or not this live video/replay contains sensitive content | [optional]
 **tags** | [**kotlin.collections.List&lt;kotlin.String&gt;**](kotlin.String.md)| Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.Boolean**| Enable or disable comments for this live video/replay | [optional]
 **downloadEnabled** | **kotlin.Boolean**| Enable or disable downloading for the replay of this live video | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="getLiveId"></a>
# **getLiveId**
> LiveVideoResponse getLiveId(id)

Get information about a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = LiveVideosApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
try {
    val result : LiveVideoResponse = apiInstance.getLiveId(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling LiveVideosApi#getLiveId")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveVideosApi#getLiveId")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |

### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateLiveId"></a>
# **updateLiveId**
> updateLiveId(id, liveVideoUpdate)

Update information about a live

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = LiveVideosApi()
val id : OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan =  // OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan | The object id, uuid or short uuid
val liveVideoUpdate : LiveVideoUpdate =  // LiveVideoUpdate | 
try {
    apiInstance.updateLiveId(id, liveVideoUpdate)
} catch (e: ClientException) {
    println("4xx response calling LiveVideosApi#updateLiveId")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling LiveVideosApi#updateLiveId")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan**](.md)| The object id, uuid or short uuid |
 **liveVideoUpdate** | [**LiveVideoUpdate**](LiveVideoUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

