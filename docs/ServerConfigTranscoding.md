
# ServerConfigTranscoding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**webtorrent** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**enabledResolutions** | **kotlin.collections.List&lt;kotlin.Int&gt;** |  |  [optional]



