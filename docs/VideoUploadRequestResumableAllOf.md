
# VideoUploadRequestResumableAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **kotlin.String** | Video filename including extension | 
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Video thumbnail file |  [optional]
**previewfile** | [**java.io.File**](java.io.File.md) | Video preview file |  [optional]



