
# ServerConfigAboutInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**shortDescription** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**terms** | **kotlin.String** |  |  [optional]



