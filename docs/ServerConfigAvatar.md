
# ServerConfigAvatar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**ServerConfigAvatarFile**](ServerConfigAvatarFile.md) |  |  [optional]
**extensions** | **kotlin.collections.List&lt;kotlin.String&gt;** |  |  [optional]



