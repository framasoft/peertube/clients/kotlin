
# NotificationComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**threadId** | **kotlin.Int** |  |  [optional]
**video** | [**VideoInfo**](VideoInfo.md) |  |  [optional]
**account** | [**ActorInfo**](ActorInfo.md) |  |  [optional]



