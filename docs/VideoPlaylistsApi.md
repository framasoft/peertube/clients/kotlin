# VideoPlaylistsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPlaylist**](VideoPlaylistsApi.md#addPlaylist) | **POST** /video-playlists | Create a video playlist
[**addVideoPlaylistVideo**](VideoPlaylistsApi.md#addVideoPlaylistVideo) | **POST** /video-playlists/{playlistId}/videos | Add a video in a playlist
[**delVideoPlaylistVideo**](VideoPlaylistsApi.md#delVideoPlaylistVideo) | **DELETE** /video-playlists/{playlistId}/videos/{playlistElementId} | Delete an element from a playlist
[**getPlaylistPrivacyPolicies**](VideoPlaylistsApi.md#getPlaylistPrivacyPolicies) | **GET** /video-playlists/privacies | List available playlist privacy policies
[**getPlaylists**](VideoPlaylistsApi.md#getPlaylists) | **GET** /video-playlists | List video playlists
[**getVideoPlaylistVideos**](VideoPlaylistsApi.md#getVideoPlaylistVideos) | **GET** /video-playlists/{playlistId}/videos | List videos of a playlist
[**putVideoPlaylistVideo**](VideoPlaylistsApi.md#putVideoPlaylistVideo) | **PUT** /video-playlists/{playlistId}/videos/{playlistElementId} | Update a playlist element
[**reorderVideoPlaylist**](VideoPlaylistsApi.md#reorderVideoPlaylist) | **POST** /video-playlists/{playlistId}/videos/reorder | Reorder a playlist
[**usersMeVideoPlaylistsVideosExistGet**](VideoPlaylistsApi.md#usersMeVideoPlaylistsVideosExistGet) | **GET** /users/me/video-playlists/videos-exist | Check video exists in my playlists
[**videoPlaylistsPlaylistIdDelete**](VideoPlaylistsApi.md#videoPlaylistsPlaylistIdDelete) | **DELETE** /video-playlists/{playlistId} | Delete a video playlist
[**videoPlaylistsPlaylistIdGet**](VideoPlaylistsApi.md#videoPlaylistsPlaylistIdGet) | **GET** /video-playlists/{playlistId} | Get a video playlist
[**videoPlaylistsPlaylistIdPut**](VideoPlaylistsApi.md#videoPlaylistsPlaylistIdPut) | **PUT** /video-playlists/{playlistId} | Update a video playlist


<a name="addPlaylist"></a>
# **addPlaylist**
> InlineResponse20012 addPlaylist(displayName, thumbnailfile, privacy, description, videoChannelId)

Create a video playlist

If the video playlist is set as public, &#x60;videoChannelId&#x60; is mandatory.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val displayName : kotlin.String = displayName_example // kotlin.String | Video playlist display name
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video playlist thumbnail file
val privacy : VideoPlaylistPrivacySet =  // VideoPlaylistPrivacySet | 
val description : kotlin.String = description_example // kotlin.String | Video playlist description
val videoChannelId : kotlin.Int =  // kotlin.Int | Video channel in which the playlist will be published
try {
    val result : InlineResponse20012 = apiInstance.addPlaylist(displayName, thumbnailfile, privacy, description, videoChannelId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#addPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#addPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **displayName** | **kotlin.String**| Video playlist display name |
 **thumbnailfile** | **java.io.File**| Video playlist thumbnail file | [optional]
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] [enum: 1, 2, 3]
 **description** | **kotlin.String**| Video playlist description | [optional]
 **videoChannelId** | [**kotlin.Int**](kotlin.Int.md)| Video channel in which the playlist will be published | [optional]

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="addVideoPlaylistVideo"></a>
# **addVideoPlaylistVideo**
> InlineResponse20013 addVideoPlaylistVideo(playlistId, inlineObject19)

Add a video in a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val inlineObject19 : InlineObject19 =  // InlineObject19 | 
try {
    val result : InlineResponse20013 = apiInstance.addVideoPlaylistVideo(playlistId, inlineObject19)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#addVideoPlaylistVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#addVideoPlaylistVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **inlineObject19** | [**InlineObject19**](InlineObject19.md)|  | [optional]

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="delVideoPlaylistVideo"></a>
# **delVideoPlaylistVideo**
> delVideoPlaylistVideo(playlistId, playlistElementId)

Delete an element from a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val playlistElementId : kotlin.Int = 56 // kotlin.Int | Playlist element id
try {
    apiInstance.delVideoPlaylistVideo(playlistId, playlistElementId)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#delVideoPlaylistVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#delVideoPlaylistVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **playlistElementId** | **kotlin.Int**| Playlist element id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPlaylistPrivacyPolicies"></a>
# **getPlaylistPrivacyPolicies**
> kotlin.collections.List&lt;kotlin.String&gt; getPlaylistPrivacyPolicies()

List available playlist privacy policies

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
try {
    val result : kotlin.collections.List<kotlin.String> = apiInstance.getPlaylistPrivacyPolicies()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#getPlaylistPrivacyPolicies")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#getPlaylistPrivacyPolicies")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.collections.List&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPlaylists"></a>
# **getPlaylists**
> InlineResponse20011 getPlaylists(start, count, sort)

List video playlists

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : InlineResponse20011 = apiInstance.getPlaylists(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#getPlaylists")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#getPlaylists")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getVideoPlaylistVideos"></a>
# **getVideoPlaylistVideos**
> VideoListResponse getVideoPlaylistVideos(playlistId)

List videos of a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
try {
    val result : VideoListResponse = apiInstance.getVideoPlaylistVideos(playlistId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#getVideoPlaylistVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#getVideoPlaylistVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putVideoPlaylistVideo"></a>
# **putVideoPlaylistVideo**
> putVideoPlaylistVideo(playlistId, playlistElementId, inlineObject21)

Update a playlist element

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val playlistElementId : kotlin.Int = 56 // kotlin.Int | Playlist element id
val inlineObject21 : InlineObject21 =  // InlineObject21 | 
try {
    apiInstance.putVideoPlaylistVideo(playlistId, playlistElementId, inlineObject21)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#putVideoPlaylistVideo")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#putVideoPlaylistVideo")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **playlistElementId** | **kotlin.Int**| Playlist element id |
 **inlineObject21** | [**InlineObject21**](InlineObject21.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="reorderVideoPlaylist"></a>
# **reorderVideoPlaylist**
> reorderVideoPlaylist(playlistId, inlineObject20)

Reorder a playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val inlineObject20 : InlineObject20 =  // InlineObject20 | 
try {
    apiInstance.reorderVideoPlaylist(playlistId, inlineObject20)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#reorderVideoPlaylist")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#reorderVideoPlaylist")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **inlineObject20** | [**InlineObject20**](InlineObject20.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersMeVideoPlaylistsVideosExistGet"></a>
# **usersMeVideoPlaylistsVideosExistGet**
> InlineResponse20014 usersMeVideoPlaylistsVideosExistGet(videoIds)

Check video exists in my playlists

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val videoIds : kotlin.collections.List<kotlin.Int> =  // kotlin.collections.List<kotlin.Int> | The video ids to check
try {
    val result : InlineResponse20014 = apiInstance.usersMeVideoPlaylistsVideosExistGet(videoIds)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#usersMeVideoPlaylistsVideosExistGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#usersMeVideoPlaylistsVideosExistGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoIds** | [**kotlin.collections.List&lt;kotlin.Int&gt;**](kotlin.Int.md)| The video ids to check |

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoPlaylistsPlaylistIdDelete"></a>
# **videoPlaylistsPlaylistIdDelete**
> videoPlaylistsPlaylistIdDelete(playlistId)

Delete a video playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
try {
    apiInstance.videoPlaylistsPlaylistIdDelete(playlistId)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videoPlaylistsPlaylistIdGet"></a>
# **videoPlaylistsPlaylistIdGet**
> VideoPlaylist videoPlaylistsPlaylistIdGet(playlistId)

Get a video playlist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
try {
    val result : VideoPlaylist = apiInstance.videoPlaylistsPlaylistIdGet(playlistId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |

### Return type

[**VideoPlaylist**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoPlaylistsPlaylistIdPut"></a>
# **videoPlaylistsPlaylistIdPut**
> videoPlaylistsPlaylistIdPut(playlistId, displayName, thumbnailfile, privacy, description, videoChannelId)

Update a video playlist

If the video playlist is set as public, the playlist must have a assigned channel.

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoPlaylistsApi()
val playlistId : kotlin.Int = 56 // kotlin.Int | Playlist id
val displayName : kotlin.String = displayName_example // kotlin.String | Video playlist display name
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video playlist thumbnail file
val privacy : VideoPlaylistPrivacySet =  // VideoPlaylistPrivacySet | 
val description : kotlin.String = description_example // kotlin.String | Video playlist description
val videoChannelId : kotlin.Int =  // kotlin.Int | Video channel in which the playlist will be published
try {
    apiInstance.videoPlaylistsPlaylistIdPut(playlistId, displayName, thumbnailfile, privacy, description, videoChannelId)
} catch (e: ClientException) {
    println("4xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoPlaylistsApi#videoPlaylistsPlaylistIdPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playlistId** | **kotlin.Int**| Playlist id |
 **displayName** | **kotlin.String**| Video playlist display name | [optional]
 **thumbnailfile** | **java.io.File**| Video playlist thumbnail file | [optional]
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] [enum: 1, 2, 3]
 **description** | **kotlin.String**| Video playlist description | [optional]
 **videoChannelId** | [**kotlin.Int**](kotlin.Int.md)| Video channel in which the playlist will be published | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

