
# VideoRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**Video**](Video.md) |  | 
**rating** | [**inline**](#RatingEnum) | Rating of the video | 


<a name="RatingEnum"></a>
## Enum: rating
Name | Value
---- | -----
rating | like, dislike, none



