
# InlineResponse20014

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoId** | [**kotlin.collections.List&lt;InlineResponse20014VideoId&gt;**](InlineResponse20014VideoId.md) |  |  [optional]



