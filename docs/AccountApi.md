# AccountApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSyndicatedSubscriptionVideos**](AccountApi.md#getSyndicatedSubscriptionVideos) | **GET** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token


<a name="getSyndicatedSubscriptionVideos"></a>
# **getSyndicatedSubscriptionVideos**
> kotlin.collections.List&lt;kotlin.Any&gt; getSyndicatedSubscriptionVideos(format, accountId, token, sort, nsfw, filter)

List videos of subscriptions tied to a token

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountApi()
val format : kotlin.String = format_example // kotlin.String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
val accountId : kotlin.String = accountId_example // kotlin.String | limit listing to a specific account
val token : kotlin.String = token_example // kotlin.String | private token allowing access
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
try {
    val result : kotlin.collections.List<kotlin.Any> = apiInstance.getSyndicatedSubscriptionVideos(format, accountId, token, sort, nsfw, filter)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountApi#getSyndicatedSubscriptionVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountApi#getSyndicatedSubscriptionVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **kotlin.String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | [enum: xml, rss, rss2, atom, atom1, json, json1]
 **accountId** | **kotlin.String**| limit listing to a specific account |
 **token** | **kotlin.String**| private token allowing access |
 **sort** | **kotlin.String**| Sort column | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]

### Return type

[**kotlin.collections.List&lt;kotlin.Any&gt;**](kotlin.Any.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

