
# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** | immutable name of the actor, used to find or mention it |  [optional]
**host** | **kotlin.String** | server on which the actor is resident |  [optional]
**hostRedundancyAllowed** | **kotlin.Boolean** | whether this actor&#39;s host allows redundancy of its videos |  [optional]
**followingCount** | **kotlin.Int** | number of actors subscribed to by this actor, as seen by this instance |  [optional]
**followersCount** | **kotlin.Int** | number of followers of this actor, as seen by this instance |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**avatar** | [**ActorImage**](ActorImage.md) |  |  [optional]
**userId** | **kotlin.Int** | object id for the user tied to this account |  [optional]
**displayName** | **kotlin.String** | editable name of the account, displayed in its representations |  [optional]
**description** | **kotlin.String** | text or bio displayed on the account&#39;s profile |  [optional]



