
# VideoConstantNumberMinusLicence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | licence id of the video (see [/videos/licences](#operation/getLicences)) |  [optional]
**label** | **kotlin.String** |  |  [optional]



