
# AbuseMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**message** | **kotlin.String** |  |  [optional]
**byModerator** | **kotlin.Boolean** |  |  [optional]
**createdAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) |  |  [optional]
**account** | [**AccountSummary**](AccountSummary.md) |  |  [optional]



