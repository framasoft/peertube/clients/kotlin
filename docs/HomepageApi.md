# HomepageApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customPagesHomepageInstanceGet**](HomepageApi.md#customPagesHomepageInstanceGet) | **GET** /custom-pages/homepage/instance | Get instance custom homepage
[**customPagesHomepageInstancePut**](HomepageApi.md#customPagesHomepageInstancePut) | **PUT** /custom-pages/homepage/instance | Set instance custom homepage


<a name="customPagesHomepageInstanceGet"></a>
# **customPagesHomepageInstanceGet**
> CustomHomepage customPagesHomepageInstanceGet()

Get instance custom homepage

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = HomepageApi()
try {
    val result : CustomHomepage = apiInstance.customPagesHomepageInstanceGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling HomepageApi#customPagesHomepageInstanceGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HomepageApi#customPagesHomepageInstanceGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CustomHomepage**](CustomHomepage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="customPagesHomepageInstancePut"></a>
# **customPagesHomepageInstancePut**
> customPagesHomepageInstancePut(inlineObject)

Set instance custom homepage

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = HomepageApi()
val inlineObject : InlineObject =  // InlineObject | 
try {
    apiInstance.customPagesHomepageInstancePut(inlineObject)
} catch (e: ClientException) {
    println("4xx response calling HomepageApi#customPagesHomepageInstancePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling HomepageApi#customPagesHomepageInstancePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject** | [**InlineObject**](InlineObject.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

