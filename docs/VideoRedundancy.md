
# VideoRedundancy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**uuid** | [**java.util.UUID**](java.util.UUID.md) |  |  [optional]
**redundancies** | [**VideoRedundancyRedundancies**](VideoRedundancyRedundancies.md) |  |  [optional]



