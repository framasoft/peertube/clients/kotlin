# VideoAbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosAbuseGet**](VideoAbusesApi.md#videosAbuseGet) | **GET** /videos/abuse | List video abuses
[**videosIdAbuseAbuseIdDelete**](VideoAbusesApi.md#videosIdAbuseAbuseIdDelete) | **DELETE** /videos/{id}/abuse/{abuseId} | Delete an abuse
[**videosIdAbuseAbuseIdPut**](VideoAbusesApi.md#videosIdAbuseAbuseIdPut) | **PUT** /videos/{id}/abuse/{abuseId} | Update an abuse
[**videosIdAbusePost**](VideoAbusesApi.md#videosIdAbusePost) | **POST** /videos/{id}/abuse | Report an abuse


<a name="videosAbuseGet"></a>
# **videosAbuseGet**
> kotlin.Array&lt;VideoAbuse&gt; videosAbuseGet(id, predefinedReason, search, state, searchReporter, searchReportee, searchVideo, searchVideoChannel, start, count, sort)

List video abuses

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbusesApi()
val id : kotlin.Int = 56 // kotlin.Int | only list the report with this id
val predefinedReason : kotlin.String = predefinedReason_example // kotlin.String | predefined reason the listed reports should contain
val search : kotlin.String = search_example // kotlin.String | plain search that will match with video titles, reporter names and more
val state : kotlin.Int = 56 // kotlin.Int | The video playlist privacy (Pending = `1`, Rejected = `2`, Accepted = `3`)
val searchReporter : kotlin.String = searchReporter_example // kotlin.String | only list reports of a specific reporter
val searchReportee : kotlin.String = searchReportee_example // kotlin.String | only list reports of a specific reportee
val searchVideo : kotlin.String = searchVideo_example // kotlin.String | only list reports of a specific video
val searchVideoChannel : kotlin.String = searchVideoChannel_example // kotlin.String | only list reports of a specific video channel
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
try {
    val result : kotlin.Array<VideoAbuse> = apiInstance.videosAbuseGet(id, predefinedReason, search, state, searchReporter, searchReportee, searchVideo, searchVideoChannel, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoAbusesApi#videosAbuseGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbusesApi#videosAbuseGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.Int**| only list the report with this id | [optional]
 **predefinedReason** | **kotlin.String**| predefined reason the listed reports should contain | [optional] [enum: violentOrAbusive, hatefulOrAbusive, spamOrMisleading, privacy, rights, serverRules, thumbnails, captions]
 **search** | **kotlin.String**| plain search that will match with video titles, reporter names and more | [optional]
 **state** | **kotlin.Int**| The video playlist privacy (Pending &#x3D; &#x60;1&#x60;, Rejected &#x3D; &#x60;2&#x60;, Accepted &#x3D; &#x60;3&#x60;) | [optional] [enum: 1, 2, 3]
 **searchReporter** | **kotlin.String**| only list reports of a specific reporter | [optional]
 **searchReportee** | **kotlin.String**| only list reports of a specific reportee | [optional]
 **searchVideo** | **kotlin.String**| only list reports of a specific video | [optional]
 **searchVideoChannel** | **kotlin.String**| only list reports of a specific video channel | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: -id, -createdAt, -state]

### Return type

[**kotlin.Array&lt;VideoAbuse&gt;**](VideoAbuse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdAbuseAbuseIdDelete"></a>
# **videosIdAbuseAbuseIdDelete**
> videosIdAbuseAbuseIdDelete(id, abuseId)

Delete an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbusesApi()
val id : OneOfLessThanIntegerCommaUUIDGreaterThan =  // OneOfLessThanIntegerCommaUUIDGreaterThan | The object id or uuid
val abuseId : kotlin.Int = 56 // kotlin.Int | Video abuse id
try {
    apiInstance.videosIdAbuseAbuseIdDelete(id, abuseId)
} catch (e: ClientException) {
    println("4xx response calling VideoAbusesApi#videosIdAbuseAbuseIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbusesApi#videosIdAbuseAbuseIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDGreaterThan**](.md)| The object id or uuid |
 **abuseId** | **kotlin.Int**| Video abuse id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdAbuseAbuseIdPut"></a>
# **videosIdAbuseAbuseIdPut**
> videosIdAbuseAbuseIdPut(id, abuseId, inlineObject10)

Update an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbusesApi()
val id : OneOfLessThanIntegerCommaUUIDGreaterThan =  // OneOfLessThanIntegerCommaUUIDGreaterThan | The object id or uuid
val abuseId : kotlin.Int = 56 // kotlin.Int | Video abuse id
val inlineObject10 : InlineObject10 =  // InlineObject10 | 
try {
    apiInstance.videosIdAbuseAbuseIdPut(id, abuseId, inlineObject10)
} catch (e: ClientException) {
    println("4xx response calling VideoAbusesApi#videosIdAbuseAbuseIdPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbusesApi#videosIdAbuseAbuseIdPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDGreaterThan**](.md)| The object id or uuid |
 **abuseId** | **kotlin.Int**| Video abuse id |
 **inlineObject10** | [**InlineObject10**](InlineObject10.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="videosIdAbusePost"></a>
# **videosIdAbusePost**
> videosIdAbusePost(id, inlineObject9)

Report an abuse

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbusesApi()
val id : OneOfLessThanIntegerCommaUUIDGreaterThan =  // OneOfLessThanIntegerCommaUUIDGreaterThan | The object id or uuid
val inlineObject9 : InlineObject9 =  // InlineObject9 | 
try {
    apiInstance.videosIdAbusePost(id, inlineObject9)
} catch (e: ClientException) {
    println("4xx response calling VideoAbusesApi#videosIdAbusePost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbusesApi#videosIdAbusePost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfLessThanIntegerCommaUUIDGreaterThan**](.md)| The object id or uuid |
 **inlineObject9** | [**InlineObject9**](InlineObject9.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

