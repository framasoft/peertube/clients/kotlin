
# GetMeVideoRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  | 
**rating** | [**inline**](#RatingEnum) | Rating of the video | 


<a name="RatingEnum"></a>
## Enum: rating
Name | Value
---- | -----
rating | like, dislike, none



