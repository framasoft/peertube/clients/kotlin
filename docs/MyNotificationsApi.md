# MyNotificationsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMeNotificationSettingsPut**](MyNotificationsApi.md#usersMeNotificationSettingsPut) | **PUT** /users/me/notification-settings | Update my notification settings
[**usersMeNotificationsGet**](MyNotificationsApi.md#usersMeNotificationsGet) | **GET** /users/me/notifications | List my notifications
[**usersMeNotificationsReadAllPost**](MyNotificationsApi.md#usersMeNotificationsReadAllPost) | **POST** /users/me/notifications/read-all | Mark all my notification as read
[**usersMeNotificationsReadPost**](MyNotificationsApi.md#usersMeNotificationsReadPost) | **POST** /users/me/notifications/read | Mark notifications as read by their id


<a name="usersMeNotificationSettingsPut"></a>
# **usersMeNotificationSettingsPut**
> usersMeNotificationSettingsPut(inlineObject5)

Update my notification settings

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyNotificationsApi()
val inlineObject5 : InlineObject5 =  // InlineObject5 | 
try {
    apiInstance.usersMeNotificationSettingsPut(inlineObject5)
} catch (e: ClientException) {
    println("4xx response calling MyNotificationsApi#usersMeNotificationSettingsPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyNotificationsApi#usersMeNotificationSettingsPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject5** | [**InlineObject5**](InlineObject5.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersMeNotificationsGet"></a>
# **usersMeNotificationsGet**
> NotificationListResponse usersMeNotificationsGet(unread, start, count, sort)

List my notifications

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyNotificationsApi()
val unread : kotlin.Boolean = true // kotlin.Boolean | only list unread notifications
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : NotificationListResponse = apiInstance.usersMeNotificationsGet(unread, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyNotificationsApi#usersMeNotificationsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyNotificationsApi#usersMeNotificationsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **kotlin.Boolean**| only list unread notifications | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**NotificationListResponse**](NotificationListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeNotificationsReadAllPost"></a>
# **usersMeNotificationsReadAllPost**
> usersMeNotificationsReadAllPost()

Mark all my notification as read

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyNotificationsApi()
try {
    apiInstance.usersMeNotificationsReadAllPost()
} catch (e: ClientException) {
    println("4xx response calling MyNotificationsApi#usersMeNotificationsReadAllPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyNotificationsApi#usersMeNotificationsReadAllPost")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeNotificationsReadPost"></a>
# **usersMeNotificationsReadPost**
> usersMeNotificationsReadPost(inlineObject4)

Mark notifications as read by their id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyNotificationsApi()
val inlineObject4 : InlineObject4 =  // InlineObject4 | 
try {
    apiInstance.usersMeNotificationsReadPost(inlineObject4)
} catch (e: ClientException) {
    println("4xx response calling MyNotificationsApi#usersMeNotificationsReadPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyNotificationsApi#usersMeNotificationsReadPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject4** | [**InlineObject4**](InlineObject4.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

