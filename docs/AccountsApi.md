# AccountsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsNameRatingsGet**](AccountsApi.md#accountsNameRatingsGet) | **GET** /accounts/{name}/ratings | List ratings of an account
[**accountsNameVideoChannelsGet**](AccountsApi.md#accountsNameVideoChannelsGet) | **GET** /accounts/{name}/video-channels | List video channels of an account
[**getAccount**](AccountsApi.md#getAccount) | **GET** /accounts/{name} | Get an account
[**getAccountVideos**](AccountsApi.md#getAccountVideos) | **GET** /accounts/{name}/videos | List videos of an account
[**getAccounts**](AccountsApi.md#getAccounts) | **GET** /accounts | List accounts


<a name="accountsNameRatingsGet"></a>
# **accountsNameRatingsGet**
> kotlin.collections.List&lt;VideoRating&gt; accountsNameRatingsGet(name, start, count, sort, rating)

List ratings of an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
val rating : kotlin.String = rating_example // kotlin.String | Optionally filter which ratings to retrieve
try {
    val result : kotlin.collections.List<VideoRating> = apiInstance.accountsNameRatingsGet(name, start, count, sort, rating)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#accountsNameRatingsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#accountsNameRatingsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]
 **rating** | **kotlin.String**| Optionally filter which ratings to retrieve | [optional] [enum: like, dislike]

### Return type

[**kotlin.collections.List&lt;VideoRating&gt;**](VideoRating.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="accountsNameVideoChannelsGet"></a>
# **accountsNameVideoChannelsGet**
> VideoChannelList accountsNameVideoChannelsGet(name, withStats, start, count, sort)

List video channels of an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
val withStats : kotlin.Boolean = true // kotlin.Boolean | include view statistics for the last 30 days (only if authentified as the account user)
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : VideoChannelList = apiInstance.accountsNameVideoChannelsGet(name, withStats, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |
 **withStats** | **kotlin.Boolean**| include view statistics for the last 30 days (only if authentified as the account user) | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**VideoChannelList**](VideoChannelList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccount"></a>
# **getAccount**
> Account getAccount(name)

Get an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
try {
    val result : Account = apiInstance.getAccount(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#getAccount")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#getAccount")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccountVideos"></a>
# **getAccountVideos**
> VideoListResponse getAccountVideos(name, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)

List videos of an account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = chocobozzz | chocobozzz@example.org // kotlin.String | The username or handle of the account
val categoryOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | category id of the video (see [/videos/categories](#operation/getCategories))
val isLive : kotlin.Boolean = true // kotlin.Boolean | whether or not the video is a live
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanIntegerCommaArrayGreaterThan =  // OneOfLessThanIntegerCommaArrayGreaterThan | licence id of the video (see [/videos/licences](#operation/getLicences))
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video (see [/videos/languages](#operation/getLanguages)). Use `_unknown` to filter on videos that don't have a video language
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges) 
val skipCount : kotlin.String = skipCount_example // kotlin.String | if you don't need the `total` in the response
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.getAccountVideos(name, categoryOneOf, isLive, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, skipCount, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#getAccountVideos")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#getAccountVideos")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The username or handle of the account |
 **categoryOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| category id of the video (see [/videos/categories](#operation/getCategories)) | [optional]
 **isLive** | **kotlin.Boolean**| whether or not the video is a live | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanIntegerCommaArrayGreaterThan**](.md)| licence id of the video (see [/videos/licences](#operation/getLicences)) | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **skipCount** | **kotlin.String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to false] [enum: true, false]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: name, -duration, -createdAt, -publishedAt, -views, -likes, -trending, -hot]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccounts"></a>
# **getAccounts**
> kotlin.collections.List&lt;Account&gt; getAccounts(start, count, sort)

List accounts

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : kotlin.collections.List<Account> = apiInstance.getAccounts(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#getAccounts")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#getAccounts")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**kotlin.collections.List&lt;Account&gt;**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

