
# VideoStreamingPlaylistsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**type** | [**inline**](#TypeEnum) | Playlist type: - &#x60;1&#x60;: HLS  |  [optional]


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | 1



