
# InlineObject9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **kotlin.Int** | Channel id that will contain this live video | 
**name** | **kotlin.String** | Live video/replay name | 
**saveReplay** | **kotlin.Boolean** |  |  [optional]
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Live video/replay thumbnail file |  [optional]
**previewfile** | [**java.io.File**](java.io.File.md) | Live video/replay preview file |  [optional]
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]
**category** | **kotlin.String** | Live video/replay category |  [optional]
**licence** | **kotlin.String** | Live video/replay licence |  [optional]
**language** | **kotlin.String** | Live video/replay language |  [optional]
**description** | **kotlin.String** | Live video/replay description |  [optional]
**support** | **kotlin.String** | A text tell the audience how to support the creator |  [optional]
**nsfw** | **kotlin.Boolean** | Whether or not this live video/replay contains sensitive content |  [optional]
**tags** | **kotlin.collections.List&lt;kotlin.String&gt;** | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) |  [optional]
**commentsEnabled** | **kotlin.Boolean** | Enable or disable comments for this live video/replay |  [optional]
**downloadEnabled** | **kotlin.Boolean** | Enable or disable downloading for the replay of this live |  [optional]



