
# InlineResponse20013

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoPlaylistElement** | [**InlineResponse20013VideoPlaylistElement**](InlineResponse20013VideoPlaylistElement.md) |  |  [optional]



