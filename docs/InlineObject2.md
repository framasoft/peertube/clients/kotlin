
# InlineObject2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verificationString** | **kotlin.String** |  | 
**isPendingEmail** | **kotlin.Boolean** |  |  [optional]



