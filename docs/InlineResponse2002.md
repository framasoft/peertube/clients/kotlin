
# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokenType** | **kotlin.String** |  |  [optional]
**accessToken** | **kotlin.String** | valid for 1 day |  [optional]
**refreshToken** | **kotlin.String** | valid for 2 weeks |  [optional]
**expiresIn** | **kotlin.Int** |  |  [optional]
**refreshTokenExpiresIn** | **kotlin.Int** |  |  [optional]



