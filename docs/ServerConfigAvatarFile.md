
# ServerConfigAvatarFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertySize** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  |  [optional]



