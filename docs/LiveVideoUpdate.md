
# LiveVideoUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saveReplay** | **kotlin.Boolean** |  |  [optional]
**permanentLive** | **kotlin.Boolean** | User can stream multiple times in a permanent live |  [optional]



