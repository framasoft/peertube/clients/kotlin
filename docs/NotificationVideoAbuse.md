
# NotificationVideoAbuse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**video** | [**VideoInfo**](VideoInfo.md) |  |  [optional]



