
# VideoImportStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**inline**](#IdEnum) | The video import state (Pending &#x3D; &#x60;1&#x60;, Success &#x3D; &#x60;2&#x60;, Failed &#x3D; &#x60;3&#x60;) |  [optional]
**label** | **kotlin.String** |  |  [optional]


<a name="IdEnum"></a>
## Enum: id
Name | Value
---- | -----
id | 1, 2, 3



