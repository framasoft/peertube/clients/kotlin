
# VideoPlaylistPrivacyConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



