
# NotificationActorFollow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** |  |  [optional]
**follower** | [**ActorInfo**](ActorInfo.md) |  |  [optional]
**state** | [**inline**](#StateEnum) |  |  [optional]
**following** | [**NotificationActorFollowFollowing**](NotificationActorFollowFollowing.md) |  |  [optional]


<a name="StateEnum"></a>
## Enum: state
Name | Value
---- | -----
state | pending, accepted



