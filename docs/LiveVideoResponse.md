
# LiveVideoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rtmpUrl** | **kotlin.String** |  |  [optional]
**streamKey** | **kotlin.String** | RTMP stream key to use to stream into this live video |  [optional]
**saveReplay** | **kotlin.Boolean** |  |  [optional]
**permanentLive** | **kotlin.Boolean** | User can stream multiple times in a permanent live |  [optional]



