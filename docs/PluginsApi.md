# PluginsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPlugin**](PluginsApi.md#addPlugin) | **POST** /plugins/install | Install a plugin
[**getAvailablePlugins**](PluginsApi.md#getAvailablePlugins) | **GET** /plugins/available | List available plugins
[**getPlugin**](PluginsApi.md#getPlugin) | **GET** /plugins/{npmName} | Get a plugin
[**getPlugins**](PluginsApi.md#getPlugins) | **GET** /plugins | List plugins
[**pluginsNpmNamePublicSettingsGet**](PluginsApi.md#pluginsNpmNamePublicSettingsGet) | **GET** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
[**pluginsNpmNameRegisteredSettingsGet**](PluginsApi.md#pluginsNpmNameRegisteredSettingsGet) | **GET** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
[**pluginsNpmNameSettingsPut**](PluginsApi.md#pluginsNpmNameSettingsPut) | **PUT** /plugins/{npmName}/settings | Set a plugin&#39;s settings
[**uninstallPlugin**](PluginsApi.md#uninstallPlugin) | **POST** /plugins/uninstall | Uninstall a plugin
[**updatePlugin**](PluginsApi.md#updatePlugin) | **POST** /plugins/update | Update a plugin


<a name="addPlugin"></a>
# **addPlugin**
> addPlugin(UNKNOWN_BASE_TYPE)

Install a plugin

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val UNKNOWN_BASE_TYPE : UNKNOWN_BASE_TYPE =  // UNKNOWN_BASE_TYPE | 
try {
    apiInstance.addPlugin(UNKNOWN_BASE_TYPE)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#addPlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#addPlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_BASE_TYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="getAvailablePlugins"></a>
# **getAvailablePlugins**
> PluginResponse getAvailablePlugins(search, pluginType, currentPeerTubeEngine, start, count, sort)

List available plugins

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val search : kotlin.String = search_example // kotlin.String | 
val pluginType : kotlin.Int = 56 // kotlin.Int | 
val currentPeerTubeEngine : kotlin.String = currentPeerTubeEngine_example // kotlin.String | 
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : PluginResponse = apiInstance.getAvailablePlugins(search, pluginType, currentPeerTubeEngine, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getAvailablePlugins")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getAvailablePlugins")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **kotlin.String**|  | [optional]
 **pluginType** | **kotlin.Int**|  | [optional]
 **currentPeerTubeEngine** | **kotlin.String**|  | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPlugin"></a>
# **getPlugin**
> Plugin getPlugin(npmName)

Get a plugin

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val npmName : kotlin.String = peertube-plugin-auth-ldap // kotlin.String | name of the plugin/theme on npmjs.com or in its package.json
try {
    val result : Plugin = apiInstance.getPlugin(npmName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npmName** | **kotlin.String**| name of the plugin/theme on npmjs.com or in its package.json |

### Return type

[**Plugin**](Plugin.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPlugins"></a>
# **getPlugins**
> PluginResponse getPlugins(pluginType, uninstalled, start, count, sort)

List plugins

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val pluginType : kotlin.Int = 56 // kotlin.Int | 
val uninstalled : kotlin.Boolean = true // kotlin.Boolean | 
val start : kotlin.Int = 56 // kotlin.Int | Offset used to paginate results
val count : kotlin.Int = 56 // kotlin.Int | Number of items to return
val sort : kotlin.String = -createdAt // kotlin.String | Sort column
try {
    val result : PluginResponse = apiInstance.getPlugins(pluginType, uninstalled, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#getPlugins")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#getPlugins")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pluginType** | **kotlin.Int**|  | [optional]
 **uninstalled** | **kotlin.Boolean**|  | [optional]
 **start** | **kotlin.Int**| Offset used to paginate results | [optional]
 **count** | **kotlin.Int**| Number of items to return | [optional] [default to 15]
 **sort** | **kotlin.String**| Sort column | [optional]

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="pluginsNpmNamePublicSettingsGet"></a>
# **pluginsNpmNamePublicSettingsGet**
> kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt; pluginsNpmNamePublicSettingsGet(npmName)

Get a plugin&#39;s public settings

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val npmName : kotlin.String = peertube-plugin-auth-ldap // kotlin.String | name of the plugin/theme on npmjs.com or in its package.json
try {
    val result : kotlin.collections.Map<kotlin.String, kotlin.Any> = apiInstance.pluginsNpmNamePublicSettingsGet(npmName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#pluginsNpmNamePublicSettingsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#pluginsNpmNamePublicSettingsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npmName** | **kotlin.String**| name of the plugin/theme on npmjs.com or in its package.json |

### Return type

[**kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt;**](kotlin.Any.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="pluginsNpmNameRegisteredSettingsGet"></a>
# **pluginsNpmNameRegisteredSettingsGet**
> kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt; pluginsNpmNameRegisteredSettingsGet(npmName)

Get a plugin&#39;s registered settings

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val npmName : kotlin.String = peertube-plugin-auth-ldap // kotlin.String | name of the plugin/theme on npmjs.com or in its package.json
try {
    val result : kotlin.collections.Map<kotlin.String, kotlin.Any> = apiInstance.pluginsNpmNameRegisteredSettingsGet(npmName)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#pluginsNpmNameRegisteredSettingsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#pluginsNpmNameRegisteredSettingsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npmName** | **kotlin.String**| name of the plugin/theme on npmjs.com or in its package.json |

### Return type

[**kotlin.collections.Map&lt;kotlin.String, kotlin.Any&gt;**](kotlin.Any.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="pluginsNpmNameSettingsPut"></a>
# **pluginsNpmNameSettingsPut**
> pluginsNpmNameSettingsPut(npmName, inlineObject30)

Set a plugin&#39;s settings

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val npmName : kotlin.String = peertube-plugin-auth-ldap // kotlin.String | name of the plugin/theme on npmjs.com or in its package.json
val inlineObject30 : InlineObject30 =  // InlineObject30 | 
try {
    apiInstance.pluginsNpmNameSettingsPut(npmName, inlineObject30)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#pluginsNpmNameSettingsPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#pluginsNpmNameSettingsPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npmName** | **kotlin.String**| name of the plugin/theme on npmjs.com or in its package.json |
 **inlineObject30** | [**InlineObject30**](InlineObject30.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="uninstallPlugin"></a>
# **uninstallPlugin**
> uninstallPlugin(inlineObject29)

Uninstall a plugin

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val inlineObject29 : InlineObject29 =  // InlineObject29 | 
try {
    apiInstance.uninstallPlugin(inlineObject29)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#uninstallPlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#uninstallPlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject29** | [**InlineObject29**](InlineObject29.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="updatePlugin"></a>
# **updatePlugin**
> updatePlugin(UNKNOWN_BASE_TYPE)

Update a plugin

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = PluginsApi()
val UNKNOWN_BASE_TYPE : UNKNOWN_BASE_TYPE =  // UNKNOWN_BASE_TYPE | 
try {
    apiInstance.updatePlugin(UNKNOWN_BASE_TYPE)
} catch (e: ClientException) {
    println("4xx response calling PluginsApi#updatePlugin")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PluginsApi#updatePlugin")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_BASE_TYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

