
# ServerConfigCustomSignup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **kotlin.Boolean** |  |  [optional]
**limit** | **kotlin.Int** |  |  [optional]
**requiresEmailVerification** | **kotlin.Boolean** |  |  [optional]



