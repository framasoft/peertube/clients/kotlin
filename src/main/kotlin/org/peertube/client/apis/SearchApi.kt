/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.InlineResponse20011
import org.peertube.client.models.OneOfLessThanIntegerCommaArrayGreaterThan
import org.peertube.client.models.OneOfLessThanStringCommaArrayGreaterThan
import org.peertube.client.models.VideoChannelList
import org.peertube.client.models.VideoListResponse

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class SearchApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * Search channels
    * 
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete channel information and interact with it.  
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort column (optional)
    * @return VideoChannelList
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun searchChannels(search: kotlin.String, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?) : VideoChannelList {
        val localVariableConfig = searchChannelsRequestConfig(search = search, start = start, count = count, searchTarget = searchTarget, sort = sort)

        val localVarResponse = request<Unit, VideoChannelList>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoChannelList
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation searchChannels
    *
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete channel information and interact with it.  
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort column (optional)
    * @return RequestConfig
    */
    fun searchChannelsRequestConfig(search: kotlin.String, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("search", listOf(search.toString()))
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (searchTarget != null) {
                    put("searchTarget", listOf(searchTarget.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/search/video-channels",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Search playlists
    * 
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete playlist information and interact with it.  
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort column (optional)
    * @return InlineResponse20011
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun searchPlaylists(search: kotlin.String, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?) : InlineResponse20011 {
        val localVariableConfig = searchPlaylistsRequestConfig(search = search, start = start, count = count, searchTarget = searchTarget, sort = sort)

        val localVarResponse = request<Unit, InlineResponse20011>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse20011
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation searchPlaylists
    *
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete playlist information and interact with it.  
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort column (optional)
    * @return RequestConfig
    */
    fun searchPlaylistsRequestConfig(search: kotlin.String, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("search", listOf(search.toString()))
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (searchTarget != null) {
                    put("searchTarget", listOf(searchTarget.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/search/video-playlists",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Search videos
    * 
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete video information and interact with it.  
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort videos by criteria (optional)
    * @param startDate Get videos that are published after this date (optional)
    * @param endDate Get videos that are published before this date (optional)
    * @param originallyPublishedStartDate Get videos that are originally published after this date (optional)
    * @param originallyPublishedEndDate Get videos that are originally published before this date (optional)
    * @param durationMin Get videos that have this minimum duration (optional)
    * @param durationMax Get videos that have this maximum duration (optional)
    * @return VideoListResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun searchVideos(search: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?, startDate: java.time.OffsetDateTime?, endDate: java.time.OffsetDateTime?, originallyPublishedStartDate: java.time.OffsetDateTime?, originallyPublishedEndDate: java.time.OffsetDateTime?, durationMin: kotlin.Int?, durationMax: kotlin.Int?) : VideoListResponse {
        val localVariableConfig = searchVideosRequestConfig(search = search, categoryOneOf = categoryOneOf, isLive = isLive, tagsOneOf = tagsOneOf, tagsAllOf = tagsAllOf, licenceOneOf = licenceOneOf, languageOneOf = languageOneOf, nsfw = nsfw, filter = filter, skipCount = skipCount, start = start, count = count, searchTarget = searchTarget, sort = sort, startDate = startDate, endDate = endDate, originallyPublishedStartDate = originallyPublishedStartDate, originallyPublishedEndDate = originallyPublishedEndDate, durationMin = durationMin, durationMax = durationMax)

        val localVarResponse = request<Unit, VideoListResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoListResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation searchVideos
    *
    * @param search String to search. If the user can make a remote URI search, and the string is an URI then the PeerTube instance will fetch the remote object and add it to its database. Then, you can use the REST API to fetch the complete video information and interact with it.  
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param searchTarget If the administrator enabled search index support, you can override the default search target.  **Warning**: If you choose to make an index search, PeerTube will get results from a third party service. It means the instance may not yet know the objects you fetched. If you want to load video/channel information:   * If the current user has the ability to make a remote URI search (this information is available in the config endpoint),   then reuse the search API to make a search using the object URI so PeerTube instance fetches the remote object and fill its database.   After that, you can use the classic REST API endpoints to fetch the complete object or interact with it   * If the current user doesn&#39;t have the ability to make a remote URI search, then redirect the user on the origin instance or fetch   the data from the origin instance API  (optional)
    * @param sort Sort videos by criteria (optional)
    * @param startDate Get videos that are published after this date (optional)
    * @param endDate Get videos that are published before this date (optional)
    * @param originallyPublishedStartDate Get videos that are originally published after this date (optional)
    * @param originallyPublishedEndDate Get videos that are originally published before this date (optional)
    * @param durationMin Get videos that have this minimum duration (optional)
    * @param durationMax Get videos that have this maximum duration (optional)
    * @return RequestConfig
    */
    fun searchVideosRequestConfig(search: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, searchTarget: kotlin.String?, sort: kotlin.String?, startDate: java.time.OffsetDateTime?, endDate: java.time.OffsetDateTime?, originallyPublishedStartDate: java.time.OffsetDateTime?, originallyPublishedEndDate: java.time.OffsetDateTime?, durationMin: kotlin.Int?, durationMax: kotlin.Int?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("search", listOf(search.toString()))
                if (categoryOneOf != null) {
                    put("categoryOneOf", listOf(categoryOneOf.toString()))
                }
                if (isLive != null) {
                    put("isLive", listOf(isLive.toString()))
                }
                if (tagsOneOf != null) {
                    put("tagsOneOf", listOf(tagsOneOf.toString()))
                }
                if (tagsAllOf != null) {
                    put("tagsAllOf", listOf(tagsAllOf.toString()))
                }
                if (licenceOneOf != null) {
                    put("licenceOneOf", listOf(licenceOneOf.toString()))
                }
                if (languageOneOf != null) {
                    put("languageOneOf", listOf(languageOneOf.toString()))
                }
                if (nsfw != null) {
                    put("nsfw", listOf(nsfw.toString()))
                }
                if (filter != null) {
                    put("filter", listOf(filter.toString()))
                }
                if (skipCount != null) {
                    put("skipCount", listOf(skipCount.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (searchTarget != null) {
                    put("searchTarget", listOf(searchTarget.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
                if (startDate != null) {
                    put("startDate", listOf(parseDateToQueryString(startDate)))
                }
                if (endDate != null) {
                    put("endDate", listOf(parseDateToQueryString(endDate)))
                }
                if (originallyPublishedStartDate != null) {
                    put("originallyPublishedStartDate", listOf(parseDateToQueryString(originallyPublishedStartDate)))
                }
                if (originallyPublishedEndDate != null) {
                    put("originallyPublishedEndDate", listOf(parseDateToQueryString(originallyPublishedEndDate)))
                }
                if (durationMin != null) {
                    put("durationMin", listOf(durationMin.toString()))
                }
                if (durationMax != null) {
                    put("durationMax", listOf(durationMax.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/search/videos",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
