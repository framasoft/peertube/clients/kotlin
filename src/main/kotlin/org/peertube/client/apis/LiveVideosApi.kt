/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.LiveVideoResponse
import org.peertube.client.models.LiveVideoUpdate
import org.peertube.client.models.OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan
import org.peertube.client.models.VideoPrivacySet
import org.peertube.client.models.VideoUploadResponse

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class LiveVideosApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * Create a live
    * 
    * @param channelId Channel id that will contain this live video 
    * @param name Live video/replay name 
    * @param saveReplay  (optional)
    * @param permanentLive User can stream multiple times in a permanent live (optional)
    * @param thumbnailfile Live video/replay thumbnail file (optional)
    * @param previewfile Live video/replay preview file (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Live video/replay description (optional)
    * @param support A text tell the audience how to support the creator (optional)
    * @param nsfw Whether or not this live video/replay contains sensitive content (optional)
    * @param tags Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this live video/replay (optional)
    * @param downloadEnabled Enable or disable downloading for the replay of this live video (optional)
    * @return VideoUploadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun addLive(channelId: kotlin.Int, name: kotlin.String, saveReplay: kotlin.Boolean?, permanentLive: kotlin.Boolean?, thumbnailfile: java.io.File?, previewfile: java.io.File?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?) : VideoUploadResponse {
        val localVariableConfig = addLiveRequestConfig(channelId = channelId, name = name, saveReplay = saveReplay, permanentLive = permanentLive, thumbnailfile = thumbnailfile, previewfile = previewfile, privacy = privacy, category = category, licence = licence, language = language, description = description, support = support, nsfw = nsfw, tags = tags, commentsEnabled = commentsEnabled, downloadEnabled = downloadEnabled)

        val localVarResponse = request<Map<String, Any?>, VideoUploadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoUploadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation addLive
    *
    * @param channelId Channel id that will contain this live video 
    * @param name Live video/replay name 
    * @param saveReplay  (optional)
    * @param permanentLive User can stream multiple times in a permanent live (optional)
    * @param thumbnailfile Live video/replay thumbnail file (optional)
    * @param previewfile Live video/replay preview file (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Live video/replay description (optional)
    * @param support A text tell the audience how to support the creator (optional)
    * @param nsfw Whether or not this live video/replay contains sensitive content (optional)
    * @param tags Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this live video/replay (optional)
    * @param downloadEnabled Enable or disable downloading for the replay of this live video (optional)
    * @return RequestConfig
    */
    fun addLiveRequestConfig(channelId: kotlin.Int, name: kotlin.String, saveReplay: kotlin.Boolean?, permanentLive: kotlin.Boolean?, thumbnailfile: java.io.File?, previewfile: java.io.File?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("channelId" to channelId, "saveReplay" to saveReplay, "permanentLive" to permanentLive, "thumbnailfile" to thumbnailfile, "previewfile" to previewfile, "privacy" to privacy, "category" to category, "licence" to licence, "language" to language, "description" to description, "support" to support, "nsfw" to nsfw, "name" to name, "tags" to tags, "commentsEnabled" to commentsEnabled, "downloadEnabled" to downloadEnabled)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/live",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get information about a live
    * 
    * @param id The object id, uuid or short uuid 
    * @return LiveVideoResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getLiveId(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : LiveVideoResponse {
        val localVariableConfig = getLiveIdRequestConfig(id = id)

        val localVarResponse = request<Unit, LiveVideoResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as LiveVideoResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getLiveId
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun getLiveIdRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/live/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update information about a live
    * 
    * @param id The object id, uuid or short uuid 
    * @param liveVideoUpdate  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun updateLiveId(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, liveVideoUpdate: LiveVideoUpdate?) : Unit {
        val localVariableConfig = updateLiveIdRequestConfig(id = id, liveVideoUpdate = liveVideoUpdate)

        val localVarResponse = request<LiveVideoUpdate, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation updateLiveId
    *
    * @param id The object id, uuid or short uuid 
    * @param liveVideoUpdate  (optional)
    * @return RequestConfig
    */
    fun updateLiveIdRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, liveVideoUpdate: LiveVideoUpdate?) : RequestConfig<LiveVideoUpdate> {
        val localVariableBody = liveVideoUpdate
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/videos/live/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
