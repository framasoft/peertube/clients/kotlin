/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.CommentThreadPostResponse
import org.peertube.client.models.CommentThreadResponse
import org.peertube.client.models.InlineObject22
import org.peertube.client.models.InlineObject23
import org.peertube.client.models.OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan
import org.peertube.client.models.VideoCommentThreadTree

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class VideoCommentsApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * List threads of a video
    * 
    * @param id The object id, uuid or short uuid 
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort comments by criteria (optional)
    * @return CommentThreadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videosIdCommentThreadsGet(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : CommentThreadResponse {
        val localVariableConfig = videosIdCommentThreadsGetRequestConfig(id = id, start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, CommentThreadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as CommentThreadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videosIdCommentThreadsGet
    *
    * @param id The object id, uuid or short uuid 
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort comments by criteria (optional)
    * @return RequestConfig
    */
    fun videosIdCommentThreadsGetRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/{id}/comment-threads".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Create a thread
    * 
    * @param id The object id, uuid or short uuid 
    * @param inlineObject22  (optional)
    * @return CommentThreadPostResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videosIdCommentThreadsPost(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, inlineObject22: InlineObject22?) : CommentThreadPostResponse {
        val localVariableConfig = videosIdCommentThreadsPostRequestConfig(id = id, inlineObject22 = inlineObject22)

        val localVarResponse = request<InlineObject22, CommentThreadPostResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as CommentThreadPostResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videosIdCommentThreadsPost
    *
    * @param id The object id, uuid or short uuid 
    * @param inlineObject22  (optional)
    * @return RequestConfig
    */
    fun videosIdCommentThreadsPostRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, inlineObject22: InlineObject22?) : RequestConfig<InlineObject22> {
        val localVariableBody = inlineObject22
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/{id}/comment-threads".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get a thread
    * 
    * @param id The object id, uuid or short uuid 
    * @param threadId The thread id (root comment id) 
    * @return VideoCommentThreadTree
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videosIdCommentThreadsThreadIdGet(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, threadId: kotlin.Int) : VideoCommentThreadTree {
        val localVariableConfig = videosIdCommentThreadsThreadIdGetRequestConfig(id = id, threadId = threadId)

        val localVarResponse = request<Unit, VideoCommentThreadTree>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoCommentThreadTree
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videosIdCommentThreadsThreadIdGet
    *
    * @param id The object id, uuid or short uuid 
    * @param threadId The thread id (root comment id) 
    * @return RequestConfig
    */
    fun videosIdCommentThreadsThreadIdGetRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, threadId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/{id}/comment-threads/{threadId}".replace("{"+"id"+"}", "$id").replace("{"+"threadId"+"}", "$threadId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Delete a comment or a reply
    * 
    * @param id The object id, uuid or short uuid 
    * @param commentId The comment id 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videosIdCommentsCommentIdDelete(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, commentId: kotlin.Int) : Unit {
        val localVariableConfig = videosIdCommentsCommentIdDeleteRequestConfig(id = id, commentId = commentId)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videosIdCommentsCommentIdDelete
    *
    * @param id The object id, uuid or short uuid 
    * @param commentId The comment id 
    * @return RequestConfig
    */
    fun videosIdCommentsCommentIdDeleteRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, commentId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/videos/{id}/comments/{commentId}".replace("{"+"id"+"}", "$id").replace("{"+"commentId"+"}", "$commentId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Reply to a thread of a video
    * 
    * @param id The object id, uuid or short uuid 
    * @param commentId The comment id 
    * @param inlineObject23  (optional)
    * @return CommentThreadPostResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videosIdCommentsCommentIdPost(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, commentId: kotlin.Int, inlineObject23: InlineObject23?) : CommentThreadPostResponse {
        val localVariableConfig = videosIdCommentsCommentIdPostRequestConfig(id = id, commentId = commentId, inlineObject23 = inlineObject23)

        val localVarResponse = request<InlineObject23, CommentThreadPostResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as CommentThreadPostResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videosIdCommentsCommentIdPost
    *
    * @param id The object id, uuid or short uuid 
    * @param commentId The comment id 
    * @param inlineObject23  (optional)
    * @return RequestConfig
    */
    fun videosIdCommentsCommentIdPostRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, commentId: kotlin.Int, inlineObject23: InlineObject23?) : RequestConfig<InlineObject23> {
        val localVariableBody = inlineObject23
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/{id}/comments/{commentId}".replace("{"+"id"+"}", "$id").replace("{"+"commentId"+"}", "$commentId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
