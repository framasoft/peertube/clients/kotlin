/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.LiveVideoResponse
import org.peertube.client.models.LiveVideoUpdate
import org.peertube.client.models.MagnetUri
import org.peertube.client.models.OneOfLessThanIntegerCommaArrayGreaterThan
import org.peertube.client.models.OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan
import org.peertube.client.models.OneOfLessThanStringCommaArrayGreaterThan
import org.peertube.client.models.TargetUrl
import org.peertube.client.models.Torrentfile
import org.peertube.client.models.UserWatchingVideo
import org.peertube.client.models.VideoDetails
import org.peertube.client.models.VideoListResponse
import org.peertube.client.models.VideoPrivacySet
import org.peertube.client.models.VideoScheduledUpdate
import org.peertube.client.models.VideoUploadRequestResumable
import org.peertube.client.models.VideoUploadResponse

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class VideoApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * Create a live
    * 
    * @param channelId Channel id that will contain this live video 
    * @param name Live video/replay name 
    * @param saveReplay  (optional)
    * @param permanentLive User can stream multiple times in a permanent live (optional)
    * @param thumbnailfile Live video/replay thumbnail file (optional)
    * @param previewfile Live video/replay preview file (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Live video/replay description (optional)
    * @param support A text tell the audience how to support the creator (optional)
    * @param nsfw Whether or not this live video/replay contains sensitive content (optional)
    * @param tags Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this live video/replay (optional)
    * @param downloadEnabled Enable or disable downloading for the replay of this live video (optional)
    * @return VideoUploadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun addLive(channelId: kotlin.Int, name: kotlin.String, saveReplay: kotlin.Boolean?, permanentLive: kotlin.Boolean?, thumbnailfile: java.io.File?, previewfile: java.io.File?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?) : VideoUploadResponse {
        val localVariableConfig = addLiveRequestConfig(channelId = channelId, name = name, saveReplay = saveReplay, permanentLive = permanentLive, thumbnailfile = thumbnailfile, previewfile = previewfile, privacy = privacy, category = category, licence = licence, language = language, description = description, support = support, nsfw = nsfw, tags = tags, commentsEnabled = commentsEnabled, downloadEnabled = downloadEnabled)

        val localVarResponse = request<Map<String, Any?>, VideoUploadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoUploadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation addLive
    *
    * @param channelId Channel id that will contain this live video 
    * @param name Live video/replay name 
    * @param saveReplay  (optional)
    * @param permanentLive User can stream multiple times in a permanent live (optional)
    * @param thumbnailfile Live video/replay thumbnail file (optional)
    * @param previewfile Live video/replay preview file (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Live video/replay description (optional)
    * @param support A text tell the audience how to support the creator (optional)
    * @param nsfw Whether or not this live video/replay contains sensitive content (optional)
    * @param tags Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this live video/replay (optional)
    * @param downloadEnabled Enable or disable downloading for the replay of this live video (optional)
    * @return RequestConfig
    */
    fun addLiveRequestConfig(channelId: kotlin.Int, name: kotlin.String, saveReplay: kotlin.Boolean?, permanentLive: kotlin.Boolean?, thumbnailfile: java.io.File?, previewfile: java.io.File?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("channelId" to channelId, "saveReplay" to saveReplay, "permanentLive" to permanentLive, "thumbnailfile" to thumbnailfile, "previewfile" to previewfile, "privacy" to privacy, "category" to category, "licence" to licence, "language" to language, "description" to description, "support" to support, "nsfw" to nsfw, "name" to name, "tags" to tags, "commentsEnabled" to commentsEnabled, "downloadEnabled" to downloadEnabled)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/live",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Add a view to a video
    * 
    * @param id The object id, uuid or short uuid 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun addView(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : Unit {
        val localVariableConfig = addViewRequestConfig(id = id)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation addView
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun addViewRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/{id}/views".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Delete a video
    * 
    * @param id The object id, uuid or short uuid 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun delVideo(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : Unit {
        val localVariableConfig = delVideoRequestConfig(id = id)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation delVideo
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun delVideoRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/videos/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List videos of an account
    * 
    * @param name The username or handle of the account 
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return VideoListResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getAccountVideos(name: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : VideoListResponse {
        val localVariableConfig = getAccountVideosRequestConfig(name = name, categoryOneOf = categoryOneOf, isLive = isLive, tagsOneOf = tagsOneOf, tagsAllOf = tagsAllOf, licenceOneOf = licenceOneOf, languageOneOf = languageOneOf, nsfw = nsfw, filter = filter, skipCount = skipCount, start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, VideoListResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoListResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getAccountVideos
    *
    * @param name The username or handle of the account 
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return RequestConfig
    */
    fun getAccountVideosRequestConfig(name: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (categoryOneOf != null) {
                    put("categoryOneOf", listOf(categoryOneOf.toString()))
                }
                if (isLive != null) {
                    put("isLive", listOf(isLive.toString()))
                }
                if (tagsOneOf != null) {
                    put("tagsOneOf", listOf(tagsOneOf.toString()))
                }
                if (tagsAllOf != null) {
                    put("tagsAllOf", listOf(tagsAllOf.toString()))
                }
                if (licenceOneOf != null) {
                    put("licenceOneOf", listOf(licenceOneOf.toString()))
                }
                if (languageOneOf != null) {
                    put("languageOneOf", listOf(languageOneOf.toString()))
                }
                if (nsfw != null) {
                    put("nsfw", listOf(nsfw.toString()))
                }
                if (filter != null) {
                    put("filter", listOf(filter.toString()))
                }
                if (skipCount != null) {
                    put("skipCount", listOf(skipCount.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/accounts/{name}/videos".replace("{"+"name"+"}", "$name"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List available video categories
    * 
    * @return kotlin.collections.List<kotlin.String>
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getCategories() : kotlin.collections.List<kotlin.String> {
        val localVariableConfig = getCategoriesRequestConfig()

        val localVarResponse = request<Unit, kotlin.collections.List<kotlin.String>>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.collections.List<kotlin.String>
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getCategories
    *
    * @return RequestConfig
    */
    fun getCategoriesRequestConfig() : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/categories",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List available video languages
    * 
    * @return kotlin.collections.List<kotlin.String>
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getLanguages() : kotlin.collections.List<kotlin.String> {
        val localVariableConfig = getLanguagesRequestConfig()

        val localVarResponse = request<Unit, kotlin.collections.List<kotlin.String>>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.collections.List<kotlin.String>
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getLanguages
    *
    * @return RequestConfig
    */
    fun getLanguagesRequestConfig() : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/languages",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List available video licences
    * 
    * @return kotlin.collections.List<kotlin.String>
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getLicences() : kotlin.collections.List<kotlin.String> {
        val localVariableConfig = getLicencesRequestConfig()

        val localVarResponse = request<Unit, kotlin.collections.List<kotlin.String>>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.collections.List<kotlin.String>
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getLicences
    *
    * @return RequestConfig
    */
    fun getLicencesRequestConfig() : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/licences",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get information about a live
    * 
    * @param id The object id, uuid or short uuid 
    * @return LiveVideoResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getLiveId(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : LiveVideoResponse {
        val localVariableConfig = getLiveIdRequestConfig(id = id)

        val localVarResponse = request<Unit, LiveVideoResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as LiveVideoResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getLiveId
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun getLiveIdRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/live/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List available video privacy policies
    * 
    * @return kotlin.collections.List<kotlin.String>
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getPrivacyPolicies() : kotlin.collections.List<kotlin.String> {
        val localVariableConfig = getPrivacyPoliciesRequestConfig()

        val localVarResponse = request<Unit, kotlin.collections.List<kotlin.String>>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.collections.List<kotlin.String>
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getPrivacyPolicies
    *
    * @return RequestConfig
    */
    fun getPrivacyPoliciesRequestConfig() : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/privacies",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get a video
    * 
    * @param id The object id, uuid or short uuid 
    * @return VideoDetails
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getVideo(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : VideoDetails {
        val localVariableConfig = getVideoRequestConfig(id = id)

        val localVarResponse = request<Unit, VideoDetails>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoDetails
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getVideo
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun getVideoRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List videos of a video channel
    * 
    * @param channelHandle The video channel handle 
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return VideoListResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getVideoChannelVideos(channelHandle: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : VideoListResponse {
        val localVariableConfig = getVideoChannelVideosRequestConfig(channelHandle = channelHandle, categoryOneOf = categoryOneOf, isLive = isLive, tagsOneOf = tagsOneOf, tagsAllOf = tagsAllOf, licenceOneOf = licenceOneOf, languageOneOf = languageOneOf, nsfw = nsfw, filter = filter, skipCount = skipCount, start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, VideoListResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoListResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getVideoChannelVideos
    *
    * @param channelHandle The video channel handle 
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return RequestConfig
    */
    fun getVideoChannelVideosRequestConfig(channelHandle: kotlin.String, categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (categoryOneOf != null) {
                    put("categoryOneOf", listOf(categoryOneOf.toString()))
                }
                if (isLive != null) {
                    put("isLive", listOf(isLive.toString()))
                }
                if (tagsOneOf != null) {
                    put("tagsOneOf", listOf(tagsOneOf.toString()))
                }
                if (tagsAllOf != null) {
                    put("tagsAllOf", listOf(tagsAllOf.toString()))
                }
                if (licenceOneOf != null) {
                    put("licenceOneOf", listOf(licenceOneOf.toString()))
                }
                if (languageOneOf != null) {
                    put("languageOneOf", listOf(languageOneOf.toString()))
                }
                if (nsfw != null) {
                    put("nsfw", listOf(nsfw.toString()))
                }
                if (filter != null) {
                    put("filter", listOf(filter.toString()))
                }
                if (skipCount != null) {
                    put("skipCount", listOf(skipCount.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/video-channels/{channelHandle}/videos".replace("{"+"channelHandle"+"}", "$channelHandle"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get complete video description
    * 
    * @param id The object id, uuid or short uuid 
    * @return kotlin.String
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getVideoDesc(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : kotlin.String {
        val localVariableConfig = getVideoDescRequestConfig(id = id)

        val localVarResponse = request<Unit, kotlin.String>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.String
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getVideoDesc
    *
    * @param id The object id, uuid or short uuid 
    * @return RequestConfig
    */
    fun getVideoDescRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos/{id}/description".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List videos
    * 
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return VideoListResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getVideos(categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : VideoListResponse {
        val localVariableConfig = getVideosRequestConfig(categoryOneOf = categoryOneOf, isLive = isLive, tagsOneOf = tagsOneOf, tagsAllOf = tagsAllOf, licenceOneOf = licenceOneOf, languageOneOf = languageOneOf, nsfw = nsfw, filter = filter, skipCount = skipCount, start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, VideoListResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoListResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getVideos
    *
    * @param categoryOneOf category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param isLive whether or not the video is a live (optional)
    * @param tagsOneOf tag(s) of the video (optional)
    * @param tagsAllOf tag(s) of the video, where all should be present in the video (optional)
    * @param licenceOneOf licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param languageOneOf language id of the video (see [/videos/languages](#operation/getLanguages)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language (optional)
    * @param nsfw whether to include nsfw videos, if any (optional)
    * @param filter Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
    * @param skipCount if you don&#39;t need the &#x60;total&#x60; in the response (optional, default to false)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort videos by criteria (optional)
    * @return RequestConfig
    */
    fun getVideosRequestConfig(categoryOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, isLive: kotlin.Boolean?, tagsOneOf: OneOfLessThanStringCommaArrayGreaterThan?, tagsAllOf: OneOfLessThanStringCommaArrayGreaterThan?, licenceOneOf: OneOfLessThanIntegerCommaArrayGreaterThan?, languageOneOf: OneOfLessThanStringCommaArrayGreaterThan?, nsfw: kotlin.String?, filter: kotlin.String?, skipCount: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (categoryOneOf != null) {
                    put("categoryOneOf", listOf(categoryOneOf.toString()))
                }
                if (isLive != null) {
                    put("isLive", listOf(isLive.toString()))
                }
                if (tagsOneOf != null) {
                    put("tagsOneOf", listOf(tagsOneOf.toString()))
                }
                if (tagsAllOf != null) {
                    put("tagsAllOf", listOf(tagsAllOf.toString()))
                }
                if (licenceOneOf != null) {
                    put("licenceOneOf", listOf(licenceOneOf.toString()))
                }
                if (languageOneOf != null) {
                    put("languageOneOf", listOf(languageOneOf.toString()))
                }
                if (nsfw != null) {
                    put("nsfw", listOf(nsfw.toString()))
                }
                if (filter != null) {
                    put("filter", listOf(filter.toString()))
                }
                if (skipCount != null) {
                    put("skipCount", listOf(skipCount.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/videos",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Import a video
    * Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
    * @param name Video name 
    * @param channelId Channel id that will contain this video 
    * @param targetUrl  (optional)
    * @param magnetUri  (optional)
    * @param torrentfile  (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @return VideoUploadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun importVideo(name: kotlin.String, channelId: kotlin.Int, targetUrl: TargetUrl?, magnetUri: MagnetUri?, torrentfile: Torrentfile?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, waitTranscoding: kotlin.Boolean?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.Set<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?, thumbnailfile: java.io.File?, previewfile: java.io.File?) : VideoUploadResponse {
        val localVariableConfig = importVideoRequestConfig(name = name, channelId = channelId, targetUrl = targetUrl, magnetUri = magnetUri, torrentfile = torrentfile, privacy = privacy, category = category, licence = licence, language = language, description = description, waitTranscoding = waitTranscoding, support = support, nsfw = nsfw, tags = tags, commentsEnabled = commentsEnabled, downloadEnabled = downloadEnabled, originallyPublishedAt = originallyPublishedAt, scheduleUpdate = scheduleUpdate, thumbnailfile = thumbnailfile, previewfile = previewfile)

        val localVarResponse = request<Map<String, Any?>, VideoUploadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoUploadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation importVideo
    *
    * @param name Video name 
    * @param channelId Channel id that will contain this video 
    * @param targetUrl  (optional)
    * @param magnetUri  (optional)
    * @param torrentfile  (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @return RequestConfig
    */
    fun importVideoRequestConfig(name: kotlin.String, channelId: kotlin.Int, targetUrl: TargetUrl?, magnetUri: MagnetUri?, torrentfile: Torrentfile?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, waitTranscoding: kotlin.Boolean?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.Set<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?, thumbnailfile: java.io.File?, previewfile: java.io.File?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("targetUrl" to targetUrl, "magnetUri" to magnetUri, "torrentfile" to torrentfile, "name" to name, "channelId" to channelId, "privacy" to privacy, "category" to category, "licence" to licence, "language" to language, "description" to description, "waitTranscoding" to waitTranscoding, "support" to support, "nsfw" to nsfw, "tags" to tags, "commentsEnabled" to commentsEnabled, "downloadEnabled" to downloadEnabled, "originallyPublishedAt" to originallyPublishedAt, "scheduleUpdate" to scheduleUpdate, "thumbnailfile" to thumbnailfile, "previewfile" to previewfile)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/imports",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update a video
    * 
    * @param id The object id, uuid or short uuid 
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param privacy  (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param name Video name (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun putVideo(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, thumbnailfile: java.io.File?, previewfile: java.io.File?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, privacy: VideoPrivacySet?, description: kotlin.String?, waitTranscoding: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, name: kotlin.String?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?) : Unit {
        val localVariableConfig = putVideoRequestConfig(id = id, thumbnailfile = thumbnailfile, previewfile = previewfile, category = category, licence = licence, language = language, privacy = privacy, description = description, waitTranscoding = waitTranscoding, support = support, nsfw = nsfw, name = name, tags = tags, commentsEnabled = commentsEnabled, downloadEnabled = downloadEnabled, originallyPublishedAt = originallyPublishedAt, scheduleUpdate = scheduleUpdate)

        val localVarResponse = request<Map<String, Any?>, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation putVideo
    *
    * @param id The object id, uuid or short uuid 
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param privacy  (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param name Video name (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @return RequestConfig
    */
    fun putVideoRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, thumbnailfile: java.io.File?, previewfile: java.io.File?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, privacy: VideoPrivacySet?, description: kotlin.String?, waitTranscoding: kotlin.String?, support: kotlin.String?, nsfw: kotlin.Boolean?, name: kotlin.String?, tags: kotlin.collections.List<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("thumbnailfile" to thumbnailfile, "previewfile" to previewfile, "category" to category, "licence" to licence, "language" to language, "privacy" to privacy, "description" to description, "waitTranscoding" to waitTranscoding, "support" to support, "nsfw" to nsfw, "name" to name, "tags" to tags, "commentsEnabled" to commentsEnabled, "downloadEnabled" to downloadEnabled, "originallyPublishedAt" to originallyPublishedAt, "scheduleUpdate" to scheduleUpdate)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/videos/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Set watching progress of a video
    * 
    * @param id The object id, uuid or short uuid 
    * @param userWatchingVideo  
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun setProgress(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, userWatchingVideo: UserWatchingVideo) : Unit {
        val localVariableConfig = setProgressRequestConfig(id = id, userWatchingVideo = userWatchingVideo)

        val localVarResponse = request<UserWatchingVideo, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation setProgress
    *
    * @param id The object id, uuid or short uuid 
    * @param userWatchingVideo  
    * @return RequestConfig
    */
    fun setProgressRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, userWatchingVideo: UserWatchingVideo) : RequestConfig<UserWatchingVideo> {
        val localVariableBody = userWatchingVideo
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/videos/{id}/watching".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update information about a live
    * 
    * @param id The object id, uuid or short uuid 
    * @param liveVideoUpdate  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun updateLiveId(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, liveVideoUpdate: LiveVideoUpdate?) : Unit {
        val localVariableConfig = updateLiveIdRequestConfig(id = id, liveVideoUpdate = liveVideoUpdate)

        val localVarResponse = request<LiveVideoUpdate, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation updateLiveId
    *
    * @param id The object id, uuid or short uuid 
    * @param liveVideoUpdate  (optional)
    * @return RequestConfig
    */
    fun updateLiveIdRequestConfig(id: OneOfLessThanIntegerCommaUUIDCommaStringGreaterThan, liveVideoUpdate: LiveVideoUpdate?) : RequestConfig<LiveVideoUpdate> {
        val localVariableBody = liveVideoUpdate
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/videos/live/{id}".replace("{"+"id"+"}", "$id"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Upload a video
    * Uses a single request to upload a video.
    * @param name Video name (optional)
    * @param channelId Channel id that will contain this video (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @param videofile Video file (optional)
    * @return VideoUploadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun uploadLegacy(name: kotlin.String?, channelId: kotlin.Int?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, waitTranscoding: kotlin.Boolean?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.Set<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?, thumbnailfile: java.io.File?, previewfile: java.io.File?, videofile: java.io.File?) : VideoUploadResponse {
        val localVariableConfig = uploadLegacyRequestConfig(name = name, channelId = channelId, privacy = privacy, category = category, licence = licence, language = language, description = description, waitTranscoding = waitTranscoding, support = support, nsfw = nsfw, tags = tags, commentsEnabled = commentsEnabled, downloadEnabled = downloadEnabled, originallyPublishedAt = originallyPublishedAt, scheduleUpdate = scheduleUpdate, thumbnailfile = thumbnailfile, previewfile = previewfile, videofile = videofile)

        val localVarResponse = request<Map<String, Any?>, VideoUploadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoUploadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation uploadLegacy
    *
    * @param name Video name (optional)
    * @param channelId Channel id that will contain this video (optional)
    * @param privacy  (optional)
    * @param category category id of the video (see [/videos/categories](#operation/getCategories)) (optional)
    * @param licence licence id of the video (see [/videos/licences](#operation/getLicences)) (optional)
    * @param language language id of the video (see [/videos/languages](#operation/getLanguages)) (optional)
    * @param description Video description (optional)
    * @param waitTranscoding Whether or not we wait transcoding before publish the video (optional)
    * @param support A text tell the audience how to support the video creator (optional)
    * @param nsfw Whether or not this video contains sensitive content (optional)
    * @param tags Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
    * @param commentsEnabled Enable or disable comments for this video (optional)
    * @param downloadEnabled Enable or disable downloading for this video (optional)
    * @param originallyPublishedAt Date when the content was originally published (optional)
    * @param scheduleUpdate  (optional)
    * @param thumbnailfile Video thumbnail file (optional)
    * @param previewfile Video preview file (optional)
    * @param videofile Video file (optional)
    * @return RequestConfig
    */
    fun uploadLegacyRequestConfig(name: kotlin.String?, channelId: kotlin.Int?, privacy: VideoPrivacySet?, category: kotlin.Int?, licence: kotlin.Int?, language: kotlin.String?, description: kotlin.String?, waitTranscoding: kotlin.Boolean?, support: kotlin.String?, nsfw: kotlin.Boolean?, tags: kotlin.collections.Set<kotlin.String>?, commentsEnabled: kotlin.Boolean?, downloadEnabled: kotlin.Boolean?, originallyPublishedAt: java.time.OffsetDateTime?, scheduleUpdate: VideoScheduledUpdate?, thumbnailfile: java.io.File?, previewfile: java.io.File?, videofile: java.io.File?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("name" to name, "channelId" to channelId, "privacy" to privacy, "category" to category, "licence" to licence, "language" to language, "description" to description, "waitTranscoding" to waitTranscoding, "support" to support, "nsfw" to nsfw, "tags" to tags, "commentsEnabled" to commentsEnabled, "downloadEnabled" to downloadEnabled, "originallyPublishedAt" to originallyPublishedAt, "scheduleUpdate" to scheduleUpdate, "thumbnailfile" to thumbnailfile, "previewfile" to previewfile, "videofile" to videofile)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/upload",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Send chunk for the resumable upload of a video
    * Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to continue, pause or resume the upload of a video
    * @param uploadId Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  
    * @param contentRange Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  
    * @param contentLength Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  
    * @param body  (optional)
    * @return VideoUploadResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun uploadResumable(uploadId: kotlin.String, contentRange: kotlin.String, contentLength: java.math.BigDecimal, body: java.io.File?) : VideoUploadResponse {
        val localVariableConfig = uploadResumableRequestConfig(uploadId = uploadId, contentRange = contentRange, contentLength = contentLength, body = body)

        val localVarResponse = request<java.io.File, VideoUploadResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoUploadResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation uploadResumable
    *
    * @param uploadId Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and you need to initialize a new upload.  
    * @param contentRange Specifies the bytes in the file that the request is uploading.  For example, a value of &#x60;bytes 0-262143/1000000&#x60; shows that the request is sending the first 262144 bytes (256 x 1024) in a 2,469,036 byte file.  
    * @param contentLength Size of the chunk that the request is sending.  The chunk size __must be a multiple of 256 KB__, and unlike [Google Resumable](https://developers.google.com/youtube/v3/guides/using_resumable_upload_protocol) doesn&#39;t mandate for chunks to have the same size throughout the upload sequence.  Remember that larger chunks are more efficient. PeerTube&#39;s web client uses chunks varying from 1048576 bytes (~1MB) and increases or reduces size depending on connection health.  
    * @param body  (optional)
    * @return RequestConfig
    */
    fun uploadResumableRequestConfig(uploadId: kotlin.String, contentRange: kotlin.String, contentLength: java.math.BigDecimal, body: java.io.File?) : RequestConfig<java.io.File> {
        val localVariableBody = body
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("upload_id", listOf(uploadId.toString()))
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()
        contentRange.apply { localVariableHeaders["Content-Range"] = this.toString() }
        contentLength.apply { localVariableHeaders["Content-Length"] = this.toString() }

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/videos/upload-resumable",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Cancel the resumable upload of a video, deleting any data uploaded so far
    * Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to cancel the upload of a video
    * @param uploadId Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  
    * @param contentLength  
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun uploadResumableCancel(uploadId: kotlin.String, contentLength: java.math.BigDecimal) : Unit {
        val localVariableConfig = uploadResumableCancelRequestConfig(uploadId = uploadId, contentLength = contentLength)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation uploadResumableCancel
    *
    * @param uploadId Created session id to proceed with. If you didn&#39;t send chunks in the last 12 hours, it is not valid anymore and the upload session has already been deleted with its data ;-)  
    * @param contentLength  
    * @return RequestConfig
    */
    fun uploadResumableCancelRequestConfig(uploadId: kotlin.String, contentLength: java.math.BigDecimal) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("upload_id", listOf(uploadId.toString()))
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()
        contentLength.apply { localVariableHeaders["Content-Length"] = this.toString() }

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/videos/upload-resumable",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Initialize the resumable upload of a video
    * Uses [a resumable protocol](https://github.com/kukhariev/node-uploadx/blob/master/proto.md) to initialize the upload of a video
    * @param xUploadContentLength Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. 
    * @param xUploadContentType MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. 
    * @param videoUploadRequestResumable  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun uploadResumableInit(xUploadContentLength: java.math.BigDecimal, xUploadContentType: kotlin.String, videoUploadRequestResumable: VideoUploadRequestResumable?) : Unit {
        val localVariableConfig = uploadResumableInitRequestConfig(xUploadContentLength = xUploadContentLength, xUploadContentType = xUploadContentType, videoUploadRequestResumable = videoUploadRequestResumable)

        val localVarResponse = request<VideoUploadRequestResumable, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation uploadResumableInit
    *
    * @param xUploadContentLength Number of bytes that will be uploaded in subsequent requests. Set this value to the size of the file you are uploading. 
    * @param xUploadContentType MIME type of the file that you are uploading. Depending on your instance settings, acceptable values might vary. 
    * @param videoUploadRequestResumable  (optional)
    * @return RequestConfig
    */
    fun uploadResumableInitRequestConfig(xUploadContentLength: java.math.BigDecimal, xUploadContentType: kotlin.String, videoUploadRequestResumable: VideoUploadRequestResumable?) : RequestConfig<VideoUploadRequestResumable> {
        val localVariableBody = videoUploadRequestResumable
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()
        xUploadContentLength.apply { localVariableHeaders["X-Upload-Content-Length"] = this.toString() }
        xUploadContentType.apply { localVariableHeaders["X-Upload-Content-Type"] = this.toString() }

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/videos/upload-resumable",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
