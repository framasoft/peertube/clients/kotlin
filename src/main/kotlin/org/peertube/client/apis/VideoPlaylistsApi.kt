/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.InlineObject19
import org.peertube.client.models.InlineObject20
import org.peertube.client.models.InlineObject21
import org.peertube.client.models.InlineResponse20011
import org.peertube.client.models.InlineResponse20012
import org.peertube.client.models.InlineResponse20013
import org.peertube.client.models.InlineResponse20014
import org.peertube.client.models.VideoListResponse
import org.peertube.client.models.VideoPlaylist
import org.peertube.client.models.VideoPlaylistPrivacySet

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class VideoPlaylistsApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * Create a video playlist
    * If the video playlist is set as public, &#x60;videoChannelId&#x60; is mandatory.
    * @param displayName Video playlist display name 
    * @param thumbnailfile Video playlist thumbnail file (optional)
    * @param privacy  (optional)
    * @param description Video playlist description (optional)
    * @param videoChannelId Video channel in which the playlist will be published (optional)
    * @return InlineResponse20012
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun addPlaylist(displayName: kotlin.String, thumbnailfile: java.io.File?, privacy: VideoPlaylistPrivacySet?, description: kotlin.String?, videoChannelId: kotlin.Int?) : InlineResponse20012 {
        val localVariableConfig = addPlaylistRequestConfig(displayName = displayName, thumbnailfile = thumbnailfile, privacy = privacy, description = description, videoChannelId = videoChannelId)

        val localVarResponse = request<Map<String, Any?>, InlineResponse20012>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse20012
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation addPlaylist
    *
    * @param displayName Video playlist display name 
    * @param thumbnailfile Video playlist thumbnail file (optional)
    * @param privacy  (optional)
    * @param description Video playlist description (optional)
    * @param videoChannelId Video channel in which the playlist will be published (optional)
    * @return RequestConfig
    */
    fun addPlaylistRequestConfig(displayName: kotlin.String, thumbnailfile: java.io.File?, privacy: VideoPlaylistPrivacySet?, description: kotlin.String?, videoChannelId: kotlin.Int?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("displayName" to displayName, "thumbnailfile" to thumbnailfile, "privacy" to privacy, "description" to description, "videoChannelId" to videoChannelId)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/video-playlists",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Add a video in a playlist
    * 
    * @param playlistId Playlist id 
    * @param inlineObject19  (optional)
    * @return InlineResponse20013
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun addVideoPlaylistVideo(playlistId: kotlin.Int, inlineObject19: InlineObject19?) : InlineResponse20013 {
        val localVariableConfig = addVideoPlaylistVideoRequestConfig(playlistId = playlistId, inlineObject19 = inlineObject19)

        val localVarResponse = request<InlineObject19, InlineResponse20013>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse20013
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation addVideoPlaylistVideo
    *
    * @param playlistId Playlist id 
    * @param inlineObject19  (optional)
    * @return RequestConfig
    */
    fun addVideoPlaylistVideoRequestConfig(playlistId: kotlin.Int, inlineObject19: InlineObject19?) : RequestConfig<InlineObject19> {
        val localVariableBody = inlineObject19
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/video-playlists/{playlistId}/videos".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Delete an element from a playlist
    * 
    * @param playlistId Playlist id 
    * @param playlistElementId Playlist element id 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun delVideoPlaylistVideo(playlistId: kotlin.Int, playlistElementId: kotlin.Int) : Unit {
        val localVariableConfig = delVideoPlaylistVideoRequestConfig(playlistId = playlistId, playlistElementId = playlistElementId)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation delVideoPlaylistVideo
    *
    * @param playlistId Playlist id 
    * @param playlistElementId Playlist element id 
    * @return RequestConfig
    */
    fun delVideoPlaylistVideoRequestConfig(playlistId: kotlin.Int, playlistElementId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/video-playlists/{playlistId}/videos/{playlistElementId}".replace("{"+"playlistId"+"}", "$playlistId").replace("{"+"playlistElementId"+"}", "$playlistElementId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List available playlist privacy policies
    * 
    * @return kotlin.collections.List<kotlin.String>
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getPlaylistPrivacyPolicies() : kotlin.collections.List<kotlin.String> {
        val localVariableConfig = getPlaylistPrivacyPoliciesRequestConfig()

        val localVarResponse = request<Unit, kotlin.collections.List<kotlin.String>>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as kotlin.collections.List<kotlin.String>
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getPlaylistPrivacyPolicies
    *
    * @return RequestConfig
    */
    fun getPlaylistPrivacyPoliciesRequestConfig() : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/video-playlists/privacies",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List video playlists
    * 
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort column (optional)
    * @return InlineResponse20011
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getPlaylists(start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : InlineResponse20011 {
        val localVariableConfig = getPlaylistsRequestConfig(start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, InlineResponse20011>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse20011
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getPlaylists
    *
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort column (optional)
    * @return RequestConfig
    */
    fun getPlaylistsRequestConfig(start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/video-playlists",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List videos of a playlist
    * 
    * @param playlistId Playlist id 
    * @return VideoListResponse
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getVideoPlaylistVideos(playlistId: kotlin.Int) : VideoListResponse {
        val localVariableConfig = getVideoPlaylistVideosRequestConfig(playlistId = playlistId)

        val localVarResponse = request<Unit, VideoListResponse>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoListResponse
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getVideoPlaylistVideos
    *
    * @param playlistId Playlist id 
    * @return RequestConfig
    */
    fun getVideoPlaylistVideosRequestConfig(playlistId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/video-playlists/{playlistId}/videos".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update a playlist element
    * 
    * @param playlistId Playlist id 
    * @param playlistElementId Playlist element id 
    * @param inlineObject21  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun putVideoPlaylistVideo(playlistId: kotlin.Int, playlistElementId: kotlin.Int, inlineObject21: InlineObject21?) : Unit {
        val localVariableConfig = putVideoPlaylistVideoRequestConfig(playlistId = playlistId, playlistElementId = playlistElementId, inlineObject21 = inlineObject21)

        val localVarResponse = request<InlineObject21, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation putVideoPlaylistVideo
    *
    * @param playlistId Playlist id 
    * @param playlistElementId Playlist element id 
    * @param inlineObject21  (optional)
    * @return RequestConfig
    */
    fun putVideoPlaylistVideoRequestConfig(playlistId: kotlin.Int, playlistElementId: kotlin.Int, inlineObject21: InlineObject21?) : RequestConfig<InlineObject21> {
        val localVariableBody = inlineObject21
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/video-playlists/{playlistId}/videos/{playlistElementId}".replace("{"+"playlistId"+"}", "$playlistId").replace("{"+"playlistElementId"+"}", "$playlistElementId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Reorder a playlist
    * 
    * @param playlistId Playlist id 
    * @param inlineObject20  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun reorderVideoPlaylist(playlistId: kotlin.Int, inlineObject20: InlineObject20?) : Unit {
        val localVariableConfig = reorderVideoPlaylistRequestConfig(playlistId = playlistId, inlineObject20 = inlineObject20)

        val localVarResponse = request<InlineObject20, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation reorderVideoPlaylist
    *
    * @param playlistId Playlist id 
    * @param inlineObject20  (optional)
    * @return RequestConfig
    */
    fun reorderVideoPlaylistRequestConfig(playlistId: kotlin.Int, inlineObject20: InlineObject20?) : RequestConfig<InlineObject20> {
        val localVariableBody = inlineObject20
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/video-playlists/{playlistId}/videos/reorder".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Check video exists in my playlists
    * 
    * @param videoIds The video ids to check 
    * @return InlineResponse20014
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun usersMeVideoPlaylistsVideosExistGet(videoIds: kotlin.collections.List<kotlin.Int>) : InlineResponse20014 {
        val localVariableConfig = usersMeVideoPlaylistsVideosExistGetRequestConfig(videoIds = videoIds)

        val localVarResponse = request<Unit, InlineResponse20014>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse20014
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation usersMeVideoPlaylistsVideosExistGet
    *
    * @param videoIds The video ids to check 
    * @return RequestConfig
    */
    fun usersMeVideoPlaylistsVideosExistGetRequestConfig(videoIds: kotlin.collections.List<kotlin.Int>) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                put("videoIds", toMultiValue(videoIds.toList(), "multi"))
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/users/me/video-playlists/videos-exist",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Delete a video playlist
    * 
    * @param playlistId Playlist id 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videoPlaylistsPlaylistIdDelete(playlistId: kotlin.Int) : Unit {
        val localVariableConfig = videoPlaylistsPlaylistIdDeleteRequestConfig(playlistId = playlistId)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videoPlaylistsPlaylistIdDelete
    *
    * @param playlistId Playlist id 
    * @return RequestConfig
    */
    fun videoPlaylistsPlaylistIdDeleteRequestConfig(playlistId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/video-playlists/{playlistId}".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Get a video playlist
    * 
    * @param playlistId Playlist id 
    * @return VideoPlaylist
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videoPlaylistsPlaylistIdGet(playlistId: kotlin.Int) : VideoPlaylist {
        val localVariableConfig = videoPlaylistsPlaylistIdGetRequestConfig(playlistId = playlistId)

        val localVarResponse = request<Unit, VideoPlaylist>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as VideoPlaylist
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videoPlaylistsPlaylistIdGet
    *
    * @param playlistId Playlist id 
    * @return RequestConfig
    */
    fun videoPlaylistsPlaylistIdGetRequestConfig(playlistId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/video-playlists/{playlistId}".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update a video playlist
    * If the video playlist is set as public, the playlist must have a assigned channel.
    * @param playlistId Playlist id 
    * @param displayName Video playlist display name (optional)
    * @param thumbnailfile Video playlist thumbnail file (optional)
    * @param privacy  (optional)
    * @param description Video playlist description (optional)
    * @param videoChannelId Video channel in which the playlist will be published (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun videoPlaylistsPlaylistIdPut(playlistId: kotlin.Int, displayName: kotlin.String?, thumbnailfile: java.io.File?, privacy: VideoPlaylistPrivacySet?, description: kotlin.String?, videoChannelId: kotlin.Int?) : Unit {
        val localVariableConfig = videoPlaylistsPlaylistIdPutRequestConfig(playlistId = playlistId, displayName = displayName, thumbnailfile = thumbnailfile, privacy = privacy, description = description, videoChannelId = videoChannelId)

        val localVarResponse = request<Map<String, Any?>, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation videoPlaylistsPlaylistIdPut
    *
    * @param playlistId Playlist id 
    * @param displayName Video playlist display name (optional)
    * @param thumbnailfile Video playlist thumbnail file (optional)
    * @param privacy  (optional)
    * @param description Video playlist description (optional)
    * @param videoChannelId Video channel in which the playlist will be published (optional)
    * @return RequestConfig
    */
    fun videoPlaylistsPlaylistIdPutRequestConfig(playlistId: kotlin.Int, displayName: kotlin.String?, thumbnailfile: java.io.File?, privacy: VideoPlaylistPrivacySet?, description: kotlin.String?, videoChannelId: kotlin.Int?) : RequestConfig<Map<String, Any?>> {
        val localVariableBody = mapOf("displayName" to displayName, "thumbnailfile" to thumbnailfile, "privacy" to privacy, "description" to description, "videoChannelId" to videoChannelId)
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/video-playlists/{playlistId}".replace("{"+"playlistId"+"}", "$playlistId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
