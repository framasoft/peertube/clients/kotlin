/**
 * PeerTube
 *
 * The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [REST API quick start](https://docs.joinpeertube.org/api-rest-getting-started) for a few examples of using the PeerTube API.  # Authentication  When you sign up for an account on a PeerTube instance, you are given the possibility to generate sessions on it, and authenticate there using an access token. Only __one access token can currently be used at a time__.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call, completed by a [RFC7807-compliant](https://tools.ietf.org/html/rfc7807) response body.  ``` HTTP 1.1 404 Not Found Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Video not found\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 404,   \"title\": \"Not Found\",   \"type\": \"about:blank\" } ```  We provide error `type` values for [a growing number of cases](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/server/server-error-code.enum.ts), but it is still optional. Types are used to disambiguate errors that bear the same status code and are non-obvious:  ``` HTTP 1.1 403 Forbidden Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Cannot get this video regarding follow constraints\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"status\": 403,   \"title\": \"Forbidden\",   \"type\": \"https://docs.joinpeertube.org/api-rest-reference.html#section/Errors/does_not_respect_follow_constraints\" } ```  Here a 403 error could otherwise mean that the video is private or blocklisted.  ### Validation errors  Each parameter is evaluated on its own against a set of rules before the route validator proceeds with potential testing involving parameter combinations. Errors coming from validation errors appear earlier and benefit from a more detailed error description:  ``` HTTP 1.1 400 Bad Request Content-Type: application/problem+json; charset=utf-8  {   \"detail\": \"Incorrect request parameters: id\",   \"docs\": \"https://docs.joinpeertube.org/api-rest-reference.html#operation/getVideo\",   \"instance\": \"/api/v1/videos/9c9de5e8-0a1e-484a-b099-e80766180\",   \"invalid-params\": {     \"id\": {       \"location\": \"params\",       \"msg\": \"Invalid value\",       \"param\": \"id\",       \"value\": \"9c9de5e8-0a1e-484a-b099-e80766180\"     }   },   \"status\": 400,   \"title\": \"Bad Request\",   \"type\": \"about:blank\" } ```  Where `id` is the name of the field concerned by the error, within the route definition. `invalid-params.<field>.location` can be either 'params', 'body', 'header', 'query' or 'cookies', and `invalid-params.<field>.value` reports the value that didn't pass validation whose `invalid-params.<field>.msg` is about.  ### Deprecated error fields  Some fields could be included with previous versions. They are still included but their use is deprecated: - `error`: superseded by `detail` - `code`: superseded by `type` (which is now an URI)  # Rate limits  We are rate-limiting all endpoints of PeerTube's API. Custom values can be set by administrators:  | Endpoint (prefix: `/api/v1`) | Calls         | Time frame   | |------------------------------|---------------|--------------| | `/_*`                         | 50            | 10 seconds   | | `POST /users/token`          | 15            | 5 minutes    | | `POST /users/register`       | 2<sup>*</sup> | 5 minutes    | | `POST /users/ask-send-verify-email` | 3      | 5 minutes    |  Depending on the endpoint, <sup>*</sup>failed requests are not taken into account. A service limit is announced by a `429 Too Many Requests` status code.  You can get details about the current state of your rate limit by reading the following headers:  | Header                  | Description                                                | |-------------------------|------------------------------------------------------------| | `X-RateLimit-Limit`     | Number of max requests allowed in the current time period  | | `X-RateLimit-Remaining` | Number of remaining requests in the current time period    | | `X-RateLimit-Reset`     | Timestamp of end of current time period as UNIX timestamp  | | `Retry-After`           | Seconds to delay after the first `429` is received         |  # CORS  This API features [Cross-Origin Resource Sharing (CORS)](https://fetch.spec.whatwg.org/), allowing cross-domain communication from the browser for some routes:  | Endpoint                    | |------------------------- ---| | `/api/_*`                    | | `/download/_*`               | | `/lazy-static/_*`            | | `/live/segments-sha256/_*`   | | `/.well-known/webfinger`    |  In addition, all routes serving ActivityPub are CORS-enabled for all origins. 
 *
 * The version of the OpenAPI document: 3.4.0
 * 
 *
 * Please note:
 * This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * Do not edit this file manually.
 */

@file:Suppress(
    "ArrayInDataClass",
    "EnumEntryName",
    "RemoveRedundantQualifierName",
    "UnusedImport"
)

package org.peertube.client.apis

import org.peertube.client.models.AbuseStateSet
import org.peertube.client.models.InlineObject11
import org.peertube.client.models.InlineObject12
import org.peertube.client.models.InlineObject13
import org.peertube.client.models.InlineResponse2005
import org.peertube.client.models.InlineResponse2006
import org.peertube.client.models.InlineResponse2007

import org.peertube.client.infrastructure.ApiClient
import org.peertube.client.infrastructure.ClientException
import org.peertube.client.infrastructure.ClientError
import org.peertube.client.infrastructure.ServerException
import org.peertube.client.infrastructure.ServerError
import org.peertube.client.infrastructure.MultiValueMap
import org.peertube.client.infrastructure.RequestConfig
import org.peertube.client.infrastructure.RequestMethod
import org.peertube.client.infrastructure.ResponseType
import org.peertube.client.infrastructure.Success
import org.peertube.client.infrastructure.toMultiValue

class AbusesApi(basePath: kotlin.String = defaultBasePath) : ApiClient(basePath) {
    companion object {
        @JvmStatic
        val defaultBasePath: String by lazy {
            System.getProperties().getProperty("org.peertube.client.baseUrl", "https://peertube2.cpy.re/api/v1")
        }
    }

    /**
    * Delete an abuse
    * 
    * @param abuseId Abuse id 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesAbuseIdDelete(abuseId: kotlin.Int) : Unit {
        val localVariableConfig = abusesAbuseIdDeleteRequestConfig(abuseId = abuseId)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesAbuseIdDelete
    *
    * @param abuseId Abuse id 
    * @return RequestConfig
    */
    fun abusesAbuseIdDeleteRequestConfig(abuseId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/abuses/{abuseId}".replace("{"+"abuseId"+"}", "$abuseId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Delete an abuse message
    * 
    * @param abuseId Abuse id 
    * @param abuseMessageId Abuse message id 
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesAbuseIdMessagesAbuseMessageIdDelete(abuseId: kotlin.Int, abuseMessageId: kotlin.Int) : Unit {
        val localVariableConfig = abusesAbuseIdMessagesAbuseMessageIdDeleteRequestConfig(abuseId = abuseId, abuseMessageId = abuseMessageId)

        val localVarResponse = request<Unit, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesAbuseIdMessagesAbuseMessageIdDelete
    *
    * @param abuseId Abuse id 
    * @param abuseMessageId Abuse message id 
    * @return RequestConfig
    */
    fun abusesAbuseIdMessagesAbuseMessageIdDeleteRequestConfig(abuseId: kotlin.Int, abuseMessageId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.DELETE,
            path = "/abuses/{abuseId}/messages/{abuseMessageId}".replace("{"+"abuseId"+"}", "$abuseId").replace("{"+"abuseMessageId"+"}", "$abuseMessageId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List messages of an abuse
    * 
    * @param abuseId Abuse id 
    * @return InlineResponse2007
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesAbuseIdMessagesGet(abuseId: kotlin.Int) : InlineResponse2007 {
        val localVariableConfig = abusesAbuseIdMessagesGetRequestConfig(abuseId = abuseId)

        val localVarResponse = request<Unit, InlineResponse2007>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse2007
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesAbuseIdMessagesGet
    *
    * @param abuseId Abuse id 
    * @return RequestConfig
    */
    fun abusesAbuseIdMessagesGetRequestConfig(abuseId: kotlin.Int) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/abuses/{abuseId}/messages".replace("{"+"abuseId"+"}", "$abuseId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Add message to an abuse
    * 
    * @param abuseId Abuse id 
    * @param inlineObject13  
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesAbuseIdMessagesPost(abuseId: kotlin.Int, inlineObject13: InlineObject13) : Unit {
        val localVariableConfig = abusesAbuseIdMessagesPostRequestConfig(abuseId = abuseId, inlineObject13 = inlineObject13)

        val localVarResponse = request<InlineObject13, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesAbuseIdMessagesPost
    *
    * @param abuseId Abuse id 
    * @param inlineObject13  
    * @return RequestConfig
    */
    fun abusesAbuseIdMessagesPostRequestConfig(abuseId: kotlin.Int, inlineObject13: InlineObject13) : RequestConfig<InlineObject13> {
        val localVariableBody = inlineObject13
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/abuses/{abuseId}/messages".replace("{"+"abuseId"+"}", "$abuseId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Update an abuse
    * 
    * @param abuseId Abuse id 
    * @param inlineObject12  (optional)
    * @return void
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesAbuseIdPut(abuseId: kotlin.Int, inlineObject12: InlineObject12?) : Unit {
        val localVariableConfig = abusesAbuseIdPutRequestConfig(abuseId = abuseId, inlineObject12 = inlineObject12)

        val localVarResponse = request<InlineObject12, Unit>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesAbuseIdPut
    *
    * @param abuseId Abuse id 
    * @param inlineObject12  (optional)
    * @return RequestConfig
    */
    fun abusesAbuseIdPutRequestConfig(abuseId: kotlin.Int, inlineObject12: InlineObject12?) : RequestConfig<InlineObject12> {
        val localVariableBody = inlineObject12
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.PUT,
            path = "/abuses/{abuseId}".replace("{"+"abuseId"+"}", "$abuseId"),
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * Report an abuse
    * 
    * @param inlineObject11  
    * @return InlineResponse2006
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun abusesPost(inlineObject11: InlineObject11) : InlineResponse2006 {
        val localVariableConfig = abusesPostRequestConfig(inlineObject11 = inlineObject11)

        val localVarResponse = request<InlineObject11, InlineResponse2006>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse2006
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation abusesPost
    *
    * @param inlineObject11  
    * @return RequestConfig
    */
    fun abusesPostRequestConfig(inlineObject11: InlineObject11) : RequestConfig<InlineObject11> {
        val localVariableBody = inlineObject11
        val localVariableQuery: MultiValueMap = mutableMapOf()
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.POST,
            path = "/abuses",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List abuses
    * 
    * @param id only list the report with this id (optional)
    * @param predefinedReason predefined reason the listed reports should contain (optional)
    * @param search plain search that will match with video titles, reporter names and more (optional)
    * @param state  (optional)
    * @param searchReporter only list reports of a specific reporter (optional)
    * @param searchReportee only list reports of a specific reportee (optional)
    * @param searchVideo only list reports of a specific video (optional)
    * @param searchVideoChannel only list reports of a specific video channel (optional)
    * @param videoIs only list deleted or blocklisted videos (optional)
    * @param filter only list account, comment or video reports (optional)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort abuses by criteria (optional)
    * @return InlineResponse2005
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getAbuses(id: kotlin.Int?, predefinedReason: kotlin.collections.List<kotlin.String>?, search: kotlin.String?, state: AbuseStateSet?, searchReporter: kotlin.String?, searchReportee: kotlin.String?, searchVideo: kotlin.String?, searchVideoChannel: kotlin.String?, videoIs: kotlin.String?, filter: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : InlineResponse2005 {
        val localVariableConfig = getAbusesRequestConfig(id = id, predefinedReason = predefinedReason, search = search, state = state, searchReporter = searchReporter, searchReportee = searchReportee, searchVideo = searchVideo, searchVideoChannel = searchVideoChannel, videoIs = videoIs, filter = filter, start = start, count = count, sort = sort)

        val localVarResponse = request<Unit, InlineResponse2005>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse2005
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getAbuses
    *
    * @param id only list the report with this id (optional)
    * @param predefinedReason predefined reason the listed reports should contain (optional)
    * @param search plain search that will match with video titles, reporter names and more (optional)
    * @param state  (optional)
    * @param searchReporter only list reports of a specific reporter (optional)
    * @param searchReportee only list reports of a specific reportee (optional)
    * @param searchVideo only list reports of a specific video (optional)
    * @param searchVideoChannel only list reports of a specific video channel (optional)
    * @param videoIs only list deleted or blocklisted videos (optional)
    * @param filter only list account, comment or video reports (optional)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @param sort Sort abuses by criteria (optional)
    * @return RequestConfig
    */
    fun getAbusesRequestConfig(id: kotlin.Int?, predefinedReason: kotlin.collections.List<kotlin.String>?, search: kotlin.String?, state: AbuseStateSet?, searchReporter: kotlin.String?, searchReportee: kotlin.String?, searchVideo: kotlin.String?, searchVideoChannel: kotlin.String?, videoIs: kotlin.String?, filter: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?, sort: kotlin.String?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (id != null) {
                    put("id", listOf(id.toString()))
                }
                if (predefinedReason != null) {
                    put("predefinedReason", toMultiValue(predefinedReason.toList(), "multi"))
                }
                if (search != null) {
                    put("search", listOf(search.toString()))
                }
                if (state != null) {
                    put("state", listOf(state.toString()))
                }
                if (searchReporter != null) {
                    put("searchReporter", listOf(searchReporter.toString()))
                }
                if (searchReportee != null) {
                    put("searchReportee", listOf(searchReportee.toString()))
                }
                if (searchVideo != null) {
                    put("searchVideo", listOf(searchVideo.toString()))
                }
                if (searchVideoChannel != null) {
                    put("searchVideoChannel", listOf(searchVideoChannel.toString()))
                }
                if (videoIs != null) {
                    put("videoIs", listOf(videoIs.toString()))
                }
                if (filter != null) {
                    put("filter", listOf(filter.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/abuses",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

    /**
    * List my abuses
    * 
    * @param id only list the report with this id (optional)
    * @param state  (optional)
    * @param sort Sort abuses by criteria (optional)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @return InlineResponse2005
    * @throws UnsupportedOperationException If the API returns an informational or redirection response
    * @throws ClientException If the API returns a client error response
    * @throws ServerException If the API returns a server error response
    */
    @Suppress("UNCHECKED_CAST")
    @Throws(UnsupportedOperationException::class, ClientException::class, ServerException::class)
    fun getMyAbuses(id: kotlin.Int?, state: AbuseStateSet?, sort: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?) : InlineResponse2005 {
        val localVariableConfig = getMyAbusesRequestConfig(id = id, state = state, sort = sort, start = start, count = count)

        val localVarResponse = request<Unit, InlineResponse2005>(
            localVariableConfig
        )

        return when (localVarResponse.responseType) {
            ResponseType.Success -> (localVarResponse as Success<*>).data as InlineResponse2005
            ResponseType.Informational -> throw UnsupportedOperationException("Client does not support Informational responses.")
            ResponseType.Redirection -> throw UnsupportedOperationException("Client does not support Redirection responses.")
            ResponseType.ClientError -> {
                val localVarError = localVarResponse as ClientError<*>
                throw ClientException("Client error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
            ResponseType.ServerError -> {
                val localVarError = localVarResponse as ServerError<*>
                throw ServerException("Server error : ${localVarError.statusCode} ${localVarError.message.orEmpty()}", localVarError.statusCode, localVarResponse)
            }
        }
    }

    /**
    * To obtain the request config of the operation getMyAbuses
    *
    * @param id only list the report with this id (optional)
    * @param state  (optional)
    * @param sort Sort abuses by criteria (optional)
    * @param start Offset used to paginate results (optional)
    * @param count Number of items to return (optional, default to 15)
    * @return RequestConfig
    */
    fun getMyAbusesRequestConfig(id: kotlin.Int?, state: AbuseStateSet?, sort: kotlin.String?, start: kotlin.Int?, count: kotlin.Int?) : RequestConfig<Unit> {
        val localVariableBody = null
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, List<kotlin.String>>()
            .apply {
                if (id != null) {
                    put("id", listOf(id.toString()))
                }
                if (state != null) {
                    put("state", listOf(state.toString()))
                }
                if (sort != null) {
                    put("sort", listOf(sort.toString()))
                }
                if (start != null) {
                    put("start", listOf(start.toString()))
                }
                if (count != null) {
                    put("count", listOf(count.toString()))
                }
            }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf()

        return RequestConfig(
            method = RequestMethod.GET,
            path = "/users/me/abuses",
            query = localVariableQuery,
            headers = localVariableHeaders,
            body = localVariableBody
        )
    }

}
