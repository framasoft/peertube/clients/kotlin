# Kotlin API client for PeerTube

## Requires

* Kotlin 1.3.41
* Gradle 4.9

## Build

First, create the gradle wrapper script:

```
gradle wrapper
```

Then, run:

```
./gradlew check assemble
```

This runs all tests and packages the library.

## Features/Implementation Notes

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *https://peertube2.cpy.re/api/v1*. Change it when instanciating `ApiClient(basePath)`.

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AbusesApi* | [**abusesAbuseIdDelete**](docs/AbusesApi.md#abusesabuseiddelete) | **DELETE** /abuses/{abuseId} | Delete an abuse
*AbusesApi* | [**abusesAbuseIdMessagesAbuseMessageIdDelete**](docs/AbusesApi.md#abusesabuseidmessagesabusemessageiddelete) | **DELETE** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
*AbusesApi* | [**abusesAbuseIdMessagesGet**](docs/AbusesApi.md#abusesabuseidmessagesget) | **GET** /abuses/{abuseId}/messages | List messages of an abuse
*AbusesApi* | [**abusesAbuseIdMessagesPost**](docs/AbusesApi.md#abusesabuseidmessagespost) | **POST** /abuses/{abuseId}/messages | Add message to an abuse
*AbusesApi* | [**abusesAbuseIdPut**](docs/AbusesApi.md#abusesabuseidput) | **PUT** /abuses/{abuseId} | Update an abuse
*AbusesApi* | [**abusesPost**](docs/AbusesApi.md#abusespost) | **POST** /abuses | Report an abuse
*AbusesApi* | [**getAbuses**](docs/AbusesApi.md#getabuses) | **GET** /abuses | List abuses
*AbusesApi* | [**getMyAbuses**](docs/AbusesApi.md#getmyabuses) | **GET** /users/me/abuses | List my abuses
*AccountApi* | [**getSyndicatedSubscriptionVideos**](docs/AccountApi.md#getsyndicatedsubscriptionvideos) | **GET** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token
*AccountBlocksApi* | [**serverBlocklistAccountsAccountNameDelete**](docs/AccountBlocksApi.md#serverblocklistaccountsaccountnamedelete) | **DELETE** /server/blocklist/accounts/{accountName} | Unblock an account by its handle
*AccountBlocksApi* | [**serverBlocklistAccountsGet**](docs/AccountBlocksApi.md#serverblocklistaccountsget) | **GET** /server/blocklist/accounts | List account blocks
*AccountBlocksApi* | [**serverBlocklistAccountsPost**](docs/AccountBlocksApi.md#serverblocklistaccountspost) | **POST** /server/blocklist/accounts | Block an account
*AccountsApi* | [**accountsNameRatingsGet**](docs/AccountsApi.md#accountsnameratingsget) | **GET** /accounts/{name}/ratings | List ratings of an account
*AccountsApi* | [**accountsNameVideoChannelsGet**](docs/AccountsApi.md#accountsnamevideochannelsget) | **GET** /accounts/{name}/video-channels | List video channels of an account
*AccountsApi* | [**getAccount**](docs/AccountsApi.md#getaccount) | **GET** /accounts/{name} | Get an account
*AccountsApi* | [**getAccountVideos**](docs/AccountsApi.md#getaccountvideos) | **GET** /accounts/{name}/videos | List videos of an account
*AccountsApi* | [**getAccounts**](docs/AccountsApi.md#getaccounts) | **GET** /accounts | List accounts
*ConfigApi* | [**delCustomConfig**](docs/ConfigApi.md#delcustomconfig) | **DELETE** /config/custom | Delete instance runtime configuration
*ConfigApi* | [**getAbout**](docs/ConfigApi.md#getabout) | **GET** /config/about | Get instance \"About\" information
*ConfigApi* | [**getConfig**](docs/ConfigApi.md#getconfig) | **GET** /config | Get instance public configuration
*ConfigApi* | [**getCustomConfig**](docs/ConfigApi.md#getcustomconfig) | **GET** /config/custom | Get instance runtime configuration
*ConfigApi* | [**putCustomConfig**](docs/ConfigApi.md#putcustomconfig) | **PUT** /config/custom | Set instance runtime configuration
*FeedsApi* | [**getSyndicatedComments**](docs/FeedsApi.md#getsyndicatedcomments) | **GET** /feeds/video-comments.{format} | List comments on videos
*FeedsApi* | [**getSyndicatedSubscriptionVideos**](docs/FeedsApi.md#getsyndicatedsubscriptionvideos) | **GET** /feeds/subscriptions.{format} | List videos of subscriptions tied to a token
*FeedsApi* | [**getSyndicatedVideos**](docs/FeedsApi.md#getsyndicatedvideos) | **GET** /feeds/videos.{format} | List videos
*HomepageApi* | [**customPagesHomepageInstanceGet**](docs/HomepageApi.md#custompageshomepageinstanceget) | **GET** /custom-pages/homepage/instance | Get instance custom homepage
*HomepageApi* | [**customPagesHomepageInstancePut**](docs/HomepageApi.md#custompageshomepageinstanceput) | **PUT** /custom-pages/homepage/instance | Set instance custom homepage
*InstanceFollowsApi* | [**serverFollowersGet**](docs/InstanceFollowsApi.md#serverfollowersget) | **GET** /server/followers | List instances following the server
*InstanceFollowsApi* | [**serverFollowersNameWithHostAcceptPost**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostacceptpost) | **POST** /server/followers/{nameWithHost}/accept | Accept a pending follower to your server
*InstanceFollowsApi* | [**serverFollowersNameWithHostDelete**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostdelete) | **DELETE** /server/followers/{nameWithHost} | Remove or reject a follower to your server
*InstanceFollowsApi* | [**serverFollowersNameWithHostRejectPost**](docs/InstanceFollowsApi.md#serverfollowersnamewithhostrejectpost) | **POST** /server/followers/{nameWithHost}/reject | Reject a pending follower to your server
*InstanceFollowsApi* | [**serverFollowingGet**](docs/InstanceFollowsApi.md#serverfollowingget) | **GET** /server/following | List instances followed by the server
*InstanceFollowsApi* | [**serverFollowingHostOrHandleDelete**](docs/InstanceFollowsApi.md#serverfollowinghostorhandledelete) | **DELETE** /server/following/{hostOrHandle} | Unfollow an actor (PeerTube instance, channel or account)
*InstanceFollowsApi* | [**serverFollowingPost**](docs/InstanceFollowsApi.md#serverfollowingpost) | **POST** /server/following | Follow a list of actors (PeerTube instance, channel or account)
*InstanceRedundancyApi* | [**serverRedundancyHostPut**](docs/InstanceRedundancyApi.md#serverredundancyhostput) | **PUT** /server/redundancy/{host} | Update a server redundancy policy
*JobApi* | [**getJobs**](docs/JobApi.md#getjobs) | **GET** /jobs/{state} | List instance jobs
*LiveVideosApi* | [**addLive**](docs/LiveVideosApi.md#addlive) | **POST** /videos/live | Create a live
*LiveVideosApi* | [**getLiveId**](docs/LiveVideosApi.md#getliveid) | **GET** /videos/live/{id} | Get information about a live
*LiveVideosApi* | [**updateLiveId**](docs/LiveVideosApi.md#updateliveid) | **PUT** /videos/live/{id} | Update information about a live
*MyHistoryApi* | [**usersMeHistoryVideosGet**](docs/MyHistoryApi.md#usersmehistoryvideosget) | **GET** /users/me/history/videos | List watched videos history
*MyHistoryApi* | [**usersMeHistoryVideosRemovePost**](docs/MyHistoryApi.md#usersmehistoryvideosremovepost) | **POST** /users/me/history/videos/remove | Clear video history
*MyNotificationsApi* | [**usersMeNotificationSettingsPut**](docs/MyNotificationsApi.md#usersmenotificationsettingsput) | **PUT** /users/me/notification-settings | Update my notification settings
*MyNotificationsApi* | [**usersMeNotificationsGet**](docs/MyNotificationsApi.md#usersmenotificationsget) | **GET** /users/me/notifications | List my notifications
*MyNotificationsApi* | [**usersMeNotificationsReadAllPost**](docs/MyNotificationsApi.md#usersmenotificationsreadallpost) | **POST** /users/me/notifications/read-all | Mark all my notification as read
*MyNotificationsApi* | [**usersMeNotificationsReadPost**](docs/MyNotificationsApi.md#usersmenotificationsreadpost) | **POST** /users/me/notifications/read | Mark notifications as read by their id
*MySubscriptionsApi* | [**usersMeSubscriptionsExistGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsexistget) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for my user
*MySubscriptionsApi* | [**usersMeSubscriptionsGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsget) | **GET** /users/me/subscriptions | Get my user subscriptions
*MySubscriptionsApi* | [**usersMeSubscriptionsPost**](docs/MySubscriptionsApi.md#usersmesubscriptionspost) | **POST** /users/me/subscriptions | Add subscription to my user
*MySubscriptionsApi* | [**usersMeSubscriptionsSubscriptionHandleDelete**](docs/MySubscriptionsApi.md#usersmesubscriptionssubscriptionhandledelete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
*MySubscriptionsApi* | [**usersMeSubscriptionsSubscriptionHandleGet**](docs/MySubscriptionsApi.md#usersmesubscriptionssubscriptionhandleget) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
*MySubscriptionsApi* | [**usersMeSubscriptionsVideosGet**](docs/MySubscriptionsApi.md#usersmesubscriptionsvideosget) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
*MyUserApi* | [**getMyAbuses**](docs/MyUserApi.md#getmyabuses) | **GET** /users/me/abuses | List my abuses
*MyUserApi* | [**getUserInfo**](docs/MyUserApi.md#getuserinfo) | **GET** /users/me | Get my user information
*MyUserApi* | [**putUserInfo**](docs/MyUserApi.md#putuserinfo) | **PUT** /users/me | Update my user information
*MyUserApi* | [**usersMeAvatarDelete**](docs/MyUserApi.md#usersmeavatardelete) | **DELETE** /users/me/avatar | Delete my avatar
*MyUserApi* | [**usersMeAvatarPickPost**](docs/MyUserApi.md#usersmeavatarpickpost) | **POST** /users/me/avatar/pick | Update my user avatar
*MyUserApi* | [**usersMeVideoQuotaUsedGet**](docs/MyUserApi.md#usersmevideoquotausedget) | **GET** /users/me/video-quota-used | Get my user used quota
*MyUserApi* | [**usersMeVideosGet**](docs/MyUserApi.md#usersmevideosget) | **GET** /users/me/videos | Get videos of my user
*MyUserApi* | [**usersMeVideosImportsGet**](docs/MyUserApi.md#usersmevideosimportsget) | **GET** /users/me/videos/imports | Get video imports of my user
*MyUserApi* | [**usersMeVideosVideoIdRatingGet**](docs/MyUserApi.md#usersmevideosvideoidratingget) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*PluginsApi* | [**addPlugin**](docs/PluginsApi.md#addplugin) | **POST** /plugins/install | Install a plugin
*PluginsApi* | [**getAvailablePlugins**](docs/PluginsApi.md#getavailableplugins) | **GET** /plugins/available | List available plugins
*PluginsApi* | [**getPlugin**](docs/PluginsApi.md#getplugin) | **GET** /plugins/{npmName} | Get a plugin
*PluginsApi* | [**getPlugins**](docs/PluginsApi.md#getplugins) | **GET** /plugins | List plugins
*PluginsApi* | [**pluginsNpmNamePublicSettingsGet**](docs/PluginsApi.md#pluginsnpmnamepublicsettingsget) | **GET** /plugins/{npmName}/public-settings | Get a plugin's public settings
*PluginsApi* | [**pluginsNpmNameRegisteredSettingsGet**](docs/PluginsApi.md#pluginsnpmnameregisteredsettingsget) | **GET** /plugins/{npmName}/registered-settings | Get a plugin's registered settings
*PluginsApi* | [**pluginsNpmNameSettingsPut**](docs/PluginsApi.md#pluginsnpmnamesettingsput) | **PUT** /plugins/{npmName}/settings | Set a plugin's settings
*PluginsApi* | [**uninstallPlugin**](docs/PluginsApi.md#uninstallplugin) | **POST** /plugins/uninstall | Uninstall a plugin
*PluginsApi* | [**updatePlugin**](docs/PluginsApi.md#updateplugin) | **POST** /plugins/update | Update a plugin
*RegisterApi* | [**registerUser**](docs/RegisterApi.md#registeruser) | **POST** /users/register | Register a user
*RegisterApi* | [**resendEmailToVerifyUser**](docs/RegisterApi.md#resendemailtoverifyuser) | **POST** /users/ask-send-verify-email | Resend user verification link
*RegisterApi* | [**verifyUser**](docs/RegisterApi.md#verifyuser) | **POST** /users/{id}/verify-email | Verify a user
*SearchApi* | [**searchChannels**](docs/SearchApi.md#searchchannels) | **GET** /search/video-channels | Search channels
*SearchApi* | [**searchPlaylists**](docs/SearchApi.md#searchplaylists) | **GET** /search/video-playlists | Search playlists
*SearchApi* | [**searchVideos**](docs/SearchApi.md#searchvideos) | **GET** /search/videos | Search videos
*ServerBlocksApi* | [**serverBlocklistServersGet**](docs/ServerBlocksApi.md#serverblocklistserversget) | **GET** /server/blocklist/servers | List server blocks
*ServerBlocksApi* | [**serverBlocklistServersHostDelete**](docs/ServerBlocksApi.md#serverblocklistservershostdelete) | **DELETE** /server/blocklist/servers/{host} | Unblock a server by its domain
*ServerBlocksApi* | [**serverBlocklistServersPost**](docs/ServerBlocksApi.md#serverblocklistserverspost) | **POST** /server/blocklist/servers | Block a server
*SessionApi* | [**getOAuthClient**](docs/SessionApi.md#getoauthclient) | **GET** /oauth-clients/local | Login prerequisite
*SessionApi* | [**getOAuthToken**](docs/SessionApi.md#getoauthtoken) | **POST** /users/token | Login
*SessionApi* | [**revokeOAuthToken**](docs/SessionApi.md#revokeoauthtoken) | **POST** /users/revoke-token | Logout
*UsersApi* | [**addUser**](docs/UsersApi.md#adduser) | **POST** /users | Create a user
*UsersApi* | [**delUser**](docs/UsersApi.md#deluser) | **DELETE** /users/{id} | Delete a user
*UsersApi* | [**getUser**](docs/UsersApi.md#getuser) | **GET** /users/{id} | Get a user
*UsersApi* | [**getUsers**](docs/UsersApi.md#getusers) | **GET** /users | List users
*UsersApi* | [**putUser**](docs/UsersApi.md#putuser) | **PUT** /users/{id} | Update a user
*UsersApi* | [**registerUser**](docs/UsersApi.md#registeruser) | **POST** /users/register | Register a user
*UsersApi* | [**resendEmailToVerifyUser**](docs/UsersApi.md#resendemailtoverifyuser) | **POST** /users/ask-send-verify-email | Resend user verification link
*UsersApi* | [**verifyUser**](docs/UsersApi.md#verifyuser) | **POST** /users/{id}/verify-email | Verify a user
*VideoApi* | [**addLive**](docs/VideoApi.md#addlive) | **POST** /videos/live | Create a live
*VideoApi* | [**addView**](docs/VideoApi.md#addview) | **POST** /videos/{id}/views | Add a view to a video
*VideoApi* | [**delVideo**](docs/VideoApi.md#delvideo) | **DELETE** /videos/{id} | Delete a video
*VideoApi* | [**getAccountVideos**](docs/VideoApi.md#getaccountvideos) | **GET** /accounts/{name}/videos | List videos of an account
*VideoApi* | [**getCategories**](docs/VideoApi.md#getcategories) | **GET** /videos/categories | List available video categories
*VideoApi* | [**getLanguages**](docs/VideoApi.md#getlanguages) | **GET** /videos/languages | List available video languages
*VideoApi* | [**getLicences**](docs/VideoApi.md#getlicences) | **GET** /videos/licences | List available video licences
*VideoApi* | [**getLiveId**](docs/VideoApi.md#getliveid) | **GET** /videos/live/{id} | Get information about a live
*VideoApi* | [**getPrivacyPolicies**](docs/VideoApi.md#getprivacypolicies) | **GET** /videos/privacies | List available video privacy policies
*VideoApi* | [**getVideo**](docs/VideoApi.md#getvideo) | **GET** /videos/{id} | Get a video
*VideoApi* | [**getVideoChannelVideos**](docs/VideoApi.md#getvideochannelvideos) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoApi* | [**getVideoDesc**](docs/VideoApi.md#getvideodesc) | **GET** /videos/{id}/description | Get complete video description
*VideoApi* | [**getVideos**](docs/VideoApi.md#getvideos) | **GET** /videos | List videos
*VideoApi* | [**importVideo**](docs/VideoApi.md#importvideo) | **POST** /videos/imports | Import a video
*VideoApi* | [**putVideo**](docs/VideoApi.md#putvideo) | **PUT** /videos/{id} | Update a video
*VideoApi* | [**setProgress**](docs/VideoApi.md#setprogress) | **PUT** /videos/{id}/watching | Set watching progress of a video
*VideoApi* | [**updateLiveId**](docs/VideoApi.md#updateliveid) | **PUT** /videos/live/{id} | Update information about a live
*VideoApi* | [**uploadLegacy**](docs/VideoApi.md#uploadlegacy) | **POST** /videos/upload | Upload a video
*VideoApi* | [**uploadResumable**](docs/VideoApi.md#uploadresumable) | **PUT** /videos/upload-resumable | Send chunk for the resumable upload of a video
*VideoApi* | [**uploadResumableCancel**](docs/VideoApi.md#uploadresumablecancel) | **DELETE** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
*VideoApi* | [**uploadResumableInit**](docs/VideoApi.md#uploadresumableinit) | **POST** /videos/upload-resumable | Initialize the resumable upload of a video
*VideoBlocksApi* | [**addVideoBlock**](docs/VideoBlocksApi.md#addvideoblock) | **POST** /videos/{id}/blacklist | Block a video
*VideoBlocksApi* | [**delVideoBlock**](docs/VideoBlocksApi.md#delvideoblock) | **DELETE** /videos/{id}/blacklist | Unblock a video by its id
*VideoBlocksApi* | [**getVideoBlocks**](docs/VideoBlocksApi.md#getvideoblocks) | **GET** /videos/blacklist | List video blocks
*VideoCaptionsApi* | [**addVideoCaption**](docs/VideoCaptionsApi.md#addvideocaption) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionsApi* | [**delVideoCaption**](docs/VideoCaptionsApi.md#delvideocaption) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionsApi* | [**getVideoCaptions**](docs/VideoCaptionsApi.md#getvideocaptions) | **GET** /videos/{id}/captions | List captions of a video
*VideoChannelsApi* | [**accountsNameVideoChannelsGet**](docs/VideoChannelsApi.md#accountsnamevideochannelsget) | **GET** /accounts/{name}/video-channels | List video channels of an account
*VideoChannelsApi* | [**addVideoChannel**](docs/VideoChannelsApi.md#addvideochannel) | **POST** /video-channels | Create a video channel
*VideoChannelsApi* | [**delVideoChannel**](docs/VideoChannelsApi.md#delvideochannel) | **DELETE** /video-channels/{channelHandle} | Delete a video channel
*VideoChannelsApi* | [**getVideoChannel**](docs/VideoChannelsApi.md#getvideochannel) | **GET** /video-channels/{channelHandle} | Get a video channel
*VideoChannelsApi* | [**getVideoChannelVideos**](docs/VideoChannelsApi.md#getvideochannelvideos) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoChannelsApi* | [**getVideoChannels**](docs/VideoChannelsApi.md#getvideochannels) | **GET** /video-channels | List video channels
*VideoChannelsApi* | [**putVideoChannel**](docs/VideoChannelsApi.md#putvideochannel) | **PUT** /video-channels/{channelHandle} | Update a video channel
*VideoChannelsApi* | [**videoChannelsChannelHandleAvatarDelete**](docs/VideoChannelsApi.md#videochannelschannelhandleavatardelete) | **DELETE** /video-channels/{channelHandle}/avatar | Delete channel avatar
*VideoChannelsApi* | [**videoChannelsChannelHandleAvatarPickPost**](docs/VideoChannelsApi.md#videochannelschannelhandleavatarpickpost) | **POST** /video-channels/{channelHandle}/avatar/pick | Update channel avatar
*VideoChannelsApi* | [**videoChannelsChannelHandleBannerDelete**](docs/VideoChannelsApi.md#videochannelschannelhandlebannerdelete) | **DELETE** /video-channels/{channelHandle}/banner | Delete channel banner
*VideoChannelsApi* | [**videoChannelsChannelHandleBannerPickPost**](docs/VideoChannelsApi.md#videochannelschannelhandlebannerpickpost) | **POST** /video-channels/{channelHandle}/banner/pick | Update channel banner
*VideoCommentsApi* | [**videosIdCommentThreadsGet**](docs/VideoCommentsApi.md#videosidcommentthreadsget) | **GET** /videos/{id}/comment-threads | List threads of a video
*VideoCommentsApi* | [**videosIdCommentThreadsPost**](docs/VideoCommentsApi.md#videosidcommentthreadspost) | **POST** /videos/{id}/comment-threads | Create a thread
*VideoCommentsApi* | [**videosIdCommentThreadsThreadIdGet**](docs/VideoCommentsApi.md#videosidcommentthreadsthreadidget) | **GET** /videos/{id}/comment-threads/{threadId} | Get a thread
*VideoCommentsApi* | [**videosIdCommentsCommentIdDelete**](docs/VideoCommentsApi.md#videosidcommentscommentiddelete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment or a reply
*VideoCommentsApi* | [**videosIdCommentsCommentIdPost**](docs/VideoCommentsApi.md#videosidcommentscommentidpost) | **POST** /videos/{id}/comments/{commentId} | Reply to a thread of a video
*VideoMirroringApi* | [**delMirroredVideo**](docs/VideoMirroringApi.md#delmirroredvideo) | **DELETE** /server/redundancy/videos/{redundancyId} | Delete a mirror done on a video
*VideoMirroringApi* | [**getMirroredVideos**](docs/VideoMirroringApi.md#getmirroredvideos) | **GET** /server/redundancy/videos | List videos being mirrored
*VideoMirroringApi* | [**putMirroredVideo**](docs/VideoMirroringApi.md#putmirroredvideo) | **POST** /server/redundancy/videos | Mirror a video
*VideoOwnershipChangeApi* | [**videosIdGiveOwnershipPost**](docs/VideoOwnershipChangeApi.md#videosidgiveownershippost) | **POST** /videos/{id}/give-ownership | Request ownership change
*VideoOwnershipChangeApi* | [**videosOwnershipGet**](docs/VideoOwnershipChangeApi.md#videosownershipget) | **GET** /videos/ownership | List video ownership changes
*VideoOwnershipChangeApi* | [**videosOwnershipIdAcceptPost**](docs/VideoOwnershipChangeApi.md#videosownershipidacceptpost) | **POST** /videos/ownership/{id}/accept | Accept ownership change request
*VideoOwnershipChangeApi* | [**videosOwnershipIdRefusePost**](docs/VideoOwnershipChangeApi.md#videosownershipidrefusepost) | **POST** /videos/ownership/{id}/refuse | Refuse ownership change request
*VideoPlaylistsApi* | [**addPlaylist**](docs/VideoPlaylistsApi.md#addplaylist) | **POST** /video-playlists | Create a video playlist
*VideoPlaylistsApi* | [**addVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#addvideoplaylistvideo) | **POST** /video-playlists/{playlistId}/videos | Add a video in a playlist
*VideoPlaylistsApi* | [**delVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#delvideoplaylistvideo) | **DELETE** /video-playlists/{playlistId}/videos/{playlistElementId} | Delete an element from a playlist
*VideoPlaylistsApi* | [**getPlaylistPrivacyPolicies**](docs/VideoPlaylistsApi.md#getplaylistprivacypolicies) | **GET** /video-playlists/privacies | List available playlist privacy policies
*VideoPlaylistsApi* | [**getPlaylists**](docs/VideoPlaylistsApi.md#getplaylists) | **GET** /video-playlists | List video playlists
*VideoPlaylistsApi* | [**getVideoPlaylistVideos**](docs/VideoPlaylistsApi.md#getvideoplaylistvideos) | **GET** /video-playlists/{playlistId}/videos | List videos of a playlist
*VideoPlaylistsApi* | [**putVideoPlaylistVideo**](docs/VideoPlaylistsApi.md#putvideoplaylistvideo) | **PUT** /video-playlists/{playlistId}/videos/{playlistElementId} | Update a playlist element
*VideoPlaylistsApi* | [**reorderVideoPlaylist**](docs/VideoPlaylistsApi.md#reordervideoplaylist) | **POST** /video-playlists/{playlistId}/videos/reorder | Reorder a playlist
*VideoPlaylistsApi* | [**usersMeVideoPlaylistsVideosExistGet**](docs/VideoPlaylistsApi.md#usersmevideoplaylistsvideosexistget) | **GET** /users/me/video-playlists/videos-exist | Check video exists in my playlists
*VideoPlaylistsApi* | [**videoPlaylistsPlaylistIdDelete**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistiddelete) | **DELETE** /video-playlists/{playlistId} | Delete a video playlist
*VideoPlaylistsApi* | [**videoPlaylistsPlaylistIdGet**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistidget) | **GET** /video-playlists/{playlistId} | Get a video playlist
*VideoPlaylistsApi* | [**videoPlaylistsPlaylistIdPut**](docs/VideoPlaylistsApi.md#videoplaylistsplaylistidput) | **PUT** /video-playlists/{playlistId} | Update a video playlist
*VideoRatesApi* | [**usersMeVideosVideoIdRatingGet**](docs/VideoRatesApi.md#usersmevideosvideoidratingget) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*VideoRatesApi* | [**videosIdRatePut**](docs/VideoRatesApi.md#videosidrateput) | **PUT** /videos/{id}/rate | Like/dislike a video
*VideoUploadApi* | [**importVideo**](docs/VideoUploadApi.md#importvideo) | **POST** /videos/imports | Import a video
*VideoUploadApi* | [**uploadLegacy**](docs/VideoUploadApi.md#uploadlegacy) | **POST** /videos/upload | Upload a video
*VideoUploadApi* | [**uploadResumable**](docs/VideoUploadApi.md#uploadresumable) | **PUT** /videos/upload-resumable | Send chunk for the resumable upload of a video
*VideoUploadApi* | [**uploadResumableCancel**](docs/VideoUploadApi.md#uploadresumablecancel) | **DELETE** /videos/upload-resumable | Cancel the resumable upload of a video, deleting any data uploaded so far
*VideoUploadApi* | [**uploadResumableInit**](docs/VideoUploadApi.md#uploadresumableinit) | **POST** /videos/upload-resumable | Initialize the resumable upload of a video
*VideosApi* | [**addVideoPlaylistVideo**](docs/VideosApi.md#addvideoplaylistvideo) | **POST** /video-playlists/{playlistId}/videos | Add a video in a playlist
*VideosApi* | [**getVideoPlaylistVideos**](docs/VideosApi.md#getvideoplaylistvideos) | **GET** /video-playlists/{playlistId}/videos | List videos of a playlist
*VideosApi* | [**usersMeSubscriptionsVideosGet**](docs/VideosApi.md#usersmesubscriptionsvideosget) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
*VideosApi* | [**usersMeVideosGet**](docs/VideosApi.md#usersmevideosget) | **GET** /users/me/videos | Get videos of my user
*VideosApi* | [**usersMeVideosImportsGet**](docs/VideosApi.md#usersmevideosimportsget) | **GET** /users/me/videos/imports | Get video imports of my user


<a name="documentation-for-models"></a>
## Documentation for Models

 - [org.peertube.client.models.Abuse](docs/Abuse.md)
 - [org.peertube.client.models.AbuseMessage](docs/AbuseMessage.md)
 - [org.peertube.client.models.AbuseStateConstant](docs/AbuseStateConstant.md)
 - [org.peertube.client.models.AbuseStateSet](docs/AbuseStateSet.md)
 - [org.peertube.client.models.AbusesAccount](docs/AbusesAccount.md)
 - [org.peertube.client.models.AbusesComment](docs/AbusesComment.md)
 - [org.peertube.client.models.AbusesVideo](docs/AbusesVideo.md)
 - [org.peertube.client.models.Account](docs/Account.md)
 - [org.peertube.client.models.AccountAllOf](docs/AccountAllOf.md)
 - [org.peertube.client.models.AccountSummary](docs/AccountSummary.md)
 - [org.peertube.client.models.Actor](docs/Actor.md)
 - [org.peertube.client.models.ActorImage](docs/ActorImage.md)
 - [org.peertube.client.models.ActorInfo](docs/ActorInfo.md)
 - [org.peertube.client.models.ActorInfoAvatar](docs/ActorInfoAvatar.md)
 - [org.peertube.client.models.AddUser](docs/AddUser.md)
 - [org.peertube.client.models.AddUserResponse](docs/AddUserResponse.md)
 - [org.peertube.client.models.AddUserResponseUser](docs/AddUserResponseUser.md)
 - [org.peertube.client.models.CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [org.peertube.client.models.CommentThreadResponse](docs/CommentThreadResponse.md)
 - [org.peertube.client.models.CustomHomepage](docs/CustomHomepage.md)
 - [org.peertube.client.models.FileRedundancyInformation](docs/FileRedundancyInformation.md)
 - [org.peertube.client.models.Follow](docs/Follow.md)
 - [org.peertube.client.models.GetMeVideoRating](docs/GetMeVideoRating.md)
 - [org.peertube.client.models.InlineObject](docs/InlineObject.md)
 - [org.peertube.client.models.InlineObject1](docs/InlineObject1.md)
 - [org.peertube.client.models.InlineObject11](docs/InlineObject11.md)
 - [org.peertube.client.models.InlineObject12](docs/InlineObject12.md)
 - [org.peertube.client.models.InlineObject13](docs/InlineObject13.md)
 - [org.peertube.client.models.InlineObject19](docs/InlineObject19.md)
 - [org.peertube.client.models.InlineObject2](docs/InlineObject2.md)
 - [org.peertube.client.models.InlineObject20](docs/InlineObject20.md)
 - [org.peertube.client.models.InlineObject21](docs/InlineObject21.md)
 - [org.peertube.client.models.InlineObject22](docs/InlineObject22.md)
 - [org.peertube.client.models.InlineObject23](docs/InlineObject23.md)
 - [org.peertube.client.models.InlineObject24](docs/InlineObject24.md)
 - [org.peertube.client.models.InlineObject25](docs/InlineObject25.md)
 - [org.peertube.client.models.InlineObject26](docs/InlineObject26.md)
 - [org.peertube.client.models.InlineObject27](docs/InlineObject27.md)
 - [org.peertube.client.models.InlineObject28](docs/InlineObject28.md)
 - [org.peertube.client.models.InlineObject29](docs/InlineObject29.md)
 - [org.peertube.client.models.InlineObject3](docs/InlineObject3.md)
 - [org.peertube.client.models.InlineObject30](docs/InlineObject30.md)
 - [org.peertube.client.models.InlineObject4](docs/InlineObject4.md)
 - [org.peertube.client.models.InlineObject5](docs/InlineObject5.md)
 - [org.peertube.client.models.InlineResponse200](docs/InlineResponse200.md)
 - [org.peertube.client.models.InlineResponse2001](docs/InlineResponse2001.md)
 - [org.peertube.client.models.InlineResponse20010](docs/InlineResponse20010.md)
 - [org.peertube.client.models.InlineResponse20011](docs/InlineResponse20011.md)
 - [org.peertube.client.models.InlineResponse20012](docs/InlineResponse20012.md)
 - [org.peertube.client.models.InlineResponse20012VideoPlaylist](docs/InlineResponse20012VideoPlaylist.md)
 - [org.peertube.client.models.InlineResponse20013](docs/InlineResponse20013.md)
 - [org.peertube.client.models.InlineResponse20013VideoPlaylistElement](docs/InlineResponse20013VideoPlaylistElement.md)
 - [org.peertube.client.models.InlineResponse20014](docs/InlineResponse20014.md)
 - [org.peertube.client.models.InlineResponse20014VideoId](docs/InlineResponse20014VideoId.md)
 - [org.peertube.client.models.InlineResponse2002](docs/InlineResponse2002.md)
 - [org.peertube.client.models.InlineResponse2003](docs/InlineResponse2003.md)
 - [org.peertube.client.models.InlineResponse2004](docs/InlineResponse2004.md)
 - [org.peertube.client.models.InlineResponse2005](docs/InlineResponse2005.md)
 - [org.peertube.client.models.InlineResponse2006](docs/InlineResponse2006.md)
 - [org.peertube.client.models.InlineResponse2006Abuse](docs/InlineResponse2006Abuse.md)
 - [org.peertube.client.models.InlineResponse2007](docs/InlineResponse2007.md)
 - [org.peertube.client.models.InlineResponse2008](docs/InlineResponse2008.md)
 - [org.peertube.client.models.InlineResponse2009](docs/InlineResponse2009.md)
 - [org.peertube.client.models.InlineResponse204](docs/InlineResponse204.md)
 - [org.peertube.client.models.InlineResponse204VideoChannel](docs/InlineResponse204VideoChannel.md)
 - [org.peertube.client.models.Job](docs/Job.md)
 - [org.peertube.client.models.LiveVideoResponse](docs/LiveVideoResponse.md)
 - [org.peertube.client.models.LiveVideoUpdate](docs/LiveVideoUpdate.md)
 - [org.peertube.client.models.MRSSGroupContent](docs/MRSSGroupContent.md)
 - [org.peertube.client.models.MRSSPeerLink](docs/MRSSPeerLink.md)
 - [org.peertube.client.models.NSFWPolicy](docs/NSFWPolicy.md)
 - [org.peertube.client.models.Notification](docs/Notification.md)
 - [org.peertube.client.models.NotificationActorFollow](docs/NotificationActorFollow.md)
 - [org.peertube.client.models.NotificationActorFollowFollowing](docs/NotificationActorFollowFollowing.md)
 - [org.peertube.client.models.NotificationComment](docs/NotificationComment.md)
 - [org.peertube.client.models.NotificationListResponse](docs/NotificationListResponse.md)
 - [org.peertube.client.models.NotificationSettingValue](docs/NotificationSettingValue.md)
 - [org.peertube.client.models.NotificationVideoAbuse](docs/NotificationVideoAbuse.md)
 - [org.peertube.client.models.NotificationVideoImport](docs/NotificationVideoImport.md)
 - [org.peertube.client.models.OAuthClient](docs/OAuthClient.md)
 - [org.peertube.client.models.PlaylistElement](docs/PlaylistElement.md)
 - [org.peertube.client.models.Plugin](docs/Plugin.md)
 - [org.peertube.client.models.PluginResponse](docs/PluginResponse.md)
 - [org.peertube.client.models.RegisterUser](docs/RegisterUser.md)
 - [org.peertube.client.models.RegisterUserChannel](docs/RegisterUserChannel.md)
 - [org.peertube.client.models.ServerConfig](docs/ServerConfig.md)
 - [org.peertube.client.models.ServerConfigAbout](docs/ServerConfigAbout.md)
 - [org.peertube.client.models.ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [org.peertube.client.models.ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [org.peertube.client.models.ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [org.peertube.client.models.ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [org.peertube.client.models.ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [org.peertube.client.models.ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [org.peertube.client.models.ServerConfigCustom](docs/ServerConfigCustom.md)
 - [org.peertube.client.models.ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [org.peertube.client.models.ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [org.peertube.client.models.ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [org.peertube.client.models.ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [org.peertube.client.models.ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [org.peertube.client.models.ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [org.peertube.client.models.ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [org.peertube.client.models.ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [org.peertube.client.models.ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [org.peertube.client.models.ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [org.peertube.client.models.ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [org.peertube.client.models.ServerConfigCustomTranscodingHls](docs/ServerConfigCustomTranscodingHls.md)
 - [org.peertube.client.models.ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [org.peertube.client.models.ServerConfigCustomTranscodingWebtorrent](docs/ServerConfigCustomTranscodingWebtorrent.md)
 - [org.peertube.client.models.ServerConfigCustomUser](docs/ServerConfigCustomUser.md)
 - [org.peertube.client.models.ServerConfigEmail](docs/ServerConfigEmail.md)
 - [org.peertube.client.models.ServerConfigFollowings](docs/ServerConfigFollowings.md)
 - [org.peertube.client.models.ServerConfigFollowingsInstance](docs/ServerConfigFollowingsInstance.md)
 - [org.peertube.client.models.ServerConfigFollowingsInstanceAutoFollowIndex](docs/ServerConfigFollowingsInstanceAutoFollowIndex.md)
 - [org.peertube.client.models.ServerConfigImport](docs/ServerConfigImport.md)
 - [org.peertube.client.models.ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [org.peertube.client.models.ServerConfigInstance](docs/ServerConfigInstance.md)
 - [org.peertube.client.models.ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [org.peertube.client.models.ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [org.peertube.client.models.ServerConfigSearch](docs/ServerConfigSearch.md)
 - [org.peertube.client.models.ServerConfigSearchRemoteUri](docs/ServerConfigSearchRemoteUri.md)
 - [org.peertube.client.models.ServerConfigSignup](docs/ServerConfigSignup.md)
 - [org.peertube.client.models.ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [org.peertube.client.models.ServerConfigTrending](docs/ServerConfigTrending.md)
 - [org.peertube.client.models.ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [org.peertube.client.models.ServerConfigUser](docs/ServerConfigUser.md)
 - [org.peertube.client.models.ServerConfigVideo](docs/ServerConfigVideo.md)
 - [org.peertube.client.models.ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [org.peertube.client.models.ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [org.peertube.client.models.ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [org.peertube.client.models.ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [org.peertube.client.models.UpdateMe](docs/UpdateMe.md)
 - [org.peertube.client.models.UpdateUser](docs/UpdateUser.md)
 - [org.peertube.client.models.User](docs/User.md)
 - [org.peertube.client.models.UserAdminFlags](docs/UserAdminFlags.md)
 - [org.peertube.client.models.UserRole](docs/UserRole.md)
 - [org.peertube.client.models.UserWatchingVideo](docs/UserWatchingVideo.md)
 - [org.peertube.client.models.UserWithStats](docs/UserWithStats.md)
 - [org.peertube.client.models.UserWithStatsAllOf](docs/UserWithStatsAllOf.md)
 - [org.peertube.client.models.Video](docs/Video.md)
 - [org.peertube.client.models.VideoBlacklist](docs/VideoBlacklist.md)
 - [org.peertube.client.models.VideoCaption](docs/VideoCaption.md)
 - [org.peertube.client.models.VideoChannel](docs/VideoChannel.md)
 - [org.peertube.client.models.VideoChannelCreate](docs/VideoChannelCreate.md)
 - [org.peertube.client.models.VideoChannelCreateAllOf](docs/VideoChannelCreateAllOf.md)
 - [org.peertube.client.models.VideoChannelList](docs/VideoChannelList.md)
 - [org.peertube.client.models.VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [org.peertube.client.models.VideoChannelSummary](docs/VideoChannelSummary.md)
 - [org.peertube.client.models.VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [org.peertube.client.models.VideoChannelUpdateAllOf](docs/VideoChannelUpdateAllOf.md)
 - [org.peertube.client.models.VideoComment](docs/VideoComment.md)
 - [org.peertube.client.models.VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [org.peertube.client.models.VideoConstantNumberMinusCategory](docs/VideoConstantNumberMinusCategory.md)
 - [org.peertube.client.models.VideoConstantNumberMinusLicence](docs/VideoConstantNumberMinusLicence.md)
 - [org.peertube.client.models.VideoConstantStringMinusLanguage](docs/VideoConstantStringMinusLanguage.md)
 - [org.peertube.client.models.VideoDetails](docs/VideoDetails.md)
 - [org.peertube.client.models.VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [org.peertube.client.models.VideoFile](docs/VideoFile.md)
 - [org.peertube.client.models.VideoImport](docs/VideoImport.md)
 - [org.peertube.client.models.VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [org.peertube.client.models.VideoImportsList](docs/VideoImportsList.md)
 - [org.peertube.client.models.VideoInfo](docs/VideoInfo.md)
 - [org.peertube.client.models.VideoListResponse](docs/VideoListResponse.md)
 - [org.peertube.client.models.VideoPlaylist](docs/VideoPlaylist.md)
 - [org.peertube.client.models.VideoPlaylistPrivacyConstant](docs/VideoPlaylistPrivacyConstant.md)
 - [org.peertube.client.models.VideoPlaylistPrivacySet](docs/VideoPlaylistPrivacySet.md)
 - [org.peertube.client.models.VideoPlaylistTypeConstant](docs/VideoPlaylistTypeConstant.md)
 - [org.peertube.client.models.VideoPlaylistTypeSet](docs/VideoPlaylistTypeSet.md)
 - [org.peertube.client.models.VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [org.peertube.client.models.VideoPrivacySet](docs/VideoPrivacySet.md)
 - [org.peertube.client.models.VideoRating](docs/VideoRating.md)
 - [org.peertube.client.models.VideoRedundancy](docs/VideoRedundancy.md)
 - [org.peertube.client.models.VideoRedundancyRedundancies](docs/VideoRedundancyRedundancies.md)
 - [org.peertube.client.models.VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [org.peertube.client.models.VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [org.peertube.client.models.VideoStateConstant](docs/VideoStateConstant.md)
 - [org.peertube.client.models.VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [org.peertube.client.models.VideoStreamingPlaylistsAllOf](docs/VideoStreamingPlaylistsAllOf.md)
 - [org.peertube.client.models.VideoStreamingPlaylistsHLSRedundancies](docs/VideoStreamingPlaylistsHLSRedundancies.md)
 - [org.peertube.client.models.VideoStreamingPlaylistsMinusHLS](docs/VideoStreamingPlaylistsMinusHLS.md)
 - [org.peertube.client.models.VideoUploadRequestCommon](docs/VideoUploadRequestCommon.md)
 - [org.peertube.client.models.VideoUploadRequestResumable](docs/VideoUploadRequestResumable.md)
 - [org.peertube.client.models.VideoUploadRequestResumableAllOf](docs/VideoUploadRequestResumableAllOf.md)
 - [org.peertube.client.models.VideoUploadResponse](docs/VideoUploadResponse.md)
 - [org.peertube.client.models.VideoUploadResponseVideo](docs/VideoUploadResponseVideo.md)
 - [org.peertube.client.models.VideoUserHistory](docs/VideoUserHistory.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

<a name="OAuth2"></a>
### OAuth2

- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
  - admin: Admin scope
  - moderator: Moderator scope
  - user: User scope


## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.
